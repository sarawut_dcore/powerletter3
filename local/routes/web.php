<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home Controller
Route::get('/', 'HomeController@getIndex')->middleware('auth');

Route::get('/menu01', 'topics_name_controller@getIndex')->middleware('auth');

Route::get('/menu01/{no_topics}', 'topics_name_controller@getTopics_detail')->middleware('auth');
Route::post('/menu01/add', 'topics_name_controller@add_Topic')->middleware('auth');

Route::post('/menu01/{main_no}/{no_topics}', 'topics_name_controller@add_Detail')->middleware('auth');

Route::post('/menu01_update/{id}/update', 'topics_name_controller@updatetopics');

//------------------------------------------------------------------------------
//   Test

Route::get('/menu2', 'policy_menu_controller@getIndex');
Route::post('/menu2/savefile', 'policy_menu_controller@savefile');

Route::get('/list_policy_01', 'policycontroller@getIndex');
Route::post('/list_policy_01/{id}', 'policycontroller@savePolicy');

Route::get('/menu3', 'DepartmentController@getIndex');
Route::get('/menu3/add', 'DepartmentController@savedepartment');
Route::get('/menu3/{id}', 'DepartmentController@getTopics_detail');
Route::get('/menu3/{id}/add', 'DepartmentController@index_add');
Route::post('/menu3/{id}/save', 'DepartmentController@index_add_save');

Route::post('/menu3_add/{id}/add', 'DepartmentController@saveDatadetaildepartment');
Route::post('/menu3_save/{id}/save', 'DepartmentController@editDatadetaildepartment');
// Route::post('/menu3/{id}/save', 'DepartmentController@savetransection_cmd_log');
Route::post('/menu3_edit/{id}/edit', 'DepartmentController@editDatadetaildepartment');
Route::post('/menu3_delete/{id}/delete', 'DepartmentController@deleteDatadetaildepartment');
Route::post('/menu3_evaluation/{id}/evaluation', 'DepartmentController@save_evaluation');
Route::post('/menu3_update/{id}/update', 'DepartmentController@updateDatadetaildepartment');

Route::get('/menu4', 'AdminreportController@getIndex')->middleware('auth', 'role:1');
Route::get('/menu4/{id}', 'AdminreportController@detailpolicy')->middleware('auth', 'role:1');
Route::post('/menu4_search/{id}/search', 'AdminreportController@detailpolicy1')->middleware('auth', 'role:1');
Route::get('/menu5', 'AdminreportController@getIndex1')->middleware('auth', 'role:1');
Route::get('/menu5/{id}', 'AdminreportController@detailpolicy2')->middleware('auth', 'role:1');
Route::post('/menu5_search/{id}/search', 'AdminreportController@detailpolicy3')->middleware('auth', 'role:1');

Route::get('/setting_prison', 'PrisonController@getIndex');
//manage prison office name
Route::get('/setting_prison', 'PrisonController@getIndex');
Route::post('/setting_prison/add', 'PrisonController@saveDataOffice');
Route::post('/setting_prison/{id}/edit', 'PrisonController@editDataOffice');
Route::post('/setting_prison/{id}/delete', 'PrisonController@deleteDataOffice');

//ToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneTone
Route::get('/setting_type_user', 'type_user_controller@getIndex');
Route::post('/setting_type_user/save', 'type_user_controller@saveType');
Route::post('/setting_type_user/{id}/edit', 'type_user_controller@editType');
Route::post('/setting_type_user/{id}/delete', 'type_user_controller@deleteType');

Route::get('/setting_user', 'UserController@getIndex');
Route::post('/setting_user/save', 'UserController@saveType');
Route::post('/setting_user/{id}/edit', 'UserController@editType');
Route::post('/setting_user/{id}/delete', 'UserController@deleteType');
//ToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneToneTone

Route::get('/menu51', function () {
    return view('report_cmd/51menu');
});

Route::get('/menu61', function () {
    return view('report_cmd/61menu');
});
Route::get('/menu71', function () {
    return view('report_cmd/71menu');
});
Route::get('/menu81', function () {
    return view('report_cmd/81menu');
});
Route::get('/menu91', function () {
    return view('report_cmd/91menu');
});
Route::get('/menu101', function () {
    return view('report_cmd/101menu');
});

/*
// Evaluation Controller
Route::get('/evaluation/{Indicator_id}', 'EvaluationController@getKpi')->middleware('auth');
Route::get('/evaluation', 'EvaluationController@getIndex')->middleware('auth');

Route::get('/evaluation2/{Indicator_id}', 'EvaluationController@getKpi2')->middleware('auth');
Route::get('/evaluation2', 'EvaluationController@getIndex2')->middleware('auth');

Route::post('/save_eva','EvaluationController@saveEva')->middleware('auth');
Route::post('/save_eva2','EvaluationController@saveEva2')->middleware('auth');

// Report  Controller
Route::get('/report/sum06', 'ReportController@getIndex6');
Route::get('/report/sum12', 'ReportController@getIndex12');



// Indicator Controller
Route::get('/indicator', 'IndicatorController@getIndex')->middleware('auth');
Route::get('/indicator2', 'IndicatorController@getIndex2')->middleware('auth');
Route::post('/ajax/indicator/save-record', 'IndicatorController@postAjaxSaveRecord')->middleware('auth');

// CheckScore Controller
Route::get('/check-score', 'CheckScoreController@getIndex')->middleware('auth');
Route::get('/check-score2', 'CheckScoreController@getIndex2')->middleware('auth');
*/

// Auth
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', 'Auth\LoginController@login');
Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/register', 'Auth\RegisterController@register');
Route::get('/logout', function () {
    Auth::logout();

    return redirect('/login');
});

// Gen password
Route::get('generate-password/{password}', function ($password) {
    return bcrypt($password);
});
