<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Indicator
 *
 * @property int $id
 * @property string|null $detail
 * @property int|null $indicator_type_id
 * @property string|null $target
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $station_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\IndicatorChoice[] $choices
 * @property-read \App\Station|null $station
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\IndicatorSubDetail[] $subDetails
 * @property-read \App\IndicatorType|null $type
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Indicator whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Indicator whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Indicator whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Indicator whereIndicatorTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Indicator whereStationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Indicator whereTarget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Indicator whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Indicator extends Model
{
    protected $table = 'indicator';

    public function isPercent() {
        if ($this->type->id == 1) {
            return true;
        }

        return false;
    }

    public function isCount() {
        if ($this->type->id == 2) {
            return true;
        }

        return false;
    }

    public function isStep() {
        if ($this->type->id == 3) {
            return true;
        }

        return false;
    }
    
    public function isStepEdit() {
        if ($this->type->id == 4) {
            return true;
        }

        return false;
    }

    public function choices() {
        return $this->hasMany(IndicatorChoice::class, 'indicator_id');
    }

    public function station() {
        return $this->belongsTo(Station::class, 'station_id');
    }

    public function type() {
        return $this->belongsTo(IndicatorType::class, 'indicator_type_id');
    }

    public function subDetails() {
        return $this->hasMany(IndicatorSubDetail::class, 'indicator_id');
    }
}
