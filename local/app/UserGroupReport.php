<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Station
 *
 * @property int $id
 * @property string|null $name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Station whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Station whereName($value)
 * @mixin \Eloquent
 */
class UserGroupReport extends Model
{
    protected $table = 'user_group_report';
}
