<?php

namespace App\Http\Controllers;
use DB;
use App\policy;
use App\transection_policy;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class policycontroller extends Controller
{
    public function getIndex(  ) {


    $datapolicy = DB::select('SELECT policy.id,transection_policy.score,transection_policy.user_id,policy.num_1,policy.num_2,policy.num_3,policy.num_comment,
                                     policy.detail,transection_policy.path_file,transection_policy.comment
                                     FROM policy
                                     LEFT JOIN transection_policy ON policy.id=transection_policy.policy_id
                                     AND transection_policy.id IN (SELECT MAX(transection_policy.id) FROM transection_policy WHERE transection_policy.user_id = ? 
                                     GROUP BY policy_id) AND transection_policy.user_id = ? ORDER BY policy.id', [Auth::user()->id,Auth::user()->id]);
    

      
    return view('policy/list_policy_01', ['_datapolicy' =>  $datapolicy] );
 
    }

    public function getIndex2(  ) {
        return "2menu";
    }

    
    public function savePolicy( Request $request, $id) {

    if($request->Check=='on')
    {
        $Check = 1;
    }
    else
    {
        $Check = 0;
    }

    $data_transection_policy = new transection_policy();

    
    if ($request->hasFile('image')){ 

        $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(public_path('/file'), $filename);
        $data_transection_policy->path_file = $filename;
     } 
     
        $data_transection_policy->policy_id = $id;
        $data_transection_policy->score = $Check;
        $data_transection_policy->user_id = Auth::user()->id;
        $data_transection_policy->user_name = Auth::user()->username;
        $data_transection_policy->comment = $request->comment;
        $data_transection_policy->save();
    
        return redirect('/list_policy_01');
        
        
         
    }

}
