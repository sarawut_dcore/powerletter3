<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\user_type;

class type_user_controller extends Controller
{
    public function getIndex()
    {
        $datatype = DB ::table('user_type')->get();

        return view('setting/type_user', ['_datatype' => $datatype]);
    }

    public function saveType(Request $request)
    {
       $validate = \Validator::make($request->all(), [
            'name'  =>  'required',
        ], [
            'name.required'     =>  'ชื่อประเภทผู้ใช้งานต้องไม่ว่าง',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }
        
      $data_type_user_all = new user_type();
      $data_type_user_all->name_user_type = $request->name;
      $data_type_user_all->save();

      return redirect('/setting_type_user');
    }

    public function editType(Request $request, $id)
    {
      $validate = \Validator::make($request->all(), [
            'name'  =>  'required',
        ], [
            'name.required'     =>  'ชื่อประเภทผู้ใช้งานต้องไม่ว่าง',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }
        
      $data_type_user_all = user_type::find($id);
      $data_type_user_all->name_user_type = $request->name;
      $data_type_user_all->save();

      return redirect('/setting_type_user');
    }

    public function deleteType($id)
    {
      $data_type_user_all = user_type::find($id);
      
      $data_type_user_all->delete();

      return redirect('/setting_type_user');
    }

}
