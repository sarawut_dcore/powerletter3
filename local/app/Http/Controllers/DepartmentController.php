<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\transection_cmd;
use App\office;
use App\Department;
use App\transection_cmd_log;

class DepartmentController extends Controller
{
    public function getIndex()
    {
        $datadepartment = DB ::table('department')->get();
        $dataoffice = DB ::table('office')->get();

        return view('report_auth/menu_report', ['_datadepartment' => $datadepartment]);
    }

    public function index_add($id)
    {              
        $dataoffice = DB ::table('office')->get();

        $no_cmd = DB::table('transection_cmd')
                        ->get();
        $data_evaluator = DB::table('department')
                        ->get();

        return view('report_cmd/report_add', ['_dataoffice' => $dataoffice,'_data_evaluator' => $data_evaluator,'_id'=>$id,'_no_cmd'=> $no_cmd]);

        // return view('report_cmd/report_add', []);
    }

    public function index_add_save($id,Request $request)
    {    
        $dataoffice = DB ::table('office')->get();

        $i = $request->no_cmd;
        $filename = NULL;

        if ($request->hasFile('image')){ 

            $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('/file'), $filename);
            // $data_transection_cmd->path_file = $filename;
            // $data_transection_cmd_log->path_file = $filename;
        } 
        
        foreach ($dataoffice as $out_dataoffice)
        {
    
            
            if($request->input($out_dataoffice->id) == $out_dataoffice->id)
            {
                // echo $out_dataoffice->id;

                $data_office = office::where('id',$out_dataoffice->id)->limit(1)->first();

                // echo $data_office->id;
                    

                    $data_transection_cmd = new transection_cmd();
                    $data_transection_cmd_log = new transection_cmd_log();

                    $data_transection_cmd->id = $i;
                    $data_transection_cmd->department_no = $id;
                    $data_transection_cmd->id_cmd = $request->id_cmd;
                    $data_transection_cmd->topic = $request->topic;
                    $data_transection_cmd->command = $request->command;
                    $data_transection_cmd->offer = $request->offer;
                    $data_transection_cmd->inheritor = $request->inheritor;
                    $data_transection_cmd->Action = $request->Action;
                    $data_transection_cmd->status = $request->status;
                    $data_transection_cmd->prison = $data_office->office_name;
                    $data_transection_cmd->evaluation = $request->evaluation;
                    $data_transection_cmd->evaluator = $request->evaluator;
                    $data_transection_cmd->user_id = Auth::user()->id;
                    $data_transection_cmd->user_name = Auth::user()->username;
                    $data_transection_cmd->path_file = $filename;
                    $data_transection_cmd->save();

                    $data_transection_cmd_log->id_cmd = $i;
                    $data_transection_cmd_log->user_id = Auth::user()->username;
                    $data_transection_cmd_log->path_file = $filename;
                    $data_transection_cmd_log->save();

                    $i = $i+1;
            }
        }

        return redirect('menu3/'.$id);
        // return $request->all();

        // return view('report_cmd/report_add', []);
    }


    public function savedepartment(Request $request)
    {
      $datadepartment = new Department();
      $datadepartment->name_department = $request->name_department;
      $datadepartment->save();

      return redirect('/menu3');
    }

    public function getTopics_detail(Request $request,$id)
    {
        if(Auth::user()->group == 3)
        {
            $datadetaildepartment =DB::select('SELECT
            transection_cmd.id,
            transection_cmd.topic,
            transection_cmd.prison,
            transection_cmd.evaluation,
            transection_cmd.evaluator,
            transection_cmd.command,
            transection_cmd.offer,
            transection_cmd.inheritor,
            transection_cmd.Action,
            transection_cmd.`status`,
            transection_cmd.path_file,
            transection_cmd.user_id
            FROM
            transection_cmd
            LEFT JOIN transection_cmd_log ON transection_cmd.id = transection_cmd_log.id_cmd AND transection_cmd_log.id in 
            (SELECT max(transection_cmd_log.id) FROM transection_cmd_log) WHERE transection_cmd.department_no = "'.$id.'"
            ORDER BY
            transection_cmd.id DESC');
        }
        else
        {
            $datadetaildepartment =DB::select('SELECT
            transection_cmd.id,
            transection_cmd.topic,
            transection_cmd.prison,
            transection_cmd.evaluation,
            transection_cmd.evaluator,
            transection_cmd.command,
            transection_cmd.offer,
            transection_cmd.inheritor,
            transection_cmd.Action,
            transection_cmd.`status`,
            transection_cmd.path_file,
            transection_cmd.user_id
            FROM
            transection_cmd
            LEFT JOIN transection_cmd_log ON transection_cmd.id = transection_cmd_log.id_cmd AND transection_cmd_log.id in 
            (SELECT max(transection_cmd_log.id) FROM transection_cmd_log WHERE transection_cmd_log.user_id= "'.Auth::user()->username.'" GROUP BY id_cmd)
            AND transection_cmd.user_id = "'.Auth::user()->id.'" WHERE transection_cmd.department_no = "'.$id.'" AND transection_cmd.user_name = "'.Auth::user()->username.'"
            ORDER BY
            transection_cmd.id DESC');
        }

        // $datadetaildepartment =DB::select('SELECT
        //             transection_cmd.id,
        //             transection_cmd.topic,
        //             transection_cmd.prison,
        //             transection_cmd.evaluation,
        //             transection_cmd.evaluator,
        //             transection_cmd.command,
        //             transection_cmd.offer,
        //             transection_cmd.inheritor,
        //             transection_cmd.Action,
        //             transection_cmd.`status`,
        //             transection_cmd.path_file,
        //             transection_cmd.user_id
        //             FROM
        //             transection_cmd
        //             LEFT JOIN transection_cmd_log ON transection_cmd.id = transection_cmd_log.id_cmd AND transection_cmd_log.id in 
        //             (SELECT max(transection_cmd_log.id) FROM transection_cmd_log) WHERE transection_cmd.department_no = "'.$id.'"
        //             ORDER BY
        //             transection_cmd.id DESC');
        
                      
        $dataoffice = DB ::table('office')->get();

        $datadepartment = DB ::table('department')
                        ->where('id', '=', $request->id)    
                        ->get();
        $no_cmd = DB::table('transection_cmd')
                        ->get();
        $data_evaluator = DB::table('department')
                        ->get();

        return view('report_cmd/41menu', ['_datadetaildepartment' => $datadetaildepartment, '_dataoffice' => $dataoffice,'_datadepartment' => $datadepartment,'_data_evaluator' => $data_evaluator,'_id'=>$id,'_no_cmd'=> $no_cmd]);
    }

    public function saveDatadetaildepartment(Request $request,$id)
    {
        /*$validate = \Validator::make($request->all(), [
            'id_cmd'  =>  'required',
            'topic'  =>  'required',
            'Action'  =>  'required',
        ], [
            'id_cmd.required'     =>  'ลำดับต้องไม่ว่าง',
            'topic.required'     =>  'เรื่องที่มอบอำนาจต้องไม่ว่าง',
            'Action.required'     =>  'สถานะต้องไม่ว่าง',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }
*/
        $data_transection_cmd = new transection_cmd();
        $data_transection_cmd_log = new transection_cmd_log();

        $filename = NULL;

        if ($request->hasFile('image')){ 

            $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('/file'), $filename);
            $data_transection_cmd->path_file = $filename;
            $data_transection_cmd_log->path_file = $filename;
        } 

        $data_transection_cmd->id =$request->no_cmd;
        $data_transection_cmd->department_no = $id;
        $data_transection_cmd->id_cmd = $request->id_cmd;
        $data_transection_cmd->topic = $request->topic;
        $data_transection_cmd->command = $request->command;
        $data_transection_cmd->offer = $request->offer;
        $data_transection_cmd->inheritor = $request->inheritor;
        $data_transection_cmd->Action = $request->Action;
        $data_transection_cmd->status = $request->status;
        $data_transection_cmd->prison = $request->prison;
        $data_transection_cmd->evaluation = $request->evaluation;
        $data_transection_cmd->evaluator = $request->evaluator;
        $data_transection_cmd->user_id = Auth::user()->id;
        $data_transection_cmd->user_name = Auth::user()->username;
        $data_transection_cmd->save();

        $data_transection_cmd_log->id_cmd = $request->no_cmd;
        $data_transection_cmd_log->user_id = Auth::user()->username;
        $data_transection_cmd_log->save();


        return redirect('/menu3/'.$id);
    }

    public function editDatadetaildepartment(Request $request,$id)
    {
        $validate = \Validator::make($request->all(), [
            'id_cmd'  =>  'required',
            'topic'  =>  'required',
            'Action'  =>  'required',
        ], [
            'id_cmd.required'     =>  'ลำดับต้องไม่ว่าง',
            'topic.required'     =>  'เรื่องที่มอบอำนาจต้องไม่ว่าง',
            'Action.required'     =>  'สถานะต้องไม่ว่าง',
        ]);
        
        $data_transection_cmd = transection_cmd::find($id);
        $data_transection_cmd_log = new transection_cmd_log();

        if ($request->hasFile('image')){ 

            $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('/file'), $filename);
            $data_transection_cmd->path_file = $filename;
            $data_transection_cmd_log->path_file = $filename;
         } 
         else{
            $data_transection_cmd->path_file = $request->image2;
            $data_transection_cmd_log->path_file = $request->image2;
         }

         
        $data_transection_cmd->id_cmd = $request->id_cmd;
        $data_transection_cmd->topic = $request->topic;
        $data_transection_cmd->command = $request->command;
        $data_transection_cmd->offer = $request->offer;
        $data_transection_cmd->inheritor = $request->inheritor;
        $data_transection_cmd->Action = $request->Action;
        $data_transection_cmd->status = $request->status;
        $data_transection_cmd->prison = $request->prison;
        $data_transection_cmd->evaluation = $request->evaluation;
        $data_transection_cmd->evaluator = $request->evaluator;
        $data_transection_cmd->save();


        $data_transection_cmd_log->id_cmd = $id;
        $data_transection_cmd_log->user_id = Auth::user()->username;
        $data_transection_cmd_log->save();


        //return redirect('/menu3/'.$request->department_no);
        return redirect('/menu3/'.$data_transection_cmd->department_no);
    }

    public function deleteDatadetaildepartment(Request $request,$id)
    {
        $data_transection_cmd = transection_cmd::find($id);
        $data_transection_cmd->delete();


        //return redirect('/menu3/'.$request->department_no);
        return redirect('/menu3/'.$data_transection_cmd->department_no);
    }

    public function updateDatadetaildepartment(Request $request,$id)
    {
        $data_transection_cmd = new transection_cmd();

        if ($request->hasFile('image')){ 

            $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('/file'), $filename);
            $data_transection_cmd->path_file = $filename;
         } 
         elseif(!($request->hasFile('image'))){
            $data_transection_cmd->path_file = $request->defualt_file;
         }
        $data_transection_cmd->department_no = $request->department;
        $data_transection_cmd->topic = $request->topics;
        $data_transection_cmd->command = $request->command_no;
        $data_transection_cmd->offer = $request->name_offer;
        $data_transection_cmd->inheritor = $request->name_receive;
        $data_transection_cmd->prison = Auth::user()->office_name;
        $data_transection_cmd->user_id = Auth::user()->id;
        $data_transection_cmd->user_name = Auth::user()->username;
        $data_transection_cmd->save();

        return redirect('/menu3/'.$request->department);
    }
    
    public function save_transection_cmd_log(Request $request,$id){
        $data_transection_cmd_log = new transection_cmd_log();

        $data_transection_cmd_log->id_cmd = $request->id_cmd;
        $data_transection_cmd_log->status = $request->status;
        $data_transection_cmd_log->user_id = Auth::user()->username;
        $data_transection_cmd_log->evaluation = $request->evaluation;
        $data_transection_cmd_log->evaluator = $request->evaluator;
        $data_transection_cmd_log->save();
        return redirect('/menu3/'.$id);
    }

    public function save_evaluation(Request $request,$id){
        $data_transection_cmd = DB::table('transection_cmd')
        ->where('id', $request->id_cmd_log)
        ->update(['evaluation' => $request->evaluation,'evaluator'=>$request->evaluator]);
        return redirect('/menu3/'.$request->id_cmd);
    }
}
