<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class AdminreportController extends Controller
{
    public function getIndex()
    {
        $report = DB::select('SELECT
                                    policy.detail,
                                    policy.id,
                                    policy.num_1,
                                    policy.num_2
                                    FROM
                                    policy
                                    WHERE
                                    policy.num_2 = 0
                           ');

        return view('admin_report/report_index1_main', ['_report' => $report]);
    }

    public function detailpolicy($id)
    {
        $report1 = DB::select("SELECT
                                    policy.detail,
                                    policy.num_1,
                                    policy.num_2,
                                    GROUP_CONCAT(DISTINCT `user`.office_name) AS office_name,
                                    transection_policy.updated_at
                                    FROM
                                    policy ,
                                    transection_policy ,
                                    `user`
                                    WHERE
                                    policy.id = transection_policy.policy_id AND
                                    transection_policy.user_id = `user`.id AND
                                    transection_policy.score = 1 AND
                                    policy.num_1 = '$id'
                                    GROUP BY policy.num_2
                                    ORDER BY
                                    policy.num_2 ASC"
                             );

        $report2 = DB::select('SELECT
                                policy.id,
                                policy.detail,
                                policy.num_1
                                FROM
                                policy
                                WHERE
                                policy.num_2 = 0 AND
                                policy.num_1 = ?', [$id]);
        $year = 0;

        return view('admin_report/report_index1', ['_report1' => $report1, '_report2' => $report2, '_year' => $year]);
    }

    public function detailpolicy1(Request $request, $id)
    {
        $report1 = DB::select('SELECT
                                    policy.detail,
                                    policy.num_1,
                                    policy.num_2,
                                    GROUP_CONCAT(DISTINCT `user`.office_name) AS office_name,
                                    transection_policy.updated_at
                                    FROM
                                    policy ,
                                    transection_policy ,
                                    `user`
                                    WHERE
                                    policy.id = transection_policy.policy_id AND
                                    transection_policy.user_id = `user`.id AND
                                    transection_policy.score = 1 AND
                                    policy.num_1 = ? AND 
                                    YEAR(transection_policy.updated_at) = ? 
                                    GROUP BY policy.num_2
                                    ORDER BY
                                    policy.num_2 ASC', [$id, $request->year]);

        $report2 = DB::select('SELECT
                                    policy.id,
                                    policy.detail,
                                    policy.num_1
                                    FROM
                                    policy
                                    WHERE
                                    policy.num_2 = 0 AND
                                    policy.num_1 = ?', [$id]);
        $year = $request->year;

        return view('admin_report/report_index1', ['_report1' => $report1, '_report2' => $report2, '_year' => $year]);
    }

    public function getIndex1()
    {
        $datadepartment = DB ::table('department')->get();

        return view('admin_report/report_index2_main', ['_datadepartment' => $datadepartment]);
    }

    public function detailpolicy2($id)
    {
        $report2 = DB::select(" SELECT
                                        department.id,
                                        department.name_department,
                                        transection_cmd.topic,
                                        transection_cmd.inheritor,
                                        transection_cmd.prison,
                                        transection_cmd.Action,
                                        transection_cmd.updated_at,
                                        GROUP_CONCAT(DISTINCT transection_cmd.offer) AS offer
                                        FROM
                                        department ,
                                        transection_cmd
                                        WHERE
                                        department.id = transection_cmd.department_no AND
                                        department.id = '$id' 
                                        GROUP BY
                                        transection_cmd.topic
                                        ORDER BY
                                        transection_cmd.topic ASC
                              ");

        $report3 = DB::select('SELECT
                                department.id
                                FROM
                                department
                                WHERE
                                department.id = ?', [$id]);
        $month = 0;
        $year = 0;

        //return $year;
        return view('admin_report/report_index2', ['_report2' => $report2, '_report3' => $report3, '_year' => $year, '_month' => $month]);

        // return view('admin_report/report_index2', ['_report2' => $report2, '_report3' => $report3]);
    }

    public function detailpolicy3(Request $request, $id)
    {
        $report2 = DB::select('SELECT
                                        department.id,
                                        department.name_department,
                                        transection_cmd.topic,
                                        transection_cmd.inheritor,
                                        transection_cmd.prison,
                                        transection_cmd.Action,
                                        transection_cmd.updated_at,
                                        GROUP_CONCAT(DISTINCT transection_cmd.offer) AS offer
                                        FROM
                                        department ,
                                        transection_cmd
                                        WHERE
                                        department.id = transection_cmd.department_no AND
                                        department.id = ? AND
                                        YEAR(transection_cmd.updated_at) = ? AND
                                        MONTH(transection_cmd.updated_at) = ?
                                        GROUP BY
                                        transection_cmd.topic
                                        ORDER BY
                                       transection_cmd.topic ASC ', [$id, $request->year, $request->month]);

        $report3 = DB::select('SELECT
                                department.id,
                                department.name_department
                                FROM
                                department
                                WHERE
                                department.id = ?', [$id]);
        $year = $request->year;
        $month = $request->month;
        //return $year;
        return view('admin_report/report_index2', ['_report2' => $report2, '_report3' => $report3, '_year' => $year, '_month' => $month]);
    }
}
