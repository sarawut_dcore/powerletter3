<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\User;
use App\user_type;
use App\office;
use App\Station;

class UserController extends Controller
{
    public function getIndex()
    {
        $datauser = DB ::table('user')
        ->join('user_type','user.group','=','user_type.id')
        ->join('station','user.station_id','=','station.id')
        ->select('user.*','station.name_station','user_type.name_user_type')
        ->get();

        $datastation = DB ::table('station')->get();
        $dataoffice = DB ::table('office')->get();
        $datauser_type = DB ::table('user_type')->get();

        return view('setting/user', ['_datauser' => $datauser,'_datastation' => $datastation, '_dataoffice' => $dataoffice, '_datauser_type' => $datauser_type]);
    }

    public function saveType(Request $request)
    {
      $validate = \Validator::make($request->all(), [
        'username'  =>  'required|unique:user',
        'password'  =>  'required|min:6',
    ], [
        'username.required'     =>  'ชื่อผู้ใช้งานต้องไม่ว่าง',
        'username.unique'       =>  'ชื่อผู้ใช้งานี้ถูกใช้งานแล้ว',
        'password.required'     =>  'รหัสผ่านต้องไม่ว่าง',
        'password.min'          =>  'รหัสผ่านต้องมีอย่างน้อย 6 ตัวอักษร',
    ]);

    if ($validate->fails()) {
        return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
    }

      $data_type_user = new user();
      $data_type_user->username = $request->username;
      $data_type_user->password = bcrypt($request->password);
      $data_type_user->name = $request->name;
      $data_type_user->telephone = $request->telephone;
      $data_type_user->group = $request->group;
      $data_type_user->office_name = $request->office_name;
      $data_type_user->note = $request->note;
      $data_type_user->station_id = $request->station_id;
      $data_type_user->save();

      return redirect('/setting_user');
    }

    public function editType(Request $request, $id)
    {
        
      $data_type_user = user::find($id);
      $data_type_user->name = $request->name;
      $data_type_user->telephone = $request->telephone;
      $data_type_user->group = $request->group;
      $data_type_user->office_name = $request->office_name;
      $data_type_user->note = $request->note;
      $data_type_user->station_id = $request->station_id;
      $data_type_user->save();

      return redirect('/setting_user');
    }

    public function deleteType($id)
    {
      $data_type_user = user::find($id);
      
      $data_type_user->delete();

      return redirect('/setting_user');
    }

}
