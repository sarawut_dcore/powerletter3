<?php

namespace App\Http\Controllers;
use DB;
use App\policy_menu;
use App\transection_policy_menu;


use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class policy_menu_controller extends Controller
{
    public function getIndex(  ) {
        
       $datapolicy_menu = DB ::table('policy_menu')->select('id')->get();

       //$count_datapolicy_menu = policy_menu::All()->count();
       
       $datapolicy_menu = policy_menu::All();

       $datapolicy_file = DB::select('SELECT
       transection_policy_menu.id,
       transection_policy_menu.policy_menu_id,
       transection_policy_menu.add_file
       FROM
       transection_policy_menu
       WHERE transection_policy_menu.id IN 
       (SELECT MAX(transection_policy_menu.id) AS id 
       FROM transection_policy_menu WHERE transection_policy_menu.user_id = ? 
       GROUP BY transection_policy_menu.policy_menu_id) 
       AND transection_policy_menu.user_id = ?', [Auth::user()->id,Auth::user()->id]);
      
       return view('policy/menu_policy', ['_datapolicy_menu' =>  $datapolicy_menu,'_datapolicy_file' => $datapolicy_file] );
 
    }

    public function savefile( Request $request) {
    
        // $data_policy_menu = new policy_menu();
        // $data_policy_menu = policy_menu::find($request->id);
    
        
        // if ($request->hasFile('image')){ 
    
        //     $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
        //     $request->file('image')->move(public_path('/file'), $filename);
        //     $data_policy_menu->add_file = $filename;
        //  } 
        //     $data_policy_menu->save();
        
        //     return redirect('/menu2');


        // $data_policy_menu = new transection_policy_menu();
        $data_policy_menu = new transection_policy_menu();
        $data_policy_menu->policy_menu_id = $request->id;
        $data_policy_menu->user_id = Auth::user()->id;

        if ($request->hasFile('image')){ 
    
            $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('/file'), $filename);
            $data_policy_menu->add_file = $filename;
         } 

        $data_policy_menu->save();

        return redirect('/menu2');
            
            
             
        }

}
