<?php

namespace App\Http\Controllers;
use App\User;
use App\Indicator;
use App\IndicatorChoice;
use App\TransactionChoice;
use App\IndicatorSubDetail;
use App\TransactionEva;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EvaluationController extends Controller
{
    public function getIndex() {
        
        if(Auth::user()->group < 2 )
        {
          if(Auth::user()->username <> 'user144' )
            return  "<H1>ท่าน ไม่มีสิทธ์ใช้งานหน้านี้ </H1>";
        }
        $_menu_9001   = Indicator::where('station_id',9001)->get();
        $_menu_9002   = Indicator::where('station_id',9002)->get();
        $_menu_9003   = Indicator::where('station_id',9003)->get();
        $_menu_9004   = Indicator::where('station_id',9004)->get();
        $_menu_9005   = Indicator::where('station_id',9005)->get();
        $_menu_9006   = Indicator::where('station_id',9006)->get();
        $_menu_9007   = Indicator::where('station_id',9007)->get();
        $_menu_9008   = Indicator::where('station_id',9008)->get();
        $_menu_9009   = Indicator::where('station_id',9009)->get();
        $_menu_9010   = Indicator::where('station_id',9010)->get();
        $_menu_9011   = Indicator::where('station_id',9011)->get();
        $_menu_9012   = Indicator::where('station_id',9012)->get();        
        $_menu_9013   = Indicator::where('station_id',9013)->get();
        $_menu_9014   = Indicator::where('station_id',9014)->get();
        $_menu_9015   = Indicator::where('station_id',9015)->get();       
        $_menu_9016   = Indicator::where('station_id',9016)->get();
        $_menu_9017   = Indicator::where('station_id',9017)->get();
        $_menu_9018   = Indicator::where('station_id',9018)->get();
        $_menu_9020   = Indicator::where('station_id',9020)->get();         
        
        
        return view('evaluation.menu',[
            
                                    '_menu_9001' =>  $_menu_9001 ,
                                    '_menu_9002' =>  $_menu_9002 ,
                                    '_menu_9003' =>  $_menu_9003 ,
                                    '_menu_9004' =>  $_menu_9004 ,                                   
                                    '_menu_9005' =>  $_menu_9005 ,
                                    '_menu_9006' =>  $_menu_9006 ,
                                    '_menu_9007' =>  $_menu_9007 ,
                                    '_menu_9008' =>  $_menu_9008 ,
                                    '_menu_9009' =>  $_menu_9009 ,
                                    '_menu_9010' =>  $_menu_9010 ,
                                    '_menu_9011' =>  $_menu_9011 ,
                                    '_menu_9012' =>  $_menu_9012 , 
                                    '_menu_9013' =>  $_menu_9013 ,
                                    '_menu_9014' =>  $_menu_9014 ,                                   
                                    '_menu_9015' =>  $_menu_9015 ,                                    
                                    '_menu_9016' =>  $_menu_9016 ,
                                    '_menu_9017' =>  $_menu_9017 ,                                   
                                    '_menu_9018' =>  $_menu_9018 ,
                                    '_menu_9020' =>  $_menu_9020 
                                  
                                    
                                    
                               ]);
       
    }

    public function getIndex2() {
        
        if(Auth::user()->group < 2 )
        {
          if(Auth::user()->username <> 'user144' )
            return  "<H1>ท่าน ไม่มีสิทธ์ใช้งานหน้านี้ </H1>";
        }
     

        $_menu_9020   = Indicator::where('station_id',9020)
                                  ->orderBy('indicator.id', 'asc')   
                                  ->get();         
        
        
        return view('evaluation.menu2',[
                      
                                    '_menu_9020' =>  $_menu_9020 
                                    
                               ]);
       
    }
    
    public function getKpi( Request $request) {
        
        //return ($request->Indicator_id);
        if($request->EvaSave != null)
        {
  
            $_TransactionEva = TransactionEva::where('indicator_id', $request->Indicator_id )->where('user_name',$request->EvaSave)->first();
            
            if(!$_TransactionEva) {
                $_TransactionEva = new TransactionEva();
            }
            $_TransactionEva->user_name =  $request->EvaSave;
            $_TransactionEva->Indicator_id =  $request->Indicator_id;
            $_TransactionEva->score =  $request->score;
            $_TransactionEva->comment =  $request->comment;
            $_TransactionEva->user_evaluate = Auth::user()->username;
            $_TransactionEva->name_evaluate = Auth::user()->name;
            $_TransactionEva->telephone_evaluate = Auth::user()->telephone;
            $_TransactionEva->save();
            $_str_redect ="/evaluation/".$request->Indicator_id; 
            return redirect($_str_redect);
        }
        

        $_indicator = Indicator::where('id',$request->Indicator_id )
                                    ->orderBy('indicator.id', 'asc') 
                                    ->get();
        $_IndicatorChoice = IndicatorChoice::where('Indicator_id', $request->Indicator_id )->orderBy('id','asc')->get();
        $_IndicatorSubDetail = IndicatorSubDetail::where('Indicator_id',$request->Indicator_id )->orderBy('id','asc','num_grad','asc')->get();
        
        $_Choice = TransactionChoice::where('indicator_choice.indicator_id',$request->Indicator_id) 
                                    ->leftJoin('indicator_choice', 'transaction_choice.indicator_choice_id', '=', 'indicator_choice.id') 
                                    ->leftJoin('transaction_evaluate',function ($join) {
                                        $join->On('transaction_evaluate.user_name' ,'=','transaction_choice.user_name')
                                             ->On('transaction_evaluate.indicator_id' ,'=','indicator_choice.indicator_id');
                                    })
                                    ->Join('user', function ($join) {
                                        $join->On('user.username' ,'=','transaction_choice.user_name')
                                             ->On('user.station_id' ,'=','transaction_choice.department_id');
                                    }) 
                                    
                                   // LEFT JOIN indicator_choice ON transaction_choice.indicator_choice_id = indicator_choice.id 
                                   //LEFT JOIN transaction_evaluate ON transaction_evaluate.user_name = transaction_choice.user_name AND transaction_evaluate.indicator_id = indicator_choice.indicator_id
                                                                   
                                    //->leftJoin('transaction_evaluate', 'transaction_evaluate.indicator_id' ,'=','transaction_choice.indicator_choice_id')
                                    //->leftJoin('transaction_evaluate', 'transaction_evaluate.user_name' ,'=','transaction_evaluate.user_name')

                                   
                                    
                                    ->select('indicator_choice.id as main_indicator_id','transaction_choice.user_name' ,'user.office_name','transaction_choice.department_id','transaction_choice.indicator_choice_id'
                                            ,'transaction_choice.score','transaction_choice.path_file','indicator_choice.detail as choice_detail'
                                            ,'transaction_evaluate.score as eva_score', 'transaction_evaluate.comment as eva_comment','transaction_evaluate.user_evaluate'
                                            ,'transaction_evaluate.user_name as eva_user_name')
                                            ->orderBy('user.office_name','asc')
                                            ->orderBy('transaction_choice.user_name')
                                            ->orderBy('indicator_choice.id', 'asc') 
                                            ->get();
       
        
        
       return view('evaluation.index',[ '_indicator'=>$_indicator,'_Choice'=>$_Choice,'_IndicatorSubDetail'=>$_IndicatorSubDetail ,'_IndicatorChoice'=>$_IndicatorChoice]);
       
    }
     
    public function getKpi2( Request $request) {
        
        //return ($request->Indicator_id);
        if($request->EvaSave != null)
        {
  
            $_TransactionEva = TransactionEva::where('indicator_id', $request->Indicator_id )
                                ->where('user_name',$request->EvaSave)
                                
                                ->first();
            if(!$_TransactionEva) {
                $_TransactionEva = new TransactionEva();
            }
            $_TransactionEva->user_name =  $request->EvaSave;
            $_TransactionEva->Indicator_id =  $request->Indicator_id;
            $_TransactionEva->score =  $request->score;
            $_TransactionEva->comment =  $request->comment;
            $_TransactionEva->user_evaluate = Auth::user()->username;
            $_TransactionEva->name_evaluate = Auth::user()->name;
            $_TransactionEva->telephone_evaluate = Auth::user()->telephone;
            $_TransactionEva->save();
            
            $_str_redect ="/evaluation2/".$request->Indicator_id; 
            return redirect($_str_redect);
        }
        

        $_indicator = Indicator::where('id',$request->Indicator_id )
                                       ->orderBy('indicator.id', 'asc') 
                                       ->get();
        $_IndicatorChoice = IndicatorChoice::where('Indicator_id', $request->Indicator_id )->orderBy('id','asc')->get();
        $_IndicatorSubDetail = IndicatorSubDetail::where('Indicator_id',$request->Indicator_id )->orderBy('id','asc','num_grad','asc')->get();
        
        $_Choice = TransactionChoice::where('indicator_choice.indicator_id',$request->Indicator_id) 
                                    ->leftJoin('indicator_choice', 'transaction_choice.indicator_choice_id', '=', 'indicator_choice.id') 
                                    ->leftJoin('transaction_evaluate',function ($join) {
                                        $join->On('transaction_evaluate.user_name' ,'=','transaction_choice.user_name')
                                             ->On('transaction_evaluate.indicator_id' ,'=','indicator_choice.indicator_id');
                                    })
                                    ->Join('user', function ($join) {
                                        $join->On('user.username' ,'=','transaction_choice.user_name')
                                             ->On('user.station_id2' ,'=','transaction_choice.department_id2');
                                    }) 
                                    
                                   // LEFT JOIN indicator_choice ON transaction_choice.indicator_choice_id = indicator_choice.id 
                                   //LEFT JOIN transaction_evaluate ON transaction_evaluate.user_name = transaction_choice.user_name AND transaction_evaluate.indicator_id = indicator_choice.indicator_id
                                                                   
                                    //->leftJoin('transaction_evaluate', 'transaction_evaluate.indicator_id' ,'=','transaction_choice.indicator_choice_id')
                                    //->leftJoin('transaction_evaluate', 'transaction_evaluate.user_name' ,'=','transaction_evaluate.user_name')

                                   
                                    
                                    ->select('indicator_choice.id as main_indicator_id','transaction_choice.user_name' ,'user.office_name','transaction_choice.department_id2','transaction_choice.indicator_choice_id'
                                            ,'transaction_choice.score','transaction_choice.path_file','indicator_choice.detail as choice_detail'
                                            ,'transaction_evaluate.score as eva_score', 'transaction_evaluate.comment as eva_comment','transaction_evaluate.user_evaluate'
                                            ,'transaction_evaluate.user_name as eva_user_name','transaction_evaluate.updated_at as eva_update_at')
                                            ->orderBy('eva_update_at','asc')
                                           // ->orderBy('user.office_name','asc')
                                            ->orderBy('transaction_choice.user_name')
                                            ->orderBy('indicator_choice.id', 'asc') 
                                            ->get();
         
       return view('evaluation.index2',[ '_indicator'=>$_indicator,'_Choice'=>$_Choice,'_IndicatorSubDetail'=>$_IndicatorSubDetail ,'_IndicatorChoice'=>$_IndicatorChoice]);
       
    }
    
    
    public function save_eva(Request $request) {
        
        

        
        return "zz";
    
    }
    
        
    
}
