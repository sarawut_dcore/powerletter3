<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\transection_cmd;
use App\office;

class PrisonController extends Controller
{
    public function getIndex()
    {
        $dataoffice = DB ::table('office')->get();

        return view('setting/prison', ['_dataoffice' => $dataoffice]);
    }


    public function saveDataOffice(Request $request)
    {
        $validate = \Validator::make($request->all(), [
            'office_name'  =>  'required',
        ], [
            'office_name.required'     =>  'ชื่อเรือนจำต้องไม่ว่าง',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }
        
        $data_office = new office();

        $data_office->office_name = $request->office_name;
        $data_office->save();

        return redirect('/setting_prison');
    }

    public function editDataOffice(Request $request,$id)
    {
        $validate = \Validator::make($request->all(), [
            'office_name'  =>  'required',
        ], [
            'office_name.required'     =>  'ชื่อเรือนจำต้องไม่ว่าง',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }

        $data_office = office::find($id);

        $data_office->office_name = $request->office_name;
        $data_office->save();

        //return redirect('/menu3/'.$request->department_no);
        return redirect('/setting_prison');
    }

    public function deleteDataOffice($id)
    {
        $data_office = office::find($id);
        $data_office->delete();

        return redirect('/setting_prison');
    }

}
