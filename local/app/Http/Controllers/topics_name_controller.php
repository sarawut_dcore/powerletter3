<?php

namespace App\Http\Controllers;
use DB;
use App\topics_name;
use App\topics_detail;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class topics_name_controller extends Controller
{
    public function getIndex( ) {

      $data_topics_name_no = DB ::table('topics_name')->select('no')->get();

      $count_data_topics_name = topics_name::All()->count();

      $data_topics_name_all = DB::table('topics_name')->get();
       

   return view('commnad_prompt/menu', ['_data_topics_name_all' =>  $data_topics_name_all] );

    //return $data_topics_name_all;
    }

    public function getTopics_detail( Request $request,$no_topic ) {

      $data_topics_name_all = DB::table('topics_name')
                              ->where('topics_name.id', '=', $request->no_topics)
                              ->get();

     $data_department_all = DB::table('department')
                              ->get();

      // $topics_detail_all = DB::table('topics_detail')->where('main_no',$request->no_topics)->get();
       
      $topics_detail_all = DB::select('SELECT *, DATE_FORMAT(topics_detail.datecreat,"%d") AS thaiday, DATE_FORMAT(topics_detail.datecreat,"%m") AS thaimount, DATE_FORMAT(topics_detail.datecreat,"%Y") + 543 AS thaiyear FROM topics_detail WHERE topics_detail.main_no = ?', [$request->no_topics]);
      
       return view('commnad_prompt/list_command', ['_topics_detail_all' =>  $topics_detail_all,'_data_topics_name_all' =>  $data_topics_name_all,'_no_topic'=>$no_topic,'_data_department_all'=>$data_department_all] );

    }
    public function add_Topic(Request $request){

      $validate = \Validator::make($request->all(), [
            'topic'  =>  'required',
        ], [
            'topic.required'     =>  'ชื่อหัวข้อหนังสือมอบอำนาจต้องไม่ว่าง',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }
        
      $data_topics_all = new topics_name();
      $data_topics_all->no = $request->count;
      $data_topics_all->topics = $request->topic;
      $data_topics_all->save();

      return redirect('/menu01');
    }
    public function add_Detail(Request $request,$main_no,$no_topics){

      $validate = \Validator::make($request->all(), [
            'topics'  =>  'required',
            'command_no'  =>  'required',
            'datecreat'  =>  'required',
            'name_offer'  =>  'required',
            'name_receive'  =>  'required',
        ], [
            'topics.required'     =>  'เรื่องที่มอบอำนาจต้องไม่ว่าง',
            'command_no.required'     =>  'คำสั่งกรมฯต้องไม่ว่าง',
            'datecreat.required'     =>  'วันที่ไม่ว่าง',
            'name_offer.required'     =>  'ชื่ออธิบดีกรมราชทัณฑ์ต้องไม่ว่าง',
            'name_receive.required'     =>  'ผู้รับมอบอำนาจต้องไม่ว่าง',
        ]);

        if ($validate->fails()) {
            return redirect()->back()->withErrors($validate->errors())->withInput($request->all());
        }
        
      
      $data_detail_all = new topics_detail();
      $data_detail_all->no = $no_topics;
      $data_detail_all->topics = $request->topics;
      $data_detail_all->command_no = $request->command_no;
      $data_detail_all->datecreat = $request->datecreat;
      $data_detail_all->name_offer = $request->name_offer;
      $data_detail_all->name_receive = $request->name_receive;
      $data_detail_all->note = $request->note;
      $data_detail_all->main_no = $main_no;


      
      if ($request->hasFile('add_file')){ 
        $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('add_file')->getClientOriginalExtension();
        $request->file('add_file')->move(public_path('/file'), $filename);
        $data_detail_all->add_file = $filename;
      } 
      $data_detail_all->save();

      return redirect('/menu01/'.$main_no);
    }

    public function updatetopics(Request $request,$id)
    {
        // $data_topics_detail = topics_detail::find($id);

        if ($request->hasFile('image')){ 

            $filename = Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(public_path('/file'), $filename);
            $path_file = $filename;
         } 
         elseif(!($request->hasFile('image'))){
            $path_file = $request->defualt_file;
         }

        
        // $data_topics_detail->topics = $request->topics;
        // $data_topics_detail->command_no = $request->command_no;
        // $data_topics_detail->datecreat = $request->datecreat;
        // $data_topics_detail->name_offer = $request->name_offer;
        // $data_topics_detail->name_receive = $request->name_receive;
        // $data_topics_detail->save();

        DB::update("UPDATE topics_detail SET topics = '$request->topics', 
                                             command_no = '$request->command_no',
                                             datecreat = '$request->datecreat',
                                             name_offer = '$request->name_offer',
                                             name_receive = '$request->name_receive',
                                             add_file = '$path_file'
                                            WHERE topics_detail.topics_detail_id = '$id' ", []);
      
        return redirect('/menu01/'.$request->no_topic);
    }
    
}
