<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\IndicatorChoice
 *
 * @property int $id
 * @property string|null $detail
 * @property int|null $indicator_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorChoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorChoice whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorChoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorChoice whereIndicatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorChoice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class IndicatorChoice extends Model
{
    protected $table = 'indicator_choice';
}
