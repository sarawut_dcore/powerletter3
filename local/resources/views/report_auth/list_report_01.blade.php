@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')



<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        แนวทางการดำเนินงานในการปฏิบัติตามนโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">

                                
                                <table cellspacing="0" border="0">
                                        <colgroup width="519"></colgroup>
                                        <colgroup width="130"></colgroup>
                                        <colgroup width="81"></colgroup>
                                        <tr>
                                            <td style="border-bottom: 1px solid #000000" colspan=3 height="41" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=4 color="#000000">แนวทางการดำเนินงานในการปฏิบัติตามนโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</font></b></td>
                                            </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="40" align="center" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">รายละเอียดการดำเนินงาน</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">ผลการดำเนินงาน</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">เอกสารแนบ</font></b></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="justify" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000">1. นโยบายด้านรัฐ  สังคม  และสิ่งแวดล้อม</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000">แนบเอกสาร</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000"><br><img src="69ea4435214141c7b239ed222fd92fa6_htm_83bf706817772acf.png" width=16 height=21 hspace=32 vspace=3>
                                        </font></b></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="justify" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">1.1 คืนคนดีสู่สังคม</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการส่งเสริมสนับสนุนกิจกรรมด้านการกีฬาผู้ต้องขัง</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการส่งเสริมการเรียนรู้ด้วยตนเองของผู้ต้องขัง</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการส่งเสริมทักษะในการประกอบอาชีพให้แก่ผู้ต้องขัง</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการแสวงหาความร่วมมือกับเครือข่ายประชาสังคม</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการสร้างความร่วมมือในการแก้ไขพัฒนาพฤตินิสัยผู้ต้องขัง</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-โครงการรณรงค์ป้องกันแก้ไขปัญหายาเสพติดและบำบัดฟื้นฟูสมรรถภาพผู้ต้องขังติดยาเสพติด</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจำแนกลักษณะผู้ต้องขังเพื่อนำไปสู่การจัดทำแผนปฏิบัติต่อผู้ต้องขังรายบุคคล</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการสัคคสาสมาธิ/โครงการเรือนจำเรือนธรรม </font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การมุ่งเน้นการเผยแพร่ ประชาสัมพันธ์ผลการดำเนินงานในหน่วยงานกรมราชทัณฑ์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- กิจกรรมการสร้างการความรับรู้ความเข้าใจด้านการพัฒนาพฤตินิสัย</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="justify" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">1.2 ส่งเสริมการมีส่วนร่วมระหว่างชุมชนและเรือนจำ</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการสร้างพันธมิตรเรือนจำ/ทัณฑสถานเพื่อการมีส่วนร่วมของชุมชนรอบเรือนจำ / ทัณฑสถาน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการสัมมนาการดำเนินงานด้านบูรณาการและส่งเสริมความร่วมมือของชุมชนรอบเรือนจำ / ทัณฑสถาน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การสร้างเครือข่ายของเรือนจำ/ทัณฑสถาน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจำแนกลักษณะผู้ต้องขังเพื่อนำไปสู่การจัดทำแผนปฏิบัติต่อผู้ต้องขังรายบุคคล</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการงานสาธารณะนอกเรือนจำ/ทัณฑสถาน </font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการประชาร่วมรัฐพัฒนาผู้ต้องขังสู่ภาคอุตสาหกรรม</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="justify" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">1.3 เรือนจำสีเขียว (Green Area)</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการพัฒนาเรือนจำเข้าสู่ความเป็นเรือนจำสีเขียว</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการปรับสภาพภูมิทัศน์ของเรือนจำให้สวยงาม</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการปลุกจิตสำนึกด้านการรักษาสิ่งแวดล้อมให้แก่บุคลากรในเรือนจำ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- เรือนจำเกษตรกรรม</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-  การดำเนินการคัดแยกขยะและสิ่งปฏิกูล</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- ปลูกต้นไม้และดูแลรักษา</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจัดทำบ่อดักไขมัน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การทำหัวเชื้อจุลินทรีย์ (EM)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="32" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- มาตรฐาน 5 ด้าน ได้แก่ ด้านสุขาภิบาล ด้านสูทกรรม โรงเลี้ยง การกำจัดสิ่งปฏิกูล ด้านเรือนนอน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การรณรงค์ประหยัดกระดาษ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- เป็นผู้ประสานงานระหว่างเรือนจำ/ทัณฑสถาน เพื่อส่งโครงการก่อสร้างต่างๆไปยังกระทรวงยุติธรรม</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- กิจกรรม 5ส.</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจัดระเบียบเรือนจำ ตามนโยบาย 5 ก้าวย่างแห่งการเปลี่ยนแปลงของกรมราชทัณฑ์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="justify" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">1.4 เสริมสร้างและพัฒนาระบบการใช้งบประมาณของทางราชการอย่างมีความคุ้มค่า</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการส่งเสริม และพัฒนาบุคลากรให้มีความรู้และเชี่ยวชาญในกระบวนงานการจัดซื้อจัดจ้าง</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการกำกับติดตาม และตรวจสอบการจัดซื้อจัดจ้าง</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการประเมินคุณธรรมและความโปร่งใสในการดำเนินงานของหน่วยงานภาครัฐ Integrity and Transparency Assessment (ITA) (ในภาพรวมของกรมฯ –กคจ.)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- (ITA) (ในส่วนที่เกี่ยวข้องกับการจัดซื้อจัดจ้าง-กค.)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- รายงานผลการติดตามและประเมินผลการใช้งบประมาณ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- มาตรการประหยัดพลังงาน (ไฟฟ้าและน้ำมันเชื้อเพลิง)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การใช้พลังงานทดแทน เช่น โซล่าเซลล์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การใช้กระดาษรีไซเคิล</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- มาตรการประหยัดพลังงาน (ไฟฟ้าและน้ำมันเชื้อเพลิง)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การรายงานผลการปฏิบัติงานทางระบบอิเล็กทรอนิกส์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- มาตรการเร่งรัดการเบิกจ่ายเงินงบประมาณ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="justify" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">1.5 มุ่งมั่นรักษาและส่งเสริมคุณภาพสิ่งแวดล้อมอันเกิดจากการดำเนินงาน</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการปลูกป่า เช่น ป่าชายเลน ปลูกป่าเฉลิมพระเกียรติ พัฒนาทรัพยากรธรรมชาติ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการให้ความรู้สู่ชุมชน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การกำหนดวิธีการจัดการกับสิ่งของที่ไม่อนุญาตให้เก็บรักษาไว้ในเรือนจำ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">1.6 มุ่งมั่นที่จะเป็นส่วนหนึ่งในการสร้างสรรค์สังคมที่ดีมีความสุข</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="81" align="justify" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">'-ระดับความสำเร็จของการดำเนินการจัดกิจกรรมเพื่อการสร้างสรรค์และช่วยเหลือสังคมอย่างสม่ำเสมอและต่อเนื่องโดยครอบคลุมการสนับสนุนจารีตประเพณีและศิลปวัฒนธรรมที่ดีงามของสังคมไทยและการอนุรักษ์ทรัพยากรธรรมชาติ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="justify" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">1.7 มุ่งมั่นรักษาผลประโยชน์ของรัฐไว้สูงสุด</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="justify" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- ระดับความสำเร็จของการใช้ประโยชน์ที่ดินเรือนจำ/ทัณฑสถานให้เกิดประโยชน์สูงสุด และการป้องการบุกรุกที่ดินของเรือนจำ/ทัณฑสถาน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="justify" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000">2. นโยบายด้านผู้รับบริการและผู้มีส่วนได้ส่วนเสีย</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000">แนบเอกสาร</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#F4B183"><font face="TH SarabunIT๙" color="#000000"><br><img src="69ea4435214141c7b239ed222fd92fa6_htm_83bf706817772acf.png" width=22 height=21 hspace=30 vspace=3>
                                        </font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">2.1 เสริมสร้างมาตรฐานการให้บริการอย่างมีประสิทธิภาพ</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำมาตรฐานของกระบวนงานของกรมราชทัณฑ์ (SOP)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การดำเนินการตามคู่มือสำหรับประชาชน ตาม พ.ร.บ.การอำนวยความสะดวกในการพิจารณาอนุญาตของทางราชการ พ.ศ. 2558</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การสร้างความรับรู้ ความเข้าใจของกระบวนงานของราชทัณฑ์ ในรูปแบบ Application / Info graphic</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">2.2 มุ่งมั่นในการให้บริการเพื่อตอบสนองความต้องการของผู้รับบริการและผู้มีส่วนได้ส่วนเสีย</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำข้อมูลด้านการให้บริการประชาชนผ่านเว็บไซต์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการให้บริการรับเรื่องราวร้องทุกข์ จากประชาชน ผ่านเว็บไซต์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจัดให้มี call center / การจองเยี่ยมผ่าน application (IOS) เพื่อให้บริการประชาชน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">2.3 การจัดระบบการแจ้งข้อมูลทุจริตผ่านทางเว็บไซต์ </font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำระบบการแจ้งข้อมูลทุจริตผ่านทางเว็บไซต์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำระบบการบริหารจัดการข้อร้องเรียนผ่านระบบอิเล็กทรนิกส์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">2.4 การส่งเสริมสนับสนุนแนวความคิดใหม่ๆ ตลอดจนการนำเทคโนโลยีสมัยใหม่มาประยุกต์ใช้ในการพัฒนาการให้บริการเพื่อความสะดวกและทันสมัย</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการ/มาตรการเผยแพร่ข้อมูล เช่น ข้อมูลด้านการบริการประชาชน ข้อมูลคำสั่งมอบอำนาจ การเผยแพร่ข้อมูลผ่าน e-book</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-  การรายงานผลการประเมินการปฏิบัติราชการตามมาตรการปรับปรุงประสิทธิภาพในการปฏิบัติราชการ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-  การจัดการเรียนรู้ให้แก่บุคลากรผ่านระบบ E-learning </font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-  การจัดประชุมผู้บริหารกรมราชทัณฑ์ผ่าน VDO Conference</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการระบบรายงาน ระบบสืบค้น และจัดเก็บข้อมูลคำสั่งมอบอำนาจ ของกรมราชทัณฑ์ผ่านระบบอิเล็กทรอนิกส์ (e-library for Authorization)  </font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- ศูนย์ปฏิบัติการและติดตามสถานการณ์กรมราชทัณฑ์  (ศปก.รท.)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000">3. นโยบายด้านองค์การ</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000">แนบเอกสาร</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#F4B183"><font face="TH SarabunIT๙" color="#000000"><br><img src="69ea4435214141c7b239ed222fd92fa6_htm_83bf706817772acf.png" width=22 height=21 hspace=30 vspace=3>
                                        </font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">3.1 พัฒนาระบบการบริหารความเสี่ยงและระบบการควบคุมภายในของหน่วยงาน</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำมาตรฐานของกระบวนงานของกรมราชทัณฑ์ (SOP)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การดำเนินการตามคู่มือสำหรับประชาชน ตาม พ.ร.บ.การอำนวยความสะดวกในการพิจารณาอนุญาตของทางราชการ พ.ศ. 2558</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การสร้างความรับรู้ ความเข้าใจของกระบวนงานของราชทัณฑ์ ในรูปแบบ Application / Info graphic</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">3.2 ส่งเสริมระบบคุณธรรมจริยธรรมในการปฏิบัติงาน</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำข้อมูลด้านการให้บริการประชาชนผ่านเว็บไซต์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการให้บริการรับเรื่องราวร้องทุกข์ จากประชาชน ผ่านเว็บไซต์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจัดให้มี call center / การจองเยี่ยมผ่าน application (IOS) เพื่อให้บริการประชาชน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">3.3 สร้างระบบผลการปฏิบัติงานที่มีประสิทธิภาพและเป็นธรรม</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำระบบการแจ้งข้อมูลทุจริตผ่านทางเว็บไซต์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำระบบการบริหารจัดการข้อร้องเรียนผ่านระบบอิเล็กทรนิกส์</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">3.4 การขับเคลื่อนการพัฒนาและการใช้นวัตกรรมในงานราชทัณฑ์</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการ/มาตรการเผยแพร่ข้อมูล เช่น ข้อมูลด้านการบริการประชาชน ข้อมูลคำสั่งมอบอำนาจ การเผยแพร่ข้อมูลผ่าน e-book</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-  การรายงานผลการประเมินการปฏิบัติราชการตามมาตรการปรับปรุงประสิทธิภาพในการปฏิบัติราชการ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-  การจัดการเรียนรู้ให้แก่บุคลากรผ่านระบบ E-learning </font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-  การจัดประชุมผู้บริหารกรมราชทัณฑ์ผ่าน VDO Conference</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการระบบรายงาน ระบบสืบค้น และจัดเก็บข้อมูลคำสั่งมอบอำนาจ ของกรมราชทัณฑ์ผ่านระบบอิเล็กทรอนิกส์ (e-library for Authorization)  </font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- ศูนย์ปฏิบัติการและติดตามสถานการณ์กรมราชทัณฑ์  (ศปก.รท.)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000">4.นโยบายด้านผู้ปฏิบัติงาน</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#F4B183"><b><font face="TH SarabunIT๙" size=4 color="#000000">แนบเอกสาร</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#F4B183"><font face="TH SarabunIT๙" color="#000000"><br><img src="69ea4435214141c7b239ed222fd92fa6_htm_83bf706817772acf.png" width=22 height=21 hspace=30 vspace=3>
                                        </font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">4.1 ส่งเสริมการบริหารและพัฒนาทรัพยากรบุคคลอย่างเป็นระบบ และต่อเนื่อง</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการเสริมสร้างความรับผิดชอบต่อผลการกระทำ (Accountability) ที่เกิดจากความประพฤติและการปฏิบัติงาน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการส่งเสริมศักยภาพการเรียนรู้</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการจัดทำมาตรฐานด้านการดูแลคุณภาพชีวิตและการทำงานของเจ้าหน้าที่</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการเสริมสร้างและพัฒนาธรรมาภิบาลผู้ปฏิบัติงาน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- โครงการพัฒนาขีดสมรรถนะในด้านการพัฒนาระบบราชการแก่ผู้ปฏิบัติงาน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">-โครงการผู้คุมดีเด่น</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- มาตรการการส่งเสริมการให้บริการที่ดีและมีประสิทธิภาพของเจ้าหน้าที่</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- พัฒนาระบบฐานข้อมูลสถิติของงานราชทัณฑ์ให้มีประสิทธิภาพ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">X</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">4.2 การให้ความสำคัญกับการพัฒนาทรัพยากรบุคคล</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจัดทำแผนแม่บทการส่งเสริมความเสมอภาคระหว่างหญิงชาย</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- จัดทำหลักสูตรอบรมสำหรับเจ้าหน้าที่ที่ปฏิบัติงานเฉพาะด้าน</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจัดการเรียนรู้จากรุ่นพี่สู้รุ่นน้อง (Coaching)</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=top bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">4.3 ปฏิบัติต่อผู้ปฏิบัติงานอย่างเป็นธรรมเสมอภาค บนพื้นฐานคุณธรรมจริยธรรม</font></b></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- กองทุนสวัสดิการข้าราชการ</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- รางวัลผู้คุมดีเด่น</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- รางวัลพะธำมะรง</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การจัดหาอุปกรณ์เครื่องมือเครื่องใช้ที่มีความจำเป็นสำหรับการปฏิบัติงานของเจ้าหน้าที่</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- การบำรุงรักษาอุปกรณ์เครื่องมือเครื่องใช้ที่มีความจำเป็นสำหรับการปฏิบัติงานของเจ้าหน้าที่</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">- อื่นๆ (โปรดระบุ)............................................................................................................................</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" color="#000000">√</font></td>
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" color="#000000"><br></font></td>
                                        </tr>
                            </table>


                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            
        </div>

</div> 
 <!-- /. end Page -->






@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection