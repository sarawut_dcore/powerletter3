@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')
    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">เมนูเลือกตัวชี้วัดแต่ละข้อเพื่อตรวจและประเมินผล</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดเรือนจำ (รจ.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
              </div>
              <ul class="list-group list-group-lg m-b-none">
                  @foreach($_menu_9001 as $_menu_9001_)
                    <li class="list-group-item">
                      <a href = "{{ url('/evaluation/').'/'.$_menu_9001_->id }}" >{{ $_menu_9001_->detail }}</a>
                    </li>
                  @endforeach
              </ul>

            </div>
          </div>
        <!--  2  -->   
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
    
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดกลุ่มงานคุ้มครองจริยธรรมกรมราชทัณฑ์ (กคจ.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9002 as $_menu_9002_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9002_->id }}" >{{ $_menu_9002_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
    
              </div>
          </div>
        <!--  3  --> 
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
  
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดกองแผนงาน (กผ.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9003 as $_menu_9003_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9003_->id }}" >{{ $_menu_9003_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
  
              </div>
          </div>
        <!--  4  -->    
          <div class="col-md-12">
                <div class="panel panel-success">
                  <div class="panel-heading wrapper b-b b-light">
      
                    <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดกองบริการทางการแพทย์ (กพ.) ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                  </div>
                  <ul class="list-group list-group-lg m-b-none">
                      @foreach($_menu_9004 as $_menu_9004_)
                        <li class="list-group-item">
                          <a href = "{{ url('/evaluation/').'/'.$_menu_9004_->id }}" >{{ $_menu_9004_->detail }}</a>
                        </li>
                      @endforeach
                  </ul>
      
                </div>
          </div>
        <!--  5  --> 
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
  
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดหน่วยตรวจสอบภายใน (ตน.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9005 as $_menu_9005_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9005_->id }}" >{{ $_menu_9005_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
  
              </div>
          </div>
        <!--  6  -->    
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
    
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดสำนักทัณฑวิทยา (สทว.) ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9006 as $_menu_9006_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9006_->id }}" >{{ $_menu_9006_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
    
              </div>
          </div>
        <!--  7  -->  
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
  
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดสำนักทัณฑปฏิบัติ (สทป.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9007 as $_menu_9007_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9007_->id }}" >{{ $_menu_9007_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
  
              </div>
          </div>
        <!--  8  -->    
          <div class="col-md-12">
                <div class="panel panel-success">
                  <div class="panel-heading wrapper b-b b-light">
      
                    <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดกองการเจ้าหน้าที่ (กจ.) ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                  </div>
                  <ul class="list-group list-group-lg m-b-none">
                      @foreach($_menu_9008 as $_menu_9008_)
                        <li class="list-group-item">
                          <a href = "{{ url('/evaluation/').'/'.$_menu_9008_->id }}" >{{ $_menu_9008_->detail }}</a>
                        </li>
                      @endforeach
                  </ul>
      
                </div>
          </div>
        <!--  9  -->                     
        
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
  
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดสำนักผู้ตรวจราชการกรม (สผต.) ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9009 as $_menu_9009_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9009_->id }}" >{{ $_menu_9009_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
  
              </div>
          </div>
        <!--  10  -->    
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
    
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดกองนิติการ (กน.) ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9010 as $_menu_9010_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9010_->id }}" >{{ $_menu_9010_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
    
              </div>
          </div>
        <!--  11  -->  
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
  
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดกลุ่มพัฒนาระบบบริหาร (กพร.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9011 as $_menu_9011_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9011_->id }}" >{{ $_menu_9011_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
  
              </div>
          </div>
        <!--  12  -->    
          <div class="col-md-12">
                <div class="panel panel-success">
                  <div class="panel-heading wrapper b-b b-light">
      
                    <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดสถาบันพัฒนาข้าราชการราชทัณฑ์ (สพ.) ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                  </div>
                  <ul class="list-group list-group-lg m-b-none">
                      @foreach($_menu_9012 as $_menu_9012_)
                        <li class="list-group-item">
                          <a href = "{{ url('/evaluation/').'/'.$_menu_9012_->id }}" >{{ $_menu_9012_->detail }}</a>
                        </li>
                      @endforeach
                  </ul>
      
                </div>
          </div>
        <!--  13  -->
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
  
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดกองคลัง (กค.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9013 as $_menu_9013_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9013_->id }}" >{{ $_menu_9013_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
  
              </div>
          </div>
        <!--  14  -->    
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
    
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดสำนักพัฒนาพฤตินิสัย (สพน.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9014 as $_menu_9014_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9014_->id }}" >{{ $_menu_9014_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
    
              </div>
          </div>
        <!--  15  -->  
          <div class="col-md-12">
              <div class="panel panel-success">
                <div class="panel-heading wrapper b-b b-light">
  
                  <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัดสำนักเลขานุการกรม (สล.)  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
                </div>
                <ul class="list-group list-group-lg m-b-none">
                    @foreach($_menu_9015 as $_menu_9015_)
                      <li class="list-group-item">
                        <a href = "{{ url('/evaluation/').'/'.$_menu_9015_->id }}" >{{ $_menu_9015_->detail }}</a>
                      </li>
                    @endforeach
                </ul>
  
              </div>
          </div>
                          
        
        
        
        
        
        
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
