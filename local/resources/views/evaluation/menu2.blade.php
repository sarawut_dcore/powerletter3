@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')
    <!-- content -->
    @php
       $_menu_9020
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">เมนูเลือกตัวชี้วัดแต่ละข้อเพื่อตรวจและประเมินผล</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted">ตัวชี้วัด กระทรวจยุติธรรม  ประจำปีงบประมาณ พ.ศ. ๒๕๖๑</h5>              
              </div>
              <ul class="list-group list-group-lg m-b-none">
                  @foreach($_menu_9020 as $_menu_9020_)
                    <li class="list-group-item">
                      <a href = "{{ url('/evaluation2/').'/'.$_menu_9020_->id }}" >{{ $_menu_9020_->detail }}</a>
                    </li>
                  @endforeach
              </ul>

            </div>
          </div>

        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
