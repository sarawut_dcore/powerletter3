
<!DOCTYPE html>
<html lang="en" class="">
<head>
    <meta charset="utf-8" />
    <title>เข้าสู่ระบบ - {{ Config::get('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/animate.css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/simple-line-icons/css/simple-line-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/jquery/bootstrap/dist/css/bootstrap.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ url('assets/css/font.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/css/app.css') }}" type="text/css" />
</head>
<body>
<div class="app app-header-fixed ">
    <div class="container">
        <a href class="navbar-brand block m-t "> ระบบฐานข้อมูลการมอบอำนาจ การปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h2></a>
        <br>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                @if($errors->all())
                    <div class="alert alert-danger">
                        {{ $errors->first() }}
                    </div>
                @endif
                <div class="panel panel-default" align="center">
                    <br>
                    <img id="profile-img" class="profile-img-card" src="{{ url('assets/img/logo.jpg') }}" />
                    <br>
                    <br>
                    <div class="panel-heading font-bold "  >เข้าใช้งานระบบโดยใส่ข้อมูล ชื่อผู้ใช้งาน(User),รหัสผ่าน(Password) </div>

                    <div class="panel-body">
                        <form action="{{ url('login') }}" method="post" class="bs-example form-horizontal ng-pristine ng-valid">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-lg-2 control-label">ชื่อผู้ใช้งาน</label>
                                <div class="col-lg-10">
                                    <input type="text" name="username" class="form-control" placeholder="ชื่อผู้ใช้งาน">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">รหัสผ่าน</label>
                                <div class="col-lg-10">
                                    <input type="password" name="password" class="form-control" placeholder="รหัสผ่าน">
                                </div>
                            </div>

                            <div class="form-group" align="center">
                                <div  class="col-lg-12" >
                                    <button type="submit" class="btn btn-lm btn-info"  >เข้าสู่ระบบ</button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ url('assets/libs/jquery/jquery/dist/jquery.js') }}"></script>
<script src="{{ url('assets/libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src="{{ url('assets/js/ui-load.js') }}"></script>
<script src="{{ url('assets/js/ui-jp.config.js') }}"></script>
<script src="{{ url('assets/js/ui-jp.js') }}"></script>
<script src="{{ url('assets/js/ui-nav.js') }}"></script>
<script src="{{ url('assets/js/ui-toggle.js') }}"></script>
<script src="{{ url('assets/js/ui-client.js') }}"></script>
</body>
</html>
