@extends('layouts.master')

@section('title', 'นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์')

@section('content')
    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->        
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted"> นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</h5>              
              </div>
                                 <!-- /.panel-heading -->
                                 <div class="panel-body">
                                    <div class="table-responsive">
                                        <table cellspacing="0" border="0" class="table table-striped table-bordered table-hover">
                                            <colgroup width="67"></colgroup>
                                            <colgroup width="648"></colgroup>
                                            <colgroup width="94"></colgroup>
                                            <colgroup width="94"></colgroup>
                                           
                                                <thead>
                                                    <tr>
                                                        <td colspan=4 height="35" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=5 color="#000000">นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</font></b></td>
                                                    </tr>
                                                    <tr>
                                                        <th><h4 align="center">ลำดับ</h4></th>
                                                        <th><h4 align="left">รายละเอียดการดำเนินงาน</h4></th>
                                                        <th><h4 align="center">เอกสาร</h4></th>
                                                        <th><h4 align="center">บันทึก</h4></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                           

                                            @foreach($_datapolicy_menu as $out_datapolicy_menu)
                                            {{ Form::open(array('url' => '/menu2/savefile', 'files' => true, 'method' => 'post')) }}
                                            <tr>
                                                <td align="center">{{ $out_datapolicy_menu->no }}</td>
                                                <td >{{ $out_datapolicy_menu->menu_detail }} &nbsp
                                                     @if($out_datapolicy_menu->type_file != "")
                                                     
                                                        <span class="fa fa-file-pdf-o text-danger m-r-sm" style="font-size: 20px;"></span>
                                                        <a href="{{ url('/local/public/file/').'/'.$out_datapolicy_menu->type_file }} "> {{ $out_datapolicy_menu->type_file }}
                                                        </a>
                                                        <br>
                                                     
                                                     @endif
                                                </td>
                                                <td><br></td>
                                                <td><br></td>
                                            </tr>
                                            <tr>
                                                <td ><br> <input type="hidden" id="id" name="id" value="{{ $out_datapolicy_menu->id }}"></td>
                                                <td >{{ $out_datapolicy_menu->sub_detail }}</td>
                                                
                                                <td >
                                                    {{ Form::file('image') }}
                                                    <!--<input type="file" name="file" />-->
                                                    <div class="m-t">
                                                        <span class="fa fa-file-pdf-o text-danger m-r-sm" style="font-size: 20px;"></span>
                                                        <label>ชื่อไฟล์ :</label>
                                                        @foreach($_datapolicy_file as $out_datapolicy_file)
                                                            @if($out_datapolicy_menu->id == $out_datapolicy_file->policy_menu_id)
                                                                <a href="{{ url('/local/public/file/').'/'.$out_datapolicy_file->add_file }} "> {{ $out_datapolicy_file->add_file }}</a>
                                                            @endif
                                                        @endforeach
                                                        <br>
                                                    </div>
                                                </td>
                                                <td><button value=" Send" class="btn btn-success" type="submit" id="submit"> บันทึก</button></td>
                                            </tr>
                                            {{ form::close() }}
                                            @endforeach


                                            <tr>
                                                <td ><br></td>
                                                <td  align="right" valign=middle><b><u><font size=4 color="#000000"><a href = "{{ url('/list_policy_01') }}" > ส่งรายงานผลการดำเนินการ</td></font>
                                                <td ><br></u></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->

            </div>
          </div>
       
        
        
        
        
        
        
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
