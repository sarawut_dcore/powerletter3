<!DOCTYPE html>
<html lang="en" class="">
<head>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') - Powerletter</title>
    <!--{{ Config::get('app.name') }}-->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/animate.css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/simple-line-icons/css/simple-line-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/jquery/bootstrap/dist/css/bootstrap.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ url('assets/css/font.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/css/app.css') }}" type="text/css" />
    
    <link rel="stylesheet" href="{{ url('https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css') }}" type="text/css" />

   

</head>
<body>
{{--<div class="app app-header-fixed app-aside-dock">--}}
<div class="app app-header-fixed">
    @include('layouts.header')
    @include('layouts.sidebar')
    @yield('content')
    @include('layouts.footer')
</div>
<script src="{{ url('js/app.js') }}"></script>
<script src="{{ url('assets/libs/jquery/jquery/dist/jquery.js') }}"></script>
<script src="{{ url('assets/libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src="{{ url('assets/js/ui-load.js') }}"></script>
<script src="{{ url('assets/js/ui-jp.config.js') }}"></script>
<script src="{{ url('assets/js/ui-jp.js') }}"></script>
<script src="{{ url('assets/js/ui-nav.js') }}"></script>
<script src="{{ url('assets/js/ui-toggle.js') }}"></script>
<script src="{{ url('assets/js/ui-client.js') }}"></script>
<script src="{{ url('https://code.jquery.com/jquery-3.3.1.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js') }}"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBHOeEfiuP7-dWQATq4ldQo_JNaPyakqKI" ></script>

@yield('script')
</body>
</html>
