        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                <a class="navbar-brand" href="index.html">ระบบฐานข้อมูลการมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">

                <!-- /.dropdown -->

                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                                            </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> ข้อมูลผู้ใช้</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> ตั้งค่าผู้ใช้งาน</a>
                        </li>
                        <li class="divider"></li>
                        <li  class="{{ Request::is('login') ? 'active' : null }}">
                            <a href="{{ url('/login') }}">
                            <i class="fa fa-sign-out fa-fw"></i>ออกจากระบบ</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>

                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <!--
                        <div style="padding:20px 5px">
                            <center>
                                <div style="border:1px solid grey; border-radius: 50% ; height: 70px;width: 70px"></div>
                                <h4 style="color:#E0E0E0">นาย อนุรักษ์ ศรีเจริญ<br>
                                    <small style="color:#8eaece">เจ้าหน้าที่ดูแลระบบ</small>
                                </h4>
                            </center>
                        </div>
                        -->
                        <li  class="{{ Request::is('dashboard') ? 'active' : null }} ">
                            <a href="{{ url('/') }}">
                                <i class="glyphicon glyphicon-dashboard fa-fw"></i>ภาพรวมระบบ</a>
                        </li>
                        <li>
                            <a href="{{ url('/menu01') }}"><i class="fa fa-check-square-o fa-fw"></i>ข้อมูลคำสั่งมอบอำนาจ</a>
                            
                        </li>
                        <li>
                            <a href="{{ url('/menu2') }}"><i class="fa fa-check-square-o fa-fw"></i>นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</a>
                            
                        </li>
                        <li>
                                <a href="{{ url('/menu3') }}"><i class="fa fa-check-square-o fa-fw"></i>รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</a>
                                
                        </li>



                        <li>
                            <a href="#"><i class="glyphicon glyphicon-list-alt   fa-fw"></i> รายงาน<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#">รายงานการเรืองที่มอบอำนาจ</a>
                                </li>
                                <li>
                                    <a href="#">รายงานการมอบอำนาจของเรือนจำ</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cog fa-fw"></i>ตั้งค่าเอกสารในระบบ<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="setting_role.html">ตั้งค่าหนังสือที่มอบอำนาจ</a>
                                </li>
                                <li>
                                    <a href="setting_access.html">ตั้งค่าหัวข้อรายงาน</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-cog fa-fw"></i>ตั้งค่าผู้ใช้งาน<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="setting_role.html">ตั้งค่าสิทธิผู้ใช้งาน</a>
                                </li>
                                <li>
                                    <a href="setting_access.html">ตั้งค่าการเข้าถึงของแต่ละสิทธิ</a>
                                </li>
                                <li>
                                    <a href="setting_access.html">ตั้งค่าเรือนจำ</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li> 
                        <li  class="{{ Request::is('login') ? 'active' : null }}">
                                <a href="{{ url('/login') }}">
                                <i class="glyphicon glyphicon-dashboard fa-fw"></i>ออกจากระบบ</a>
                        </li>                       
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>


            <!-- /.navbar-static-side -->
        </nav>