<!-- footer -->
<footer style="z-index: 0;" class="app-footer">
    <div class="wrapper b-t bg-white-only">
        &copy; 2018 {{ Config::get('app.name') }}.
    </div>
</footer>
<!-- / footer -->