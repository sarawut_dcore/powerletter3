<!-- aside -->
<aside class="app-aside hidden-xs bg-black">
    <div class="aside-wrap">
        <div class="navi-wrap">
            <!-- nav -->
            <nav ui-nav class="navi clearfix">
                <ul class="nav">
                    <li class="hidden-folded padder m-t m-b-sm text-muted text-xs ">
                        <i class="glyphicon glyphicon-th-large"></i>
                        <span>เมนู</span>
                    </li>
                     <!--------------------- menu head ----------------------- -->
                    <li class="{{ Request::is('/') ? 'active' : null }}">  
                        <a href="{{ url('/') }}">
                            <i class="glyphicon glyphicon-home"></i>
                            <span>ภาพรวมระบบ</span>
                        </a>
                    </li>
                     <!--------------------- menu head ----------------------- -->
                    <li class="{{ Request::is('indicator2') ? 'active' : null }}">
                        <a href="{{  url('/menu01') }}">
                            
                            <i class="glyphicon  glyphicon-pencil"></i>
                            <span>ข้อมูลคำสั่งมอบอำนาจ</span>
                        </a>
                    </li>  
                     <!--------------------- menu head ----------------------- -->
                    <li class="{{ Request::is('indicator') ? 'active' : null }}">
                        <a href="{{ url('/menu2') }}">
                            
                            <i class="glyphicon  glyphicon-pencil"></i>
                            <span>นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</span>
                        </a>
                    </li>                                      
                     <!--------------------- menu head ----------------------- -->
                    <li class="{{ Request::is('evaluation') ? 'active' : null }}">
                        <a href="{{ url('/menu3') }}">
                            <i class="glyphicon glyphicon-check"></i>
                            <span>รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</span>
                        </a>

                    </li> 
                    @if(Auth::user()->group == 1)
                        <li class="{{ Request::is('mainreport') ? 'active' : null }}">
                            <a href="#">
                                <span class="pull-right text-muted">
                                <i class="fa fa-fw fa-angle-right text"></i>
                                <i class="fa fa-fw fa-angle-down text-active"></i>
                                </span>
                                <b class="badge bg-info pull-right"></b>
                                <i class="glyphicon glyphicon-check"></i>
                                <span>รายงาน</span>
                            </a>
                            <ul class="nav nav-sub dk">
                                    <li class="nav-sub-header">
                                        <a href>
                                            <span></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/menu4') }}">
                                            <span>รายงานสรุป นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/menu5') }}">
                                            <span>รายงานสรุป รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</span>
                                        </a>
                                    </li>
                                
                                                                                                                                                                                                                                            
                                </ul>
                        </li>
                     @endif                                           
                    <!--------------------- menu head and sub----------------------- -->
                    <li>
                        <a href class="auto">      
                            <span class="pull-right text-muted">
                               <i class="fa fa-fw fa-angle-right text"></i>
                               <i class="fa fa-fw fa-angle-down text-active"></i>
                            </span>
                               <b class="badge bg-info pull-right"></b>
                                <i class="glyphicon  glyphicon-book"></i>
                          <span>ตั้งค่าผู้ใช้งาน</span>
                        </a>
                                <ul class="nav nav-sub dk">
                                    <li class="nav-sub-header">
                                        <a href>
                                            <span></span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/setting_type_user') }}">
                                            <span>ตั้งค่าสิทธิผู้ใช้งาน</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/setting_user') }}">
                                            <span>ตั้งค่าการเข้าถึงของแต่ละสิทธิ</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('/setting_prison') }}">
                                            <span>ตั้งค่าเรือนจำ</span>
                                        </a>
                                    </li>
                                                                                                                                                                                                                                            
                                </ul>
                      </li>
                       <!--------------------- end  menu head and sub----------------------- -->

                    </ul>
            </nav>
            <!-- nav -->
        </div>
    </div>
</aside>
<!-- / aside -->