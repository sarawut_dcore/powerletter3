@extends('layouts.master')

@section('title', 'รายชื่อหน่วยงานในสังกัดกรมราชทัณฑ์ที่แจ้งการลงนามรับทราบการปฏิบัติ')

@section('content')
    <!-- content -->

    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div align="center">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์ <br>  
                    ตามนโยบายกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์ ประจำปี พ.ศ. 2560 ของเจ้าหน้าที่ในสังกัด รจ./ทส.</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-left">
                    <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์ 
                      
                    </h5>              
               
                </div>


                    <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="table-responsive">
    
                                    
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th align="center">ลำดับ</th>
                                            <th align="center">เรือนจำ/ทัณฑสถาน</th>
                                            <th align="center">กลุ่ม</th>
                                            <th align="center">รายงานผล เเบบที่2</th>
                                            <th align="center">รายงานผล เเบบที่3</th>
                                            <th align="center">ไม่รายงานผล</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                     <?php $count = 0; ?> 
                                    @foreach($_datauser as $out_datauser)
                                        <tr>
                                            <td align="center">  
                                                {{ ++$count }}
                                            </td>
                                            <td> 
                                                {{$out_datauser->office_name}}
                                            </td>
                                            <td align="center">
                                                {{$out_datauser->zone}}
                                            </td>
                                            <td align="center">
                                               <i class="ace-icon fa fa-check"></i>
                                            </td>
                                            <td align="center">
                                               <i class="ace-icon fa fa-check"></i>
                                            </td>
                                            <td>
                                               
                                            </td>
                                        </tr>
                                    @endforeach  
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->



            </div>
          </div>
       
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
