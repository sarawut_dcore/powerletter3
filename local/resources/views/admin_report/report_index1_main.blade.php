@extends('layouts.master')

@section('title', 'ายชื่อหน่วยงานในสังกัดกรมราชทัณฑ์ที่แจ้งการลงนามรับทราบการปฏิบัติ')

@section('content')
    <!-- content -->

    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                 <div align="center">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์ <br>  
                    ตามนโยบายกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์ ประจำปีพุทธศักราช ของเจ้าหน้าที่ในสังกัด รจ./ทส.</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">          
              </div>
                                 <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="table-responsive">
                                    <table id="example" cellspacing="0" border="0" class="table table-striped table-bordered" style="width:100%">
                                            <colgroup width="56"></colgroup>
                                            <colgroup width="362"></colgroup>
                                            <thead>
                                                <tr>
                                                    <td colspan=3 height="35" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=5 color="#000000">รายชื่อหน่วยงานในสังกัดกรมราชทัณฑ์ที่แจ้งการลงนามรับทราบการปฏิบัติ</font></b></td>
                                                </tr>
                                                <tr>
                                                    <th ><h4 align="center">ลำดับ</h4></td>
                                                    <th  ><h4 align="center">หน่วยงาน</h4></td>
                                                </tr>
                                            </thead>
                                            <?php $count = 0; ?>
                                            @foreach($_report as $out_report)
                                                <tr>
                                                    <td   align="center" >{{ ++$count }}</td>
                                                    <td  valign=middle> <font size=3 color="#000000"> <a href = "{{ url('menu4/').'/'.$out_report->num_1 }}" >{{  $out_report->detail }} </font> </td>
                                                </tr>
                                            @endforeach
                                        </table>
    
    
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
            </div>
          </div>
        
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection

