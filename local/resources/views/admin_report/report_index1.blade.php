@extends('layouts.master')

@section('title', 'รายชื่อหน่วยงานในสังกัดกรมราชทัณฑ์ที่แจ้งการลงนามรับทราบการปฏิบัติ')

@section('content')
    <!-- content -->

    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div align="center">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์ <br>  
                    ตามนโยบายกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์ ประจำปี พ.ศ. 
                    @if($_year += '0')
                         <?php
                         $newyear = ($_year + 543);
                         ?>
                         {{$newyear}}
                    @endif
                    
                    ของเจ้าหน้าที่ในสังกัด รจ./ทส.</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
        @foreach($_report2 as $out_report2)
         {{ Form::open(['method' => 'post' , 'url' => '/menu4_search/'.$out_report2->num_1.'/search']) }}
       
        <div class="row">

            <div class= col-sm-2 ">
                        <select class="form-control" id="year" name="year">
                            <option value=" ">เลือกปี</option>
                            <option value="2016">2559</option>
                            <option value="2017">2560</option>
                            <option value="2018">2561</option>
                            <option value="2019">2562</option>
                            <option value="2020">2563</option>
                            <option value="2021">2564</option>
                            <option value="2022">2565</option>
                            <option value="2023">2566</option>
                            <option value="2024">2567</option>
                            <option value="2025">2568</option>
                            <option value="2026">2569</option>
                            <option value="2027">2570</option>
                        </select> 
            </div>
             <!--
            <div class= col-sm-2 ">
                <input class="form-control" type="date" id="st_date" name="st_date"  >
            </div>
            -->
            <div class= col-sm-2 ">
                 <button type="submit" class="btn btn-sm btn-success">&nbsp;&nbsp;ค้นหา</button>
            </div>

        </div> 
         {{ Form::close() }} 
        @endforeach   
      <!-- users -->
        <div class="row">
        
          <!--  1  -->  
            <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading wrapper b-b b-left">
                        @foreach(array_slice($_report1, 0, 1) as $report1)
                            @if($report1->num_2 == '0')
                                <h3>{{$report1->num_1 }}. {{$report1->detail }}</h3> 
                            @endif   
                        @endforeach
                    </div>

                    <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="table-responsive"> 
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th align="center">มาตรการ/โครงการ รองรับ</th>
                                            <th align="center">รายชื่อเรือนจำ ทัณฑสถาน สถานกักกัน สถานกักขัง ที่มีผลการดำเนินงาน</th>
                                            <th align="center">ปี-เดือน-วัน ที่รายงาน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($_report1 as $out_report1)
                                        <tr>
                                            <td>  
                                               {{  $out_report1->detail }}
                                            </td>
                                            <td> 
                                               {{  $out_report1->office_name }}
                                            </td>
                                            <td> 
                                               {{  $out_report1->updated_at }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                    </div>
                     <!-- /.panel-body -->
                </div>
            </div>
           <!-- 1 -->  
        </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
