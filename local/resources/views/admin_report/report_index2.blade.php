@extends('layouts.master')

@section('title', 'รายงานสรุป มอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์ ')

@section('content')
    <!-- content -->

    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div align="center">
                    <h1 class="m-n font-thin h3 text-black">  
                    ตารางสรุปผลเเบบรายการใช้อำนาจของผู้รับมอบอำนาจในการปฎิบัติราชการเเทนอธิบดีกรมราชทัณฑ์ <br>
                     ประจำเดือน 
                    @if($_month == '1')
                           {{'มกราคม'}}
                    @elseif($_month == '2')
                           {{'กุมภาพันธ์'}}
                    @elseif($_month == '3')
                           {{'มีนาคม'}}
                    @elseif($_month == '4')
                           {{'เมษายน'}}
                    @elseif($_month == '5')
                           {{'พฤษภาคม'}}
                    @elseif($_month == '6')
                           {{'มิถุนายน'}}
                    @elseif($_month == '7')
                           {{'กรกฎาคม'}}
                    @elseif($_month == '8')
                           {{'สิงหาคม'}}
                    @elseif($_month == '9')
                           {{'กันยายน'}}
                    @elseif($_month == '10')
                           {{'ตุลาคม'}}
                    @elseif($_month == '11')
                           {{'พฤศจิกายน'}} 
                    @elseif($_month == '12')
                           {{'ธันวาคม'}}       
                    @endif
                    @if($_year += '0')
                         <?php
                         $newyear = ($_year + 543);
                         ?>
                         {{$newyear}}
                    @endif
                      </h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
    @foreach($_report3 as $out_report3)
       {{ Form::open(['method' => 'post' , 'url' => '/menu5_search/'.$out_report3->id.'/search']) }}
        <div class="row">
            <div class= col-sm-2 ">
                        <select class="form-control" id="month" name="month">
                            <option value=" " >เลือกเดือน</option>
                            <option value="1" >มกราคม</option>
                            <option value="2">กุมภาพันธ์</option>
                            <option value="3">มีนาคม</option>
                            <option value="4">เมษายน</option>
                            <option value="5">พฤษภาคม</option>
                            <option value="6">มิถุนายน</option>
                            <option value="7">กรกฎาคม</option>
                            <option value="8">สิงหาคม</option>
                            <option value="9">กันยยน</option>
                            <option value="10">ตุลาคม</option>
                            <option value="11">พฤศจิกายน</option>
                            <option value="12">ธันวาคม</option>
                        </select>  
            </div>

           
            <div class= col-sm-2 ">
                      <select class="form-control" id="year" name="year">
                            <option value=" ">เลือกปี</option>
                            <option value="2016">2559</option>
                            <option value="2017">2560</option>
                            <option value="2018">2561</option>
                            <option value="2019">2562</option>
                            <option value="2020">2563</option>
                            <option value="2021">2564</option>
                            <option value="2022">2565</option>
                            <option value="2023">2566</option>
                            <option value="2024">2567</option>
                            <option value="2025">2568</option>
                            <option value="2026">2569</option>
                            <option value="2027">2570</option>
                        </select>    
            </div>

            <div class= col-sm-2 ">
                 <button type="submit" class="btn btn-sm btn-success">&nbsp;&nbsp;ค้นหา</button>
            </div>

        </div> 
       {{ Form::close() }} 
    @endforeach   

      <!-- users -->
        <div class="row">
        
          <!--  1  -->  
          <div class="col-md-12">
                <div class="panel panel-success">
                    <div class="panel-heading wrapper b-b b-left">
                        @foreach(array_slice($_report2, 0, 1) as $out_report2)
                                <h3>{{$out_report2->id }}. {{$out_report2->name_department}}</h3> 
                        @endforeach
            
                       </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="table-responsive"> 
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>เรื่องที่มอบอำนาจ</th>
                                            <th>ผู้มอบอำนาจ</th>
                                            <th>ผู้รับมอบอำนาจต่อ</th>
                                            <th>การดำเนินการ</th>
                                            <th>เรือนจำ/ทัณฑสถาน</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($_report2 as $out_report2)
                                        <tr>
                                            <td>  
                                                {{  $out_report2->topic }}
                                            </td>
                                            <td> 
                                                {{  $out_report2->offer }}
                                            </td>
                                            <td>  
                                                {{  $out_report2->inheritor }}
                                            </td>
                                            <td> 
                                                {{  $out_report2->Action }}
                                            </td>
                                            <td> 
                                                {{  $out_report2->prison }}
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                    </div>
                     <!-- /.panel-body -->
                </div>
            </div>
           <!-- 1 -->  
        </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
