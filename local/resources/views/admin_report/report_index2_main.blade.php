@extends('layouts.master')

@section('title', 'รายงานสรุป มอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')
    <!-- content -->
    
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                    <div class="col-sm-10 col-xs-7">
                        <h1 class="m-n font-thin h3 text-black">รายงานสรุป มอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์  </h1>
                       <!-- main table --> 
                    </div> 
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted">รายงานสรุป มอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h5>              
              </div>
                                 <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="table-responsive">
                                    <table id="example" cellspacing="0" border="0" class="table table-striped table-bordered" style="width:100%">
                                            <colgroup width="56"></colgroup>
                                            <colgroup width="362"></colgroup>
                                            <thead>
                                            <tr>
                                                <td colspan=3 height="35" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=5 color="#000000">รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</font></b></td>
                                            </tr>
                                            <tr>
                                                <th ><h4 align="center">ลำดับ</h4></td>
                                                <th ><h4 align="center">หน่วยงาน</h4></td>
                                            </tr>
                                            </thead>
                                            <?php $count = 0; ?>
                                            @foreach($_datadepartment as $out_datadepartment)
                                                <tr>
                                                    <td  align="center" >{{ ++$count }}</td>
                                                    <td  valign=middle><u> <font size=3 color="#000000"> <a href = "{{ url('menu5/').'/'.$out_datadepartment->id }}" > 
                                                        {{ $out_datadepartment->name_department }} </font> </td>
                                                </tr>
                                            @endforeach
                                        </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
            </div>
          </div>
       
          {{ Form::open(['method' => 'get' ,'url' => '/menu3/add']) }}
        <div class="modal fade" id="ADD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">เพิ่มหัวชื่อหน่วยงาน</h4>
                    </div>
                    <div class="panel-body">
                        <br>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::text('name_department',null, ['class' => 'form-control','placeholder' => 'ชื่อหน่วยงาน']) }}
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="clearfix">
                            <span class="pull-right">
                                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
        
        
        
        
        
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection

