@extends('layouts.master')

@section('title', 'รายงาน')

@section('content')
<!-- content -->
<div id="content" class="app-content" role="main">
    <div class="app-content-body ">
        <div class="hbox hbox-auto-xs hbox-auto-sm">
            <!-- main -->
            <div class="col">
                <div class="wrapper-md">
                    <div class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div class="col-sm-10 col-xs-7">
                                <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์  </h1>
                                <!-- main table --> 
                            </div>
                        </div>
                    </div>
                
                    <div class="row"> 
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading wrapper b-b b-light">
                                    <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h5>              
                                </div>
                                 <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="example" cellspacing="0" border="0" class="table table-striped table-bordered" style="width:100%">
                                            <colgroup width="56"></colgroup>
                                            <colgroup width="362"></colgroup>
                                            <thead>
                                                <tr>
                                                    <td colspan=3 height="35" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=5 color="#000000">รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</font></b></td>
                                                </tr>
                                                <tr>
                                                     <td ><h4 align="center">ลำดับ</h4></td>
                                                     <td ><h4 align="center">username</h4></td>
                                                     <td ><h4 align="center">ชื่อ</h4></td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $count = 0; ?>
                                             @foreach($_datauser as $out_datauser)
                                                <tr>
                                                    <td>{{ ++$count }}</td>
                                                    <td><a href = "{{ url('menu4_list_policy/').'/'.$out_datauser->id }}" >{{$out_datauser->username}}</a></td>
                                                    <td><a href = "{{ url('menu4_list_policy/').'/'.$out_datauser->id }}" >{{$out_datauser->name}}</a></td>
                                                </tr>
                                          @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
          <!-- / main -->
        </div>
    </div>
</div>
<!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection

