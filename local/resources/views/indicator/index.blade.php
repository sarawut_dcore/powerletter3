@extends('layouts.master')

@section('title', 'รายงานตัวชี้วัด')

@section('content')
    <!-- content -->
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div  class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div  class="col-sm-6 col-xs-12">
                                <h1   class="m-n font-thin h3 text-black">รายงานตัวชี้วัด</h1>
                            </div>
                        </div>
                    </div>
                    <!-- / main header -->
                    <div class="wrapper-md">
                        <div class="row">
                            <form disabled>
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            รายการ
                                        </div>
                                        <table class="table-responsive table table-bordered">
                                            <thead>
                                            <tr class="footable-header">
                                                <th>ขั้นตอนที่</th>
                                                <th>ชื่อตัวชี้วัด</th>
                                                <th>หน่วยวัด</th>
                                                <th>เป้าหมาย</th>
                                                <th>ผลการดำเนินงาน</th>
                                                <th>แนบไฟล์</th>
                                                
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($indicators as $indicator)
                                                <tr class="info">
                                                    <td></td>
                                                    <td class="col-lg-5">
                                                        <span class="font-bold">{{ $indicator->detail }}</span><br/>
                                                        @foreach($indicator->subDetails as $subDetail)
                                                            <span> {{ $subDetail->num_grad }} : {{ $subDetail->detail }} <br> </span><br/>
                                                        @endforeach
                                                    </td>
                                                    <td>{{ $indicator->type ? $indicator->type->name : '-' }}</td>
                                                    <td>{{ $indicator->target }}</td>
                                                    <td>
                                                        <input type="text" name="score" placeholder="คำตอบ" class="form-control" />
                                                    </td>
                                                    <td>
                                                        @if($indicator->isPercent() || $indicator->isCount())
                                                            <input type="file" name="file" />
                                                        @endif
                                                    </td>
                                                </tr>
                                                @php
                                                    $count = 1;
                                                @endphp

                                                @foreach($indicator->choices as $choice)
                                                    @if($indicator->isStep())
                                                    <tr class="active">
                                                        <td>{{ $count }}</td>
                                                        <td>{{ $choice->detail }}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            <label class="i-checks">
                                                                <input type="checkbox"><i></i>
                                                            </label>
                                                        </td>
                                                        <td>
                                                            <input type="file" name="file" />
                                                        </td>
                                                    </tr>
                                                    @php
                                                        $count = $count + 1;
                                                    @endphp
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
     
                                    <div class="text-center m-b">
                                        <span class="text-danger m-r">***กรณีตรวจสอบความถูกต้องครบทุกตัว***</span>

                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection
