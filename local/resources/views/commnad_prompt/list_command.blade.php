@extends('layouts.master')

@section('title', 'ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')
    <!-- content -->
    @php
     

    @endphp

    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <h1 class="m-n font-thin h3 text-black">
                                    ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์
                                </h1>
                               <!-- main table --> 
                            </div>
                        </div>
                    </div>

                    
                    <!-- / main header -->

                    <div class="wrapper-md">
                        <div class="row">
                             <!-- / form  -->
                              
                                <div class="col-sm-12">
                                    <div class="panel panel-default">

                                        @if($errors->all())
                                            <div class="alert alert-danger">
                                                {{ $errors->first() }}
                                            </div>
                                        @endif
                                        
                                        <div class="panel-heading" >
                                            <h4>  แนวทางการดำเนินงานในการปฏิบัติตามนโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์  &nbsp;
                                                @if(Auth::user()->group == 3) 
                                                <button type="submit" class="btn btn-success" name = "btn-insert" data-toggle="modal" data-target="#ADD"> เพิ่มข้อมูล </button> </h4>
                                                @endif
                                        </div>

                                            <tr>
                                                @foreach($_data_topics_name_all as $out_data_topics_name_all)
                                                <td style="border-bottom: 1px solid #000000" colspan=7 height="40" align="left" valign=middle bgcolor="#9DC3E6"><b><font face="TH SarabunIT๙" size=4 color="#000000">&nbsp;&nbsp;&nbsp;{{  $out_data_topics_name_all->topics }}</font></b></td>
                                                @endforeach
                                            </tr>
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <td rowspan=2 height="80" align="center" valign=middle>ลำดับที่</td>
                                                    <td rowspan=2 align="center" valign=middle>เรื่องที่มอบอำนาจ</td>
                                                    <td align="center" valign=middle>คำสั่งกรมฯ</td>
                                                    <td align="left" valign=middle>ชื่ออธิบดีกรมราชทัณฑ์</td>
                                                    <td rowspan=2 align="left" valign=middle>ผู้รับมอบอำนาจ</td>
                                                    <td rowspan=2 align="center" valign=middle>เอกสาร</td>
                                                    <td rowspan=2 align="center" valign=middle>Action</td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign=middle>ลงวันที่</td>
                                                    <td align="left" valign=middle>ผู้มอบอำนาจ</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $count=1 ?>
                                                @foreach($_topics_detail_all as $out_topics_detail_all)
                                                    <tr>
                                                        <?php $count=$count+1 ?>
                                                        <td>{{  $out_topics_detail_all->no }}</td>
                                                        <td>{{  $out_topics_detail_all->topics }}</td>
                                                        <td>{{  $out_topics_detail_all->command_no }}
                                                            <br>
                                                            <?php
                                                                echo  $out_topics_detail_all->thaiday ."/". $out_topics_detail_all->thaimount."/".$out_topics_detail_all->thaiyear;
                                                            ?>
                                                        </td>
                                                        <td>{{  $out_topics_detail_all->name_offer }}</td>
                                                        <td>{{  $out_topics_detail_all->name_receive }}</td>
                                                        <td><div class="m-t">
                                                                <span class="fa fa-file-pdf-o text-danger m-r-sm" style="font-size: 20px;"></span> <label> ชื่อไฟล์ : </label>
                                                            <a href="{{ url('/local/public/file/').'/'.mb_convert_encoding($out_topics_detail_all->add_file, 'HTML-ENTITIES', 'UTF-8') }} ">
                                                           {{$out_topics_detail_all->add_file}}
                                                            <br></a></div>
                                                        </td>
                                                        <td>

                                                            <button type="submit" class="btn btn-danger" name = "btn-edit" data-toggle="modal" data-target="#EDIT{{ $out_topics_detail_all->topics_detail_id }}"> มอบอำนาจ </button>
                                                            
                                                            @if(Auth::user()->group == 3)
                                                            <button type="submit" class="btn btn-warning" name = "btn-edit" data-toggle="modal" data-target="#EDITADMIN{{ $out_topics_detail_all->topics_detail_id }}"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;แก้ไข&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </button>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                            
                                    </div>
                                   
                                </div>
                               
                            <!-- / end form -->
                        </div>
                        
                        
                    </div>
                    

                    
                    
                    
                <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->

    <!--pop up -->
    {{ Form::open(['method' => 'post' ,'url' => '/menu01/'.$_no_topic.'/'.$count , 'files' => true,]) }}
    <div class="modal fade" id="ADD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel">เพิ่มแนวทางการดำเนินงาน</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">เรื่องที่มอบอำนาจ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input type="text" class=" col-xs-8 col-sm-10" id="topics" name="topics" />
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">คำสั่งกรมฯ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <textarea class=" col-xs-8 col-sm-10" id="command_no" name="command_no"></textarea>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">วันที่</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input type="date" class=" col-xs-8 col-sm-10" id="datecreat" name="datecreat" />
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">ชื่ออธิบดีกรมราชทัณฑ์</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input type="text" class=" col-xs-8 col-sm-10" id="name_offer" name="name_offer" />
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">	ผู้รับมอบอำนาจ</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <input type="text" class=" col-xs-8 col-sm-10" id="name_receive" name="name_receive" />
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">	หมายเหตุ </label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    <textarea class=" col-xs-8 col-sm-10" id="note" name="note"></textarea>
                                </div>
                            </div>
                        </div> 
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">	เอกสาร</label>
                            <div class="col-xs-12 col-sm-9">
                                <div class="clearfix">
                                    {{Form::file('add_file')}}
                                </div>
                            </div>
                        </div> 
                    </div>

                <div class="panel-footer">
                    <div class="clearfix">
                        <span class="pull-right">
                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                             <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
{{ Form::close() }}


@foreach($_data_department_all as $out_data_department_all)
@foreach($_topics_detail_all as $out_topics_detail_all)
        {{ Form::open(['method' => 'post' , 'url' => '/menu3_update/'.$out_data_department_all->id.'/update', 'files' => true,]) }}
            <div class="modal fade" id="EDIT{{ $out_topics_detail_all->topics_detail_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" >
                            <div class="modal-content" >
                                <div class="modal-header" >
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">มอบอำนาจ</h4>
                                </div>
                                <div class="panel-body">
                                    
                                    <div class="row">
                                        <label class="control-label col-sm-3" for="form_name">เรื่องที่มอบอำนาจ</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::textarea('topics',$out_topics_detail_all->topics, ['class' => 'form-control','placeholder' => 'เรื่องที่มอบอำนาจ' , 'cols'=>"66" , 'rows'=>"3" ,"readonly" ]) }}
                                            </div>
                                        </div>
                                    </div>
        
                                    <div class="row">
                                    <label class="control-label col-sm-3" for="form_name">คำสั่งกรมราชทัณฑ์เลขที่</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::text('command_no',$out_topics_detail_all->command_no, ['class' => 'form-control','placeholder' => 'คำสั่งกรมราชทัณฑ์',"readonly" ]) }}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                     <label class="control-label col-sm-3" for="form_name">ผู้มอบอำนาจ</label>
                                         <div class="col-sm-9">
                                             <div class="form-group">
                                                 {{ Form::text('name_offer',$out_topics_detail_all->name_offer, ['class' => 'form-control','placeholder' => 'ผู้รับมอบอำนาจ',"readonly" ]) }}
                                             </div>
                                         </div>
                                     </div>
        
                                     <div class="row">
                                        <label class="control-label col-sm-3" for="form_name">ผู้รับมอบอำนาจต่อ</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::text('name_receive',$out_topics_detail_all->name_receive, ['class' => 'form-control','placeholder' => 'ผู้รับมอบอำนาจต่อ' ]) }}
                                            </div>
                                        </div>
                                     </div>
                                    <br>
                                    <div class="row">
                                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">หน่วยงาน</label>
                                            <div class="col-xs-12 col-sm-9">
                                                <select class="form-control" name="department" id="department">
                                                        @foreach($_data_department_all as $out_data_department_all)
                                                            <option value="{{$out_data_department_all->id}}">{{$out_data_department_all->name_department}}</option>
                                                        @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    <br>.
                                    
                                    <div class="row">
                                        <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">เอกสารแนบ</label>
                                        <div class="col-xs-12 col-sm-9">
                                        <input type="file" name="image" /><br>
                                        <label>ชื่อไฟล์ :
                                          {{ $out_topics_detail_all->add_file }}
                                        </label>
                                        <input type="hidden" name="defualt_file" value="{{ $out_topics_detail_all->add_file }}" />
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <div class="panel-footer">
                                    <div class="clearfix">
                                        <span class="pull-right">
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;มอบอำนาจ</button>
                                             <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        {{ Form::close() }}
        @endforeach
        @endforeach



        @foreach($_topics_detail_all as $out_topics_detail_all)
        {{ Form::open(['method' => 'post' , 'url' => '/menu01_update/'.$out_topics_detail_all->topics_detail_id.'/update', 'files' => true,]) }}
            <div class="modal fade" id="EDITADMIN{{ $out_topics_detail_all->topics_detail_id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" >
                            <div class="modal-content" >
                                <div class="modal-header" >
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">มอบอำนาจ</h4>
                                </div>
                                <div class="panel-body">
                                    
                                    <div class="row">
                                        <label class="control-label col-sm-3" for="form_name">เรื่องที่มอบอำนาจ</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::textarea('topics',$out_topics_detail_all->topics, ['class' => 'form-control','placeholder' => 'เรื่องที่มอบอำนาจ' , 'cols'=>"66" , 'rows'=>"3"]) }}
                                            </div>
                                        </div>
                                    </div>
        
                                    <div class="row">
                                    <label class="control-label col-sm-3" for="form_name">คำสั่งกรมฯ</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::text('command_no',$out_topics_detail_all->command_no, ['class' => 'form-control','placeholder' => 'คำสั่งกรมราชทัณฑ์']) }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                    <label class="control-label col-sm-3" for="form_name">วันที่</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::date('datecreat',$out_topics_detail_all->datecreat, ['class' => 'form-control','placeholder' => 'วันที่']) }}
                                            </div>
                                        </div>
                                    </div>

                                    
                                    <div class="row">
                                     <label class="control-label col-sm-3" for="form_name">ชื่ออธิบดีกรมราชทัณฑ์</label>
                                         <div class="col-sm-9">
                                             <div class="form-group">
                                                 {{ Form::text('name_offer',$out_topics_detail_all->name_offer, ['class' => 'form-control','placeholder' => 'ผู้รับมอบอำนาจ']) }}
                                             </div>
                                         </div>
                                     </div>
        
                                     <div class="row">
                                        <label class="control-label col-sm-3" for="form_name">ผู้รับมอบอำนาจ</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::text('name_receive',$out_topics_detail_all->name_receive, ['class' => 'form-control','placeholder' => 'ผู้รับมอบอำนาจต่อ' ]) }}
                                            </div>
                                        </div>
                                     </div>

                                    <div class="row">
                                        <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">เอกสารแนบ</label>
                                        <div class="col-xs-12 col-sm-9">
                                        <input type="file" name="image" /><br>
                                        <label>ชื่อไฟล์ :
                                          {{ $out_topics_detail_all->add_file }}
                                        </label>
                                        <input type="hidden" name="defualt_file" value="{{ $out_topics_detail_all->add_file }}" />
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <div class="panel-footer">
                                    <div class="clearfix">
                                        <span class="pull-right">
                                            <input type="hidden" name="no_topic" value="{{ $_no_topic }}" />
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;แก้ไข</button>
                                             <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        {{ Form::close() }}
        @endforeach

@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection
    
  