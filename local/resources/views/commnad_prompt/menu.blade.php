
@extends('layouts.master')

@section('title', 'ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')
    
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
        @if($errors->all())
            <div class="alert alert-danger">
                {{ $errors->first() }}
            </div>
        @endif
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์ &nbsp; 
                        @if(Auth::user()->group == 3)
                            <button type="submit" class="btn btn-success" name = "btn-insert" data-toggle="modal" data-target="#ADD"> เพิ่มข้อมูล </button>
                        @endif 
                    </h1>
                    
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted">รายการหัวข้อหนังสือมอบอำนาจ</h5>              
              </div>

              <div class="panel-body">
                    <div class="table-responsive">
                            <table id="example" cellspacing="0" border="0" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                    <tr>
                                        <td colspan=3 height="35" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=5 color="#000000">รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</font></b></td>
                                    </tr>
                                    <tr>
                                        <th ><h4>ลำดับ</h4></td>
                                        <th ><h4>รายการหัวข้อหนังสือมอบอำนาจ</h4></td>
                                    </tr>
                                    </thead>
                                    @foreach($_data_topics_name_all as $out_data_topics_name_all)
                                        <tr>
                                            <td>
                                                <a href ="{{ url('menu01/').'/'.$out_data_topics_name_all->no }}"  >
                                                {{  $out_data_topics_name_all->no  }}.</a>
                                            </td>
                                            <td>
                                                <a href ="{{ url('menu01/').'/'.$out_data_topics_name_all->no }}"  >
                                                    {{  $out_data_topics_name_all->topics  }}
                                                </a></td>
                                        </tr>
                                    @endforeach
                                </table>


                    </div>
                    <!-- /.table-responsive -->
                </div>

            </div>
          </div>


        <!--pop up -->
        {{ Form::open(['method' => 'post' ,'url' => '/menu01/add']) }}
             <div class="modal fade" id="ADD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                 <div class="modal-dialog">
                     <div class="modal-content">
                         <div class="modal-header">
                             <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                 <h4 class="modal-title" id="myModalLabel">เพิ่มหัวข้อหนังสือมอบอำนาจ</h4>
                         </div>

                         <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::text('topic',null, ['class' => 'form-control','placeholder' => 'ชื่อหัวข้อหนังสือมอบอำนาจ']) }}

                                        <?php $count = 0; ?>
                                        @foreach($_data_topics_name_all as $out_data_topics_name_all)
                                        <?php ++$count; ?>
                                        @endforeach

                                        <input type="hidden" class=" col-xs-8 col-sm-12" id="count" name="count" value="{{ ++$count}}">
                                    </div>
                                </div>
                            </div>
                        </div>

                         <div class="panel-footer">
                             <div class="clearfix">
                                 <span class="pull-right">
                                     <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                      <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                 </span>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             {{ Form::close() }}
        
        
        
        
        
        
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection

