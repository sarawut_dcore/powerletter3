@extends('layouts.master')

@section('title', 'นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์')

@section('content')
  

   <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"> ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h3>
                <br>
                        <!--  -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ข้อมูลคำสั่งมอบอำนำจกำรปฏิบัติรำชกำรแทนอธิบดีกรมรำชทัณฑ์
                        
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        
                                        <th>รายการหัวข้อหนังสือมอบอำนาจ</th>
                                       
                                    </tr>
                                </thead>
                                <tbody>
                                        <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle>
                                                    <b><u>
                                                    <font face="TH SarabunIT๙" size=4 color="#000000" >
                                                    <a href = "{{ url('/list_command_01') }}" >

                                                    1.การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ รองอธิบดีกรมราชทัณฑ์ 
                                                   
                                                    </a>
                                                    </font>
                                                    </u></b> 
                                                </td>
                                        </tr>
                                        <tr>
                                               
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle>
                                                    <b><u>
                                                    <font face="TH SarabunIT๙" size=4 color="#000000" >
                                                    <a href = "{{ url('/list_command_01') }}" >

                                                        2. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ สำนักผู้ตรวจราชการกรมและหน่วยงานที่เกี่ยวข้อง
                                                   
                                                    </a>
                                                    </font>
                                                    </u></b>
                                                </td>
                                        </tr>
                                        

                                        <tr>
                                       
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle>
                                                    <b><u>
                                                    <font face="TH SarabunIT๙" size=4 color="#000000" >
                                                    <a href = "{{ url('/list_command_01') }}" >
                                                        3. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ สำนักทัณฑปฏิบัติและหน่วยงานที่เกี่ยวข้อง
                                           
                                                    </a>
                                                    </font>
                                                    </u></b>
                                                 </td>
                                        </tr>
                                        <tr>
                                       
                                            <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle>
                                                <b><u>
                                                <font face="TH SarabunIT๙" size=4 color="#000000" >
                                                <a href = "{{ url('/list_command_01') }}" >
                                                    4. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ สำนักทัณฑวิทยา และหน่วยงานที่เกี่ยวข้อง
                                       
                                                </a>
                                                </font>
                                                </u></b>
                                             </td>
                                    </tr>
                                                
                                            
                                           
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">4. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ สำนักทัณฑวิทยา และหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">5. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ ผู้อำนวยการสำนักพัฒนาพฤตินิสัยและหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">6. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ นักทัณฑวิทยาเชี่ยวชาญ ซึ่งปฏิบัติหน้าที่ในตำแหน่ง ผู้อำนวยการสำนักวิจัยและพัฒนาระบบงานราชทัณฑ์</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">7. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ สำนักงานเลขานุการ และหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">8. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ กองการเจ้าหน้าที่ และหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">9. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ กองบริการทางการแพทย์และหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">10. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ สถาบันพัฒนาข้าราชการราชทัณฑ์และหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">11. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ หน่วยตรวจสอบภายในและหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">12. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ กองคลัง และหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">13. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ ผู้ว่าราชการจังหวัด</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">14. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ เรือนจำ/ทัณฑสถาน</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">15. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่เรือนจำกลาง ทัณฑสถาน สถานกักกัน สถานกักขัง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">16. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ ผู้บัญชาการเรือนจำ ผู้อำนวยการทัณฑสถาน ในเขตกรุงเทพมหานคร</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">17. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ ผู้อำนวยการทัณฑสถานโรงพยาบาลราชทัณฑ์</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">18. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ ผู้บัญชาการเรืองจำกลางบางขวาง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">19. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ </font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">20. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ ผู้ว่าราชการจังหวัดชายแดนภาคใต้ (ปัตตานี, ยะลา, นราธิวาส)</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">22. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ผู้บัญชาการเรือนจำกลางเขาบิน</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">23. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ผู้บัญชาการเรือนจำกลางพิษณุโลก</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">24. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ผู้บัญชาการเรือนจำกลางขอนแก่น</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">25. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ผู้บัญชาการเรือนจำกลางสงขลา</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">26. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ ผู้อำนวยการทัณฑสถานหญิงสงขลา</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">27. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ ผู้บัญชาเรือนจำกลางนครศรีธรรมราช</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">28. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ ผู้บัญชาการเรือนจำพิเศษมีนบุรี</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">29. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ผู้อำนวยการทัณฑสถานวัยหนุ่มกลาง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">30. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ผู้บัญชาการเรือนจำกลางปัตตานี</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">31. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ผู้บัญชาการเรือนจำกลางยะลา</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">32. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ผู้บัญชาการเรือนจำกลางคลองไผ่</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">33. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ ผู้อำนวยการกองสังคมสงเคราะห์และหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">34. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ หัวหน้ากลุ่มงานคุ้มครองจริยธรรมและหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">35. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ ผู้อำนวยศูนย์ข่าวกรองและความมั่นคงของกรมราชทัณฑ์และหน่วยงานที่เกี่ยวข้อง</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">36. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ ให้แก่ ให้แก่ผู้บัญชาการเรือนจำกลางสุราษฎร์ธานี</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">37. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ ผู้บัญชาการเรือนจำ ผู้อำนวยการทัณฑสถานสถานกักขังและสถานกักกัน</font></u></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" colspan=6 height="40" align="left" valign=middle><b><u><font face="TH SarabunIT๙" size=4 color="#000000">38. ยกเลิกคำสั่งกรมราชทัณฑ์เกี่ยวกับการมอบอำนาจของอธิบดีกรมราชทัณฑ์</font></u></b></td>
                                                </tr>
                                                
                            
                                    
                                    
                                   
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            
        </div>

       
   </div>



@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection