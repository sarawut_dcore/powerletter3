@extends('layouts.master')

@section('title', 'รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')

    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ตารางสรุปผลแบบรายงานการใช้อำนาจของผู้รับมอบอำนาจในการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">      
        <!--    -->  
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
            <div class="row">
                <!-- main -->
                    <div class="col">
                        <div class="wrapper-md">
                            <div class=" lter b-b wrapper-md">
                                <div class="row">
                                    <div class="col-sm-10 col-xs-6">
                                        <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                                        <!-- main table --> 
                                    </div>
                                    @if(Auth::user()->group == 3)
                                    <div class="col-sm-1 col-xs-6" >
                                        <div align="right">
                                            <a href="{{ url('/menu3').'/'.$_id.'/add' }}"><button type="button" class="btn btn-success" name = "btn-insert"> เพิ่มข้อมูล </button></a>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            
                    <!-- users -->
                    <div class="row">
                        <!--  1  -->  
                        <div class="col-md-11 col-xs-6">
                            <div class="panel panel-success">
                                <div class="panel-heading wrapper b-b b-light">
                                    <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h5>              
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body" style="overflow-x:auto;">
                                    <div class="table-responsive" >
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    {{-- <th>ID</th> --}}
                                                    <th>เรื่องที่มอบอำนาจ</th>
                                                    <th>คำสั่งกรมราชทัณฑ์เลขที่</th>
                                                    <th>ผู้รับมอบอำนาจ</th>
                                                    <th>ผู้รับมอบอำนาจต่อ</th>
                                                    <th>การดำเนินการ</th>
                                                    <th>สถานะ</th>
                                                    <th>เอกสารแนบ</th>
                                                    <th>เรือนจำ/ทัณฑสถาน</th>
                                                    <th>ประเมินผลการดำเนินงาน</th>
                                                    <th>ผู้ประเมิน</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($_datadetaildepartment as $out_datadetaildepartment)
                                                    <tr>
                                                        <input type="hidden" name="id_cmd" id="id_cmd" value="{{$out_datadetaildepartment->id}}"/>
                                                        {{-- <td>{{ $out_datadetaildepartment->id }} </td> --}}
                                                        <td>{{ $out_datadetaildepartment->topic }} </td>
                                                        <td>{{ $out_datadetaildepartment->command }} </td>
                                                        <td>{{ $out_datadetaildepartment->offer }}</td>
                                                        <td>{{ $out_datadetaildepartment->inheritor }}</td>
                                                        <td>{{ $out_datadetaildepartment->Action }}</td>
                                                        <td>{{ $out_datadetaildepartment->status }} <input type="hidden" name="status" id="status" value="{{$out_datadetaildepartment->status}}"/> </td>

                                                        <td>
                                                            @if($out_datadetaildepartment->path_file==null) 
                                                            @else
                                                                <div class="m-t"><span class="fa fa-file-pdf-o text-danger m-r-sm" style="font-size: 20px;"></span> ชื่อไฟล์ : <a href="{{ url('/local/public/file/').'/'.$out_datadetaildepartment->path_file }} ">
                                                                {{ $out_datadetaildepartment->path_file }}
                                                                <input type="hidden" name="path_file" id="path_file" value="{{$out_datadetaildepartment->path_file}}"/>
                                                                <br></a></div>
                                                            @endif
                                                        </td>
                                                        <td>{{ $out_datadetaildepartment->prison }}</td>
                                                        <td>
                                                           {{$out_datadetaildepartment->evaluation}}
                                                        </td>
                                                        <td>
                                                            @foreach ($_data_evaluator as $out_data_evaluator)
                                                                @if($out_data_evaluator->id == $out_datadetaildepartment->evaluator)
                                                                    {{$out_data_evaluator->name_department}}
                                                                @endif
                                                            @endforeach
                                                            
                                                        </td>
                                                        <td>
                                                            @if($out_datadetaildepartment->user_id == Auth::user()->id || Auth::user()->group == 3)
                                                                <button type="submit" class="btn btn-warning" name = "btn-edit" data-toggle="modal" data-target="#EDIT{{ $out_datadetaildepartment->id }}"> &nbsp;&nbsp;แก้ไข&nbsp;&nbsp; </button>
                                                                <button type="submit" class="btn btn-danger" name = "btn-delete" data-toggle="modal" data-target="#DELETE{{ $out_datadetaildepartment->id }}"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ลบ&nbsp;&nbsp;&nbsp;&nbsp; </button>
                                                                <button type="submit" class="btn btn-success" name = "btn-evaluation" data-toggle="modal" data-target="#evaluation{{ $out_datadetaildepartment->id }}"> ประเมิน </button>
                                                            @endif
                                                        </td>   
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>

         <!--pop up -->
         
        {{ Form::open(['method' => 'post' , 'url' => '/menu3_add/'.$_id.'/add', 'files' => true,]) }}
            <div class="modal fade" id="ADD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" >
                    <div class="modal-content" >
                        <div class="modal-header" >
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h4>
						</div>
                        <div class="panel-body">
                            <div class="row">
                                <label class="control-label col-sm-3" for="form_name">เรื่องที่มอบอำนาจ</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        {{ Form::textarea('topic',null, ['class' => 'form-control','placeholder' => 'เพิ่มเรื่องที่มอบอำนาจ' , 'cols'=>"66" , 'rows'=>"3" ]) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            <label class="control-label col-sm-3" for="form_name">คำสั่งกรมราชทัณฑ์เลขที่</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        {{ Form::text('command',null, ['class' => 'form-control','placeholder' => 'เพิ่มเลขที่คำสั่งกรมราชทัณฑ์']) }}
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                             <label class="control-label col-sm-3" for="form_name">ผู้รับมอบอำนาจ</label>
                                 <div class="col-sm-9">
                                     <div class="form-group">
                                         {{ Form::text('offer',null, ['class' => 'form-control','placeholder' => 'เพิ่มตำแหน่ง,ชื่อ']) }}
                                     </div>
                                 </div>
                             </div>

                             <div class="row">
                                <label class="control-label col-sm-3" for="form_name">ผู้รับมอบอำนาจต่อ</label>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        {{ Form::text('inheritor',null, ['class' => 'form-control','placeholder' => 'เพิ่มตำแหน่ง,ชื่อ']) }}
                                    </div>
                                </div>
                             </div>

                             <div class="row">
                                 <label class="control-label col-sm-3" for="form_name">การดำเนินการ</label>
                                 <div class="col-sm-9">
                                     <div class="form-group">
                                         {{ Form::text('Action',null, ['class' => 'form-control','placeholder' => 'รายละเอียดการดำเนินการ']) }}
                                     </div>
                                 </div>
                              </div>
                            <div class="row">
                                <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset2">สถานะ</label>
                                <div class="col-sm-9">
                                        <select class="form-control" id="status" name="status">
                                            <option value="">เลือกสถานะ</option>
                                            <option value="รายงานแล้ว">รายงานแล้ว</option>
                                            <option value="ยังไม่ได้รายงาน">ยังไม่ได้รายงาน</option>
                                            <option value="ไม่มีผลการดำเนินงานประจำเดือน">ไม่มีผลการดำเนินงานประจำเดือน</option>
                                       </select>
                                </div>
							</div>
                            <br>
                            <div class="row">
                                    <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">ผลการประเมิน</label>
                                    <div class="col-xs-12 col-sm-9">
                                        <select class="form-control" name="evaluation" id="evaluation">
                                                <option value="">เลือกผลการประเมิน</option>
                                                <option value="รายงานแล้ว">รายงานแล้ว</option>
                                                <option value="ยังไม่ได้รายงาน">ยังไม่ได้รายงาน</option>
                                                <option value="ไม่มีผลการดำเนินงานประจำเดือน">ไม่มีผลการดำเนินงานประจำเดือน</option>
                                        </select>
                                    </div>
                            </div>
                            <br>
                            <div class="row">
                                <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">ผู้ประเมิน</label>
                                <div class="col-xs-12 col-sm-9">
                                    <select class="form-control" name="evaluator" id="evaluator">
                                            <option>เลือกผู้ประเมิน</option>
                                            @foreach($_data_evaluator as $out_data_evaluator)
                                                <option value="{{$out_data_evaluator->id}}">{{$out_data_evaluator->name_department}}</option>
                                            @endforeach
                                    </select>
                                    <input type="hidden" name="id_cmd_log" id="id_cmd_log" value="{{$_id}}"/>
                                    @foreach ($_no_cmd as $out_no_cmd)
                                    @endforeach
                                    <input type="hidden" name="no_cmd" id="no_cmd" value="{{$out_no_cmd->id+1}}"/>
                                </div>
                            </div>
                            <br>
                            <div class="row">
								<label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset" >เรือนจำ/ทัณฑสถาน</label>
                                <div class="col-sm-9">
								    <div class="clearfix">
                                        <select class="form-control" id="prison" name="prison">	
                                                <option>เลือกเรือนจำ/ทัณฑสถาน</option>
                                            @foreach($_dataoffice as $out_dataoffice)	
                                                <option>{{  $out_dataoffice->office_name  }}</option>
                                            @endforeach
                                        </select>
								    </div>
								</div>
						    </div>
                            <br>
                            <div class="row">
								<label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">เอกสารแนบ</label>
								<div class="col-xs-12 col-sm-9">
                                    <input type="file" name="image" />
								</div>
                            </div>
                            
						</div>
                        <div class="panel-footer">
                            <div class="clearfix">
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                     <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{ Form::close() }}


        @foreach($_datadetaildepartment as $out_datadetaildepartment)
        {{ Form::open(['method' => 'post' , 'url' => '/menu3_delete/'.$out_datadetaildepartment->id.'/delete']) }}
        <div class="modal fade" id="DELETE{{ $out_datadetaildepartment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title" id="myModalLabel">ลบข้อมูลคำสั่งมอบอำนาจ ยืนยันที่จะลบหรือไม่</h4>
                  </div>
                  <div class="panel-body">
                          <br>
                          <div class="row">
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <h5 class="font-thin m-t-none m-b-none text-muted"> ลบข้อมูลคำสั่งมอบอำนาจ : {{ $out_datadetaildepartment->command }}</h5> 
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="panel-footer">
                        <div class="clearfix">
                            <span class="pull-right">
                              <button type="submit" class="btn btn-sm btn-success">ยืนยัน</button>
                              <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal">ยกเลิก</a>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{ Form::close() }}
        @endforeach

        @if($_datadetaildepartment != null) 
        @foreach($_datadetaildepartment as $out_datadetaildepartment)
        {{ Form::open(['method' => 'post' , 'url' => '/menu3_edit/'.$out_datadetaildepartment->id.'/edit', 'files' => true,]) }}
            <div class="modal fade" id="EDIT{{ $out_datadetaildepartment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog" >
                            <div class="modal-content" >
                                <div class="modal-header" >
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                        <h4 class="modal-title" id="myModalLabel">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h4>
                                </div>
                                <div class="panel-body">                                 
                                    <div class="row">
                                        <label class="control-label col-sm-3" for="form_name">เรื่องที่มอบอำนาจ</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::textarea('topic',$out_datadetaildepartment->topic, ['class' => 'form-control','placeholder' => 'เพิ่มเรื่องที่มอบอำนาจ' , 'cols'=>"66" , 'rows'=>"3" ]) }}
                                            </div>
                                        </div>
                                    </div>
        
                                    <div class="row">
                                    <label class="control-label col-sm-3" for="form_name">คำสั่งกรมราชทัณฑ์เลขที่</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::text('command',$out_datadetaildepartment->command, ['class' => 'form-control','placeholder' => 'เพิ่มเลขที่คำสั่งกรมราชทัณฑ์']) }}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                     <label class="control-label col-sm-3" for="form_name">ผู้รับมอบอำนาจ</label>
                                         <div class="col-sm-9">
                                             <div class="form-group">
                                                 {{ Form::text('offer',$out_datadetaildepartment->offer, ['class' => 'form-control','placeholder' => 'เพิ่มตำแหน่ง,ชื่อ']) }}
                                             </div>
                                         </div>
                                     </div>
        
                                     <div class="row">
                                        <label class="control-label col-sm-3" for="form_name">ผู้รับมอบอำนาจต่อ</label>
                                        <div class="col-sm-9">
                                            <div class="form-group">
                                                {{ Form::text('inheritor',$out_datadetaildepartment->inheritor, ['class' => 'form-control','placeholder' => 'เพิ่มตำแหน่ง,ชื่อ']) }}
                                            </div>
                                        </div>
                                     </div>
        
                                     <div class="row">
                                         <label class="control-label col-sm-3" for="form_name">การดำเนินการ</label>
                                         <div class="col-sm-9">
                                             <div class="form-group">
                                                 {{ Form::text('Action',$out_datadetaildepartment->Action, ['class' => 'form-control','placeholder' => 'รายละเอียดการดำเนินการ']) }}
                                             </div>
                                         </div>
                                      </div>
                                    <div class="row">
                                        <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset2">สถานะ</label>
                                        <div class="col-sm-9">
                                                <select class="form-control" id="status" name="status">
                                                    <option>{{  $out_datadetaildepartment->status  }}</option>
                                                    <option>รายงานแล้ว</option>
                                                    <option>ยังไม่ได้รายงาน</option>
                                                    <option>ไม่มีผลการดำเนินงานประจำเดือน</option>
                                               </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">ผลการประเมิน</label>
                                            <div class="col-xs-12 col-sm-9">
                                                <select class="form-control" name="evaluation" id="evaluation">
                                                        @if($out_datadetaildepartment->evaluation != '')
                                                            <option value="{{  $out_datadetaildepartment->evaluation  }}">{{  $out_datadetaildepartment->evaluation  }}</option>
                                                        @else
                                                            <option value="">เลือกผลการประเมิน</option>
                                                        @endif
                                                        <option value="รายงานแล้ว" >รายงานแล้ว</option>
                                                        <option value="ยังไม่ได้รายงาน">ยังไม่ได้รายงาน</option>
                                                        <option value="ไม่มีผลดำเนินงานประจำเดือน">ไม่มีผลการดำเนินงานประจำเดือน</option>
                                                </select>
                                            </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">ผู้ประเมิน</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <select class="form-control" name="evaluator" id="evaluator">
                                                    @foreach ($_data_evaluator as $out_data_evaluator)
                                                        @if($out_data_evaluator->id == $out_datadetaildepartment->evaluator)
                                                            <option value="{{$out_data_evaluator->id}}">{{$out_data_evaluator->name_department}}</option>
                                                        @endif
                                                    @endforeach
                                                    @if($out_datadetaildepartment->evaluator == '')
                                                            <option value="">เลือกผู้ประเมิน</option>
                                                        @endif
                                                    @foreach($_data_evaluator as $out_data_evaluator)
                                                        <option value="{{$out_data_evaluator->id}}">{{$out_data_evaluator->name_department}}</option>
                                                    @endforeach
                                            </select>
                                            <input type="hidden" name="id_cmd_log" id="id_cmd_log" value="{{$out_datadetaildepartment->id}}"/>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset" >เรือนจำ/ทัณฑสถาน</label>
                                        <div class="col-sm-9">
                                            <div class="clearfix">
                                                <select class="form-control" id="prison" name="prison">	
                                                        <option>{{  $out_datadetaildepartment->prison  }}</option>
                                                    @foreach($_dataoffice as $out_dataoffice)	
                                                        <option>{{  $out_dataoffice->office_name  }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">เอกสารแนบ</label>
                                        <div class="col-xs-12 col-sm-9">
                                        <input type="file" name="image" value={{$out_datadetaildepartment->path_file}}/>
                                        <input type="hidden" name="image2" value={{$out_datadetaildepartment->path_file}}>
                                        </div>
                                    </div>
                                    <br>
                                    <!--<div class="row">
                                        <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">สถานะ</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <select name="evaluation" >
                                                <option value=""  hidden>เลือก</option>
                                                <option value="ตรวจสอบแล้ว">ตรวจสอบแล้ว</option>
                                                <option value="ยังไม่ตรวจสอบ">ยังไม่ตรวจสอบ</option>
                                                <option value="ไม่มีผลการดำเนินงานประจำเดือน">ไม่มีผลการดำเนินงานประจำเดือน</option>
                                            </select>

                                        </div>
                                    </div>
                                    <br>-->
                                </div>
                                <div class="panel-footer">
                                    <div class="clearfix">
                                        <span class="pull-right">
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                             <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
            </div>
        {{ Form::close() }}
        @endforeach

        @foreach($_datadetaildepartment as $out_datadetaildepartment)
        {{ Form::open(['method' => 'post' , 'url' => '/menu3_evaluation/'.$out_datadetaildepartment->id.'/evaluation']) }}
                    <div class="modal fade" id="evaluation{{ $out_datadetaildepartment->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" >
                                <div class="modal-content" >
                                    <div class="modal-header" >
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h4>
                                    </div>
                                    <div class="panel-body">   
                                    <div class="row">
                                            <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">ผลการประเมิน</label>
                                            <div class="col-xs-12 col-sm-9">
                                                <select class="form-control" name="evaluation" id="evaluation">
                                                        @if($out_datadetaildepartment->evaluation != '')
                                                            <option value="{{  $out_datadetaildepartment->evaluation  }}">{{  $out_datadetaildepartment->evaluation  }}</option>
                                                        @else
                                                            <option value="">เลือกผลการประเมิน</option>
                                                        @endif
                                                        <option value="รายงานแล้ว" >รายงานแล้ว</option>
                                                        <option value="ยังไม่ได้รายงาน">ยังไม่ได้รายงาน</option>
                                                        <option value="ไม่มีผลดำเนินงานประจำเดือน">ไม่มีผลการดำเนินงานประจำเดือน</option>
                                                </select>
                                            </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset">ผู้ประเมิน</label>
                                        <div class="col-xs-12 col-sm-9">
                                            <select class="form-control" name="evaluator" id="evaluator">
                                                    @foreach ($_data_evaluator as $out_data_evaluator)
                                                        @if($out_data_evaluator->id == $out_datadetaildepartment->evaluator)
                                                            <option value="{{$out_data_evaluator->id}}">{{$out_data_evaluator->name_department}}</option>
                                                        @endif
                                                    @endforeach
                                                    @if($out_datadetaildepartment->evaluator == '')
                                                            <option value="">เลือกผู้ประเมิน</option>
                                                        @endif
                                                    @foreach($_data_evaluator as $out_data_evaluator)
                                                        <option value="{{$out_data_evaluator->id}}">{{$out_data_evaluator->name_department}}</option>
                                                    @endforeach
                                            </select>
                                            <input type="hidden" name="id_cmd_log" id="id_cmd_log" value="{{$out_datadetaildepartment->id}}"/>
                                            <input type="hidden" name="id_cmd" id="id_cmd" value="{{$_id}}"/>
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <div class="panel-footer">
                                    <div class="clearfix">
                                        <span class="pull-right">
                                            <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                             <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        {{ Form::close() }}
        @endforeach
        @endif

      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable(
                {
                    "order": []
                }
            );
        } );
    </script>
@endsection


