@extends('layouts.master')

@section('title', 'ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')
    <!-- content -->

    <SCRIPT>
        function readOnlyCheckBox() {
           return false;
        }
    </SCRIPT>

    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h5>              
              </div>


                    <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="table-responsive">
    
                                    
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ลำดับ</th>
                                            <th>รายละเอียดการดำเนินงาน</th>
                                            <th class="col-sm-2">ผลดำเนินงาน</th>
                                            <th>เอกสารแนบ</th>
                                            <th>เวลาดำเนินการ</th>
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
    
                                        @foreach($_datapolicy as $_datapolicy_)

                                        <form>   
                                        <tr>
                                                <td>
                                                @if( $_datapolicy_->num_3 == '0') 
                                                    {{  $_datapolicy_->num_1 <> '0' ? $_datapolicy_->num_1 : ' ' }}
                                                    {{  $_datapolicy_->num_2 <> '0' ? '.'.$_datapolicy_->num_2 : ' ' }}
                                                @endif    
    
                                                </td>
                                                <td> 
                                                    @if( $_datapolicy_->num_comment == '1') 
                                                        {{ $_datapolicy_->detail }}
                                                        &nbsp;&nbsp;
                                                        <!--<textarea rows="2" cols="50" name="comment" form="usrform"></textarea>-->
                                                        {{ Form::textarea('comment',$_datapolicy_->comment,['class'=>'form-control','rows' => 2, 'cols' => 40]) }}
                                                    @else
                                                        {{$_datapolicy_->detail }} 
                                                    @endif
                                                </td>
                                                <td align="center">
                                                      @if($_datapolicy_->score=='1')
                                                        <label class="i-checks">
                                                            <input type="checkbox" id="cbx1" onClick="return readOnlyCheckBox()" CHECKED />
                                                        <i></i>
                                                    </label>
                                                    @else
                                                    <label class="i-checks">
                                                        <input type="checkbox" id="cbx2" onClick="return readOnlyCheckBox()"/>
                                                        <i></i>
                                                    </label>
                                                    @endif 
                                                </td>
                                                
                                                <td>
                                                    @if( $_datapolicy_->num_2 == '0')
                                                    <!--<input type="file" name="file" />-->
                                                    <div class="m-t">
                                                        <a href="{{ url('/local/public/file/').'/'.$_datapolicy_->path_file }} ">
                                                        <span class="fa fa-file-pdf-o text-danger m-r-sm" style="font-size: 20px;"></span>ชื่อไฟล์ : {{ $_datapolicy_->path_file }}
                                                        <br>
                                                    </div>
                                                    @endif    
                                                </td>
                                            
                                                <td>
                                                    {{$_datapolicy_->updated_at }} 
                                                </td>
    
                                        </tr>
                                         </form>    
                                        @endforeach
                                     
                                        
                                       
                                    </tbody>
                                </table>
    
    
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->



            </div>
          </div>
       
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
