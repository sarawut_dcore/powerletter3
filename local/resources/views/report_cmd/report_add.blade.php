@extends('layouts.master')

@section('title', 'รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')
    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                    <div class="col-sm-10 col-xs-7">
                        <h1 class="m-n font-thin h3 text-black">รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์  </h1>
                       <!-- main table --> 
                    </div>
                    
            </div>
        </div>
      <!-- users -->
      <div class="row">  
                <div class="col-md-12">
                    <div class="panel panel-success">
                        <div class="panel-heading wrapper b-b b-light">
                            <h5 class="font-thin m-t-none m-b-none text-muted"> รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h5>              
                        </div>
                        {{ Form::open(['method' => 'post' , 'url' => '/menu3'.'/'.$_id.'/save', 'files' => true,]) }}
                        <div class="panel-body">
                            <!-- /.table-responsive -->
                            <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">เรื่องที่มอบอำนาจ</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            {{ Form::textarea('topic',null, ['class' => 'form-control','placeholder' => 'เพิ่มเรื่องที่มอบอำนาจ' , 'cols'=>"66" , 'rows'=>"3" ]) }}
                                        </div>
                                    </div>
                                </div>
    
                                <div class="row">
                                <label class="control-label col-sm-2" for="form_name">คำสั่งกรมราชทัณฑ์เลขที่</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            {{ Form::text('command',null, ['class' => 'form-control','placeholder' => 'เพิ่มเลขที่คำสั่งกรมราชทัณฑ์']) }}
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                 <label class="control-label col-sm-2" for="form_name">ผู้รับมอบอำนาจ</label>
                                     <div class="col-sm-10">
                                         <div class="form-group">
                                             {{ Form::text('offer',null, ['class' => 'form-control','placeholder' => 'เพิ่มตำแหน่ง,ชื่อ']) }}
                                         </div>
                                     </div>
                                 </div>
    
                                 <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">ผู้รับมอบอำนาจต่อ</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            {{ Form::text('inheritor',null, ['class' => 'form-control','placeholder' => 'เพิ่มตำแหน่ง,ชื่อ']) }}
                                        </div>
                                    </div>
                                 </div>
    
                                 <div class="row">
                                     <label class="control-label col-sm-2" for="form_name">การดำเนินการ</label>
                                     <div class="col-sm-10">
                                         <div class="form-group">
                                             {{ Form::text('Action',null, ['class' => 'form-control','placeholder' => 'รายละเอียดการดำเนินการ']) }}
                                         </div>
                                     </div>
                                  </div>
                                <div class="row">
                                    <label class="control-label col-xs-8 col-sm-2 no-padding-right" for="id_asset2">สถานะ</label>
                                    <div class="col-sm-10">
                                            <select class="form-control" id="status" name="status">
                                                <option value="">เลือกสถานะ</option>
                                                <option value="รายงานแล้ว">รายงานแล้ว</option>
                                                <option value="ยังไม่ได้รายงาน">ยังไม่ได้รายงาน</option>
                                                <option value="ไม่มีผลการดำเนินงานประจำเดือน">ไม่มีผลการดำเนินงานประจำเดือน</option>
                                           </select>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                        <label class="control-label col-xs-8 col-sm-2 no-padding-right" for="id_asset">ผลการประเมิน</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="evaluation" id="evaluation">
                                                    <option value="">เลือกผลการประเมิน</option>
                                                    <option value="รายงานแล้ว">รายงานแล้ว</option>
                                                    <option value="ยังไม่ได้รายงาน">ยังไม่ได้รายงาน</option>
                                                    <option value="ไม่มีผลการดำเนินงานประจำเดือน">ไม่มีผลการดำเนินงานประจำเดือน</option>
                                            </select>
                                        </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="control-label col-xs-8 col-sm-2 no-padding-right" for="id_asset">ผู้ประเมิน</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="evaluator" id="evaluator">
                                                <option>เลือกผู้ประเมิน</option>
                                                @foreach($_data_evaluator as $out_data_evaluator)
                                                    <option value="{{$out_data_evaluator->id}}">{{$out_data_evaluator->name_department}}</option>
                                                @endforeach
                                        </select>
                                        <input type="hidden" name="id_cmd_log" id="id_cmd_log" value="{{$_id}}"/>
                                        @foreach ($_no_cmd as $out_no_cmd)
                                        @endforeach
                                        <input type="hidden" name="no_cmd" id="no_cmd" value="{{$out_no_cmd->id+1}}"/>
                                    </div>
                                </div>
                                <br>
                                {{-- <div class="row">
                                    <label class="control-label col-xs-8 col-sm-3 no-padding-right" for="id_asset" >เรือนจำ/ทัณฑสถาน</label>
                                    <div class="col-sm-9">
                                        <div class="clearfix">
                                            <select class="form-control" id="prison" name="prison">	
                                                    <option>เลือกเรือนจำ/ทัณฑสถาน</option>
                                                @foreach($_dataoffice as $out_dataoffice)	
                                                    <option>{{  $out_dataoffice->office_name  }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div> --}}

                                 <div class="row">
                                    <label class="control-label col-xs-8 col-sm-2 no-padding-right" for="id_asset" >เรือนจำ/ทัณฑสถาน</label>
                                    <div class="col-sm-10">
                                        <div class="clearfix">
                                            <?php $count = 1 ?>
                                            @foreach($_dataoffice as $out_dataoffice)
                                            <div class="col-sm-4">	
                                                <input type="checkbox" name="{{  $out_dataoffice->id }}" value="{{  $out_dataoffice->id }}"> {{  $out_dataoffice->office_name  }}<br>
                                                <?php $count=$count+1 ?>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <label class="control-label col-xs-8 col-sm-2 no-padding-right" for="id_asset">เอกสารแนบ</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="image" />
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="form-actions m-t-40">
                                        <div style="text-align:center;">
                                             <button type="submit" class="btn btn-success btn-outline waves-effect waves-light">Save</button>
                                            <a href="javascript:history.go(-1)">
                                                <button type="button" class="btn btn-danger btn-outline waves-effect waves-light"> Cancel</button>
                                            </a>
                                        </div>
                                    </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
@endsection

