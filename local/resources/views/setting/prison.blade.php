@extends('layouts.master')

@section('title', 'ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')
    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ตั้งค่าเรือนจำ</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel panel-success">
            <div class="row">
                <!-- main -->
                    <div class="col-12">
                        <div class="wrapper-md">
                            @if($errors->all())
                                <div class="alert alert-danger">
                                    {{ $errors->first() }}
                                </div>
                            @endif

                            <div class="bg-light lter b-b wrapper-md">
                                <div class="row">
                                    <div class="col-sm-9 col-xs-6">
                                        <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                                        <!-- main table --> 
                                    </div>
                                    <div class="col-sm-3 col-xs-6" >
                                        <div align="right">
                                            <button type="submit" class="btn btn-success" name = "btn-insert" data-toggle="modal" data-target="#ADD"> เพิ่มข้อมูล </button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- users -->
                    <div class="row">
                        <!--  1  -->  
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading wrapper b-b b-light">
                                    <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h5>              
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>ลำดับ</th>
                                                    <th>ชื่อเรือนจำ</th>
                                                    <th>ดำเนินการ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($_dataoffice as $out_dataoffice)
                                                    <tr>
                                                        <td>{{ $out_dataoffice->id }}</td>
                                                        <td>{{ $out_dataoffice->office_name }}</td>
                                                        
                                                        <td>
                                                            <button type="submit" class="btn btn-success" name = "btn-edit" data-toggle="modal" data-target="#EDIT{{ $out_dataoffice->id }}"> แก้ไข </button>
                                                            <button type="submit" class="btn btn-danger" name = "btn-edit" data-toggle="modal" data-target="#DELETE{{ $out_dataoffice->id }}"> ลบ </button></td>   
                                                    </tr>
                                                          
                                                    
                                                   
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>

          <!--pop up -->
          
          
        
          {{ Form::open(['method' => 'post' , 'url' => '/setting_prison/add']) }}  
          <div class="modal fade" id="ADD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title" id="myModalLabel">เพิ่มข้อมูลเรือนจำ</h4>
                      </div>
                      <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::text('office_name',null, ['class' => 'form-control','placeholder' => 'ชื่อเรือนจำ']) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                      <div class="panel-footer">
                          <div class="clearfix">
                              <span class="pull-right">
                                  <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                   <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                              </span>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          {{ Form::close() }}

          @foreach($_dataoffice as $out_dataoffice)
          {{ Form::open(['method' => 'post' , 'url' => '/setting_prison/'.$out_dataoffice->id.'/edit']) }}
              <div class="modal fade" id="EDIT{{ $out_dataoffice->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title" id="myModalLabel">แก้ไขข้อมูลเรือนจำ</h4>
                          </div>
                          <div class="panel-body">
                                <br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            {{ Form::text('office_name',$out_dataoffice->office_name, ['class' => 'form-control','placeholder' => 'ชื่อเรือนจำ']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>


                          <div class="panel-footer">
                              <div class="clearfix">
                                  <span class="pull-right">
                                      <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                       <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                  </span>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          {{ Form::close() }}
          @endforeach

          @foreach($_dataoffice as $out_dataoffice)
          {{ Form::open(['method' => 'post' , 'url' => '/setting_prison/'.$out_dataoffice->id.'/delete']) }}
          <div class="modal fade" id="DELETE{{ $out_dataoffice->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">ลบรายชื่อเรือนจำ ยืนยันที่จะลบหรือไม่</h4>
                    </div>
                    <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h5 class="font-thin m-t-none m-b-none text-muted"> ลบรายชื่อเรือนจำ : {{ $out_dataoffice->office_name }}</h5> 
                                    </div>
                                </div>
                            </div>
						</div>
                      <div class="panel-footer">
                          <div class="clearfix">
                              <span class="pull-right">
                                <button type="submit" class="btn btn-sm btn-success">ยืนยัน</button>
                                <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal">ยกเลิก</a>
                              </span>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          {{ Form::close() }}
          @endforeach
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>


    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection


