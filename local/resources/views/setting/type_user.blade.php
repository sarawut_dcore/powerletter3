@extends('layouts.master')

@section('title', 'ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')
    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ตั้งค่าสิทธิผู้ใช้งาน</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel panel-success">
            <div class="row">
                <!-- main -->
                    <div class="col-12">
                        <div class="wrapper-md">
                            @if($errors->all())
                                <div class="alert alert-danger">
                                    {{ $errors->first() }}
                                </div>
                            @endif

                            <div class="bg-light lter b-b wrapper-md">
                                <div class="row">
                                    <div class="col-sm-9 col-xs-6">
                                        <h1 class="m-n font-thin h3 text-black">ประเภทผู้ใช้งาน</h1>
                                        <!-- main table --> 
                                    </div>
                                    <div class="col-sm-3 col-xs-6" >
                                        <div align="right">
                                            <button type="submit" class="btn btn-success" name = "btn-insert" data-toggle="modal" data-target="#ADD"> เพิ่มข้อมูล </button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- users -->
                    <div class="row">
                        <!--  1  -->  
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading wrapper b-b b-light">
                                    <h5 class="font-thin m-t-none m-b-none text-muted"> ประเภทผู้ใช้งาน</h5>              
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>ลำดับ</th>
                                                    <th>ประเภทผู้ใช้&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        &nbsp;&nbsp;&nbsp;&nbsp;</th>
                                                    <th>ดำเนินการ</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($_datatype as $out_datatype)
                                                    <tr>
                                                        <td>{{ $out_datatype->id }}</td>
                                                        <td>{{ $out_datatype->name_user_type }}</td>
                                                        
                                                        <td>
                                                            <button type="submit" class="btn btn-success" name = "btn-edit" data-toggle="modal" data-target="#EDIT{{ $out_datatype->id }}"> แก้ไข </button>
                                                            <button type="submit" class="btn btn-danger" name = "btn-delete" data-toggle="modal" data-target="#DEL{{ $out_datatype->id }}"> ลบ </button></td>   
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                                
                            </div>
                        </div>   
                    </div>
                </div>
                
            </div>
        </div>
        </div>
        </div>
        </div>

         
        {{ Form::open(['method' => 'post' , 'url' => '/setting_type_user/save', 'files' => true,]) }}
            <div class="modal fade" id="ADD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">เพิ่มประเภทผู้ใช้งาน</h4>
						</div>
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::text('name',null, ['class' => 'form-control','placeholder' => 'ประเภทผู้ใช้']) }}
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="panel-footer">
                            <div class="clearfix">
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                     <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{ Form::close() }}

        @foreach($_datatype as $out_datatype)
        {{ Form::open(['method' => 'post' , 'url' => '/setting_type_user/'.$out_datatype->id.'/edit']) }}
            <div class="modal fade" id="EDIT{{ $out_datatype->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">แก้ไขประเภทผู้ใช้งาน</h4>
						</div>
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        {{ Form::text('name',$out_datatype->name_user_type, ['class' => 'form-control','placeholder' => 'ประเภทฝู้ใช้']) }}
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="panel-footer">
                            <div class="clearfix">
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                     <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{ Form::close() }}
        @endforeach

        @foreach($_datatype as $out_datatype)
        {{ Form::open(['method' => 'post' , 'url' => '/setting_type_user/'.$out_datatype->id.'/delete']) }}
            <div class="modal fade" id="DEL{{ $out_datatype->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">ลบประเภทผู้ใช้งาน</h4>
						</div>
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h5 class="font-thin m-t-none m-b-none text-muted"> ลบประเภทผู้ใช้งาน : {{ $out_datatype->name_user_type }}</h5> 
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="panel-footer">
                            <div class="clearfix">
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;ตกลง</button>
                                     <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{ Form::close() }}
        @endforeach
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
    
    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection


