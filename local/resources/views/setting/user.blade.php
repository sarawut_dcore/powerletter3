@extends('layouts.master')

@section('title', 'ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์')

@section('content')
    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col-12">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-12 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ตั้งค่าสิทธิผู้ใช้งาน</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
          <div class="col-md-12">
            <div class="panel panel-success">
            <div class="row">
                <!-- main -->
                    <div class="col">
                        <div class="wrapper-md">
                            @if($errors->all())
                                <div class="alert alert-danger">
                                    {{ $errors->first() }}
                                </div>
                            @endif

                            <div class="bg-light lter b-b wrapper-md">
                                <div class="row">
                                    <div class="col-sm-9 col-xs-6">
                                        <h1 class="m-n font-thin h3 text-black">รายชื่อผู้ใช้งาน</h1>
                                        <!-- main table --> 
                                    </div>
                                    <div class="col-sm-3 col-xs-6" >
                                        <div align="right">
                                            <button type="submit" class="btn btn-success" name = "btn-insert" data-toggle="modal" data-target="#ADD"> เพิ่มข้อมูล </button> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <!-- users -->
                    <div class="row">
                        <!--  1  -->  
                        <div class="col-md-12">
                            <div class="panel panel-success">
                                <div class="panel-heading wrapper b-b b-light">
                                    <h5 class="font-thin m-t-none m-b-none text-muted"> ผู้ใช้งาน</h5>              
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table id="example" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>username</th>
                                                    <th>name</th>
                                                    <th>telephone</th>
                                                    <th>group</th>
                                                    <th>office</th>
                                                    <th>note</th>
                                                    <th>station</th>
                                                    <th>action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($_datauser as $out_datauser)
                                                    <tr>
                                                        <td>{{ $out_datauser->id }}</td>
                                                        <td>{{ $out_datauser->username }}</td>
                                                        <td>{{ $out_datauser->name }}</td>
                                                        <td>{{ $out_datauser->telephone }}</td>
                                                        <td>{{ $out_datauser->name_user_type }}</td>
                                                        <td>{{ $out_datauser->office_name }}</td>
                                                        <td>{{ $out_datauser->note }}</td>
                                                        <td>{{ $out_datauser->name_station }}</td>
                                                        
                                                        <td>
                                                            <button type="submit" class="btn btn-success" name = "btn-edit" data-toggle="modal" data-target="#EDIT{{ $out_datauser->id }}"> แก้ไข </button>
                                                            <button type="submit" class="btn btn-danger" name = "btn-delete" data-toggle="modal" data-target="#DEL{{ $out_datauser->id }}"> &nbsp;&nbsp;ลบ&nbsp;&nbsp;&nbsp; </button></td>   
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                        </div>   
                    </div>
                </div>
            </div>
        </div>
        </div>
        </div>
        </div>

         
        {{ Form::open(['method' => 'post' , 'url' => '/setting_user/save', 'files' => true,]) }}
            <div class="modal fade" id="ADD" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">เพิ่มผู้ใช้งาน</h4>
						</div>
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Username</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        {{ Form::text('username',null, ['class' => 'form-control','placeholder' => 'ชื่อฝู้ใช้']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Password</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        {{ Form::password('password',null, ['class' => 'form-control','placeholder' => 'รหัสผ่าน']) }}
                                        <label>&nbsp;&nbsp;&nbsp;&nbsp;<font color="red">*ต้องใส่รหัสผ่าน</font></label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Name</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        {{ Form::text('name',null, ['class' => 'form-control','placeholder' => 'ชื่อ']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Telephone</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        {{ Form::text('telephone',null, ['class' => 'form-control','placeholder' => 'เบอร์โทรศัพท์']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Group</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <select class="form-control" id="group" name="group">	
                                                <option>เลือกประเภท</option>
                                            @foreach($_datauser_type as $out_datauser_type)	
                                                <option>{{  $out_datauser_type->id  }}</option>
                                            @endforeach
                                        </select>
                                        <!--{{ Form::text('group',null, ['class' => 'form-control','placeholder' => 'กลุ่มผู่้ใช้']) }}-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Office</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <select class="form-control" id="office_name" name="office_name">	
                                                <option>เลือกสำนักงาน</option>
                                            @foreach($_dataoffice as $out_dataoffice)	
                                                <option>{{  $out_dataoffice->office_name  }}</option>
                                            @endforeach
                                        </select>
                                        <!--{{ Form::text('office_name',null, ['class' => 'form-control','placeholder' => 'สถานที่ทำงาน']) }}-->
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Note</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        {{ Form::textarea('note',null, ['class' => 'form-control','placeholder' => 'Note','rows' => 2, 'cols' => 40]) }}
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Station</label>
                                <div class="col-sm-10">
                                    <div class="form-group">
                                        <select class="form-control" id="station_id" name="station_id">	
                                                <option>เลือกสถานที่ทำงาน</option>
                                            @foreach($_datastation as $out_datastation)	
                                                <option>{{  $out_datastation->id  }}</option>
                                            @endforeach
                                        </select>
                                        <!--{{ Form::text('station_id',null, ['class' => 'form-control','placeholder' => 'สถานที่ทำงาน']) }}-->
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="panel-footer">
                            <div class="clearfix">
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                     <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{ Form::close() }}

        @foreach($_datauser as $out_datauser)
        {{ Form::open(['method' => 'post' , 'url' => '/setting_user/'.$out_datauser->id.'/edit']) }}
            <div class="modal fade" id="EDIT{{ $out_datauser->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">แก้ไขผู้ใช้งาน</h4>
						</div>
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Username</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            {{ Form::text('username',$out_datauser->username, ['class' => 'form-control','placeholder' => 'ชื่อฝู้ใช้', 'readonly']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Name</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            {{ Form::text('name',$out_datauser->name, ['class' => 'form-control','placeholder' => 'ชื่อ']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Telephone</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            {{ Form::text('telephone',$out_datauser->telephone, ['class' => 'form-control','placeholder' => 'เบอร์โทรศัพท์']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Group</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <select class="form-control" id="group" name="group">	
                                                    <option>{{  $out_datauser->group  }}</option>
                                            @foreach($_datauser_type as $out_datauser_type)	
                                                <option>{{  $out_datauser_type->id  }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Office</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <select class="form-control" id="office_name" name="office_name">	
                                                <option>{{  $out_datauser->office_name  }}</option>
                                            @foreach($_dataoffice as $out_dataoffice)	
                                                <option>{{  $out_dataoffice->office_name  }}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Note</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            {{ Form::textarea('note',$out_datauser->note, ['class' => 'form-control','placeholder' => 'Note','rows' => 2, 'cols' => 40]) }}
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-2" for="form_name">&nbsp;&nbsp;&nbsp;&nbsp;Station</label>
                                    <div class="col-sm-10">
                                        <div class="form-group">
                                            <select class="form-control" id="station_id" name="station_id">	
                                                <option>{{  $out_datauser->station_id }}</option>
                                            @foreach($_datastation as $out_datastation)	
                                                <option>{{  $out_datastation->id  }}</option>
                                            @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
						</div>
                        <div class="panel-footer">
                            <div class="clearfix">
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-plus"></i>&nbsp;&nbsp;บันทึก</button>
                                     <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{ Form::close() }}
        @endforeach

        @foreach($_datauser as $out_datauser)
        {{ Form::open(['method' => 'post' , 'url' => '/setting_user/'.$out_datauser->id.'/delete']) }}
            <div class="modal fade" id="DEL{{ $out_datauser->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
								<h4 class="modal-title" id="myModalLabel">ลบผู้ใช้งาน</h4>
						</div>
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <h5 class="font-thin m-t-none m-b-none text-muted"> ลบผู้ใช้งาน : {{ $out_datauser->username }}</h5> 
                                    </div>
                                </div>
                            </div>
						</div>
                        <div class="panel-footer">
                            <div class="clearfix">
                                <span class="pull-right">
                                    <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i>&nbsp;&nbsp;ตกลง</button>
                                     <a type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>&nbsp;&nbsp;ยกเลิก</a>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        {{ Form::close() }}
        @endforeach
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>

    <script>
        $(document).ready(function() {
            $('#example').DataTable();
        } );
    </script>
@endsection


