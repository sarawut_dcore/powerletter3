<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\IndicatorType
 *
 * @property int $id
 * @property string|null $name
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorType whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class IndicatorType extends Model
{
    protected $table = 'indicator_type';
}
