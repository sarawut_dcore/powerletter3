<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TransactionEva
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $department_id
 * @property int|null $indicator_choice_id
 * @property int|null $score
 * @property string|null $path_file
 * @property string|null $comment
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva whereIndicatorChoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva wherePathFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionEva whereUserId($value)
 * @mixin \Eloquent
 */
class TransactionEva extends Model
{
    protected $table = 'transaction_evaluate';
}
