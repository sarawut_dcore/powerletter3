<?php

namespace App\Http\Controllers;
use DB;
use App\policy_menu;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class policy_menu_controller extends Controller
{
    public function getIndex(  ) {
        
       $datapolicy_menu = DB ::table('policy_menu')->select('id')->get();

       $count_datapolicy_menu = policy_menu::All()->count();
       
       $datapolicy_menu = policy_menu::All();
      
       return view('policy/menu_policy', ['_datapolicy_menu' =>  $datapolicy_menu] );
 
    }

}
