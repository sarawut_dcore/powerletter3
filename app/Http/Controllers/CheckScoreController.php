<?php

namespace App\Http\Controllers;
use App\User;
use App\Indicator;
use App\IndicatorChoice;
use App\TransactionChoice;
use App\IndicatorSubDetail;
use App\TransactionEva;
use App\Station;


use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CheckScoreController extends Controller
{
    public function getIndex() {


        
        $_txt_name = Station::where('id',Auth::user()->station_id)->get();
        $_office_name = User::where('username',Auth::user()->username)->get();
        $_ChkScore = User::where('username',Auth::user()->username)
                                ->Join('indicator', 'indicator.station_id', '=','user.station_id')
                                ->leftJoin('transaction_evaluate',function ($join) {
                                    $join->On('transaction_evaluate.indicator_id', '=','indicator.id')
                                         ->On( 'transaction_evaluate.user_name', '=', 'user.username'); 
                                })
                                ->select(
                                        'user.id',
                                        'user.username',
                                        'user.office_name',
                                        'indicator.id AS indicator_id',
                                        'indicator.detail',
                                        'indicator.indicator_type_id',
                                        'indicator.target',
                                        'indicator.percent',
                                        'indicator.station_id',
                                        'user.station_id AS user_station',
                                        'transaction_evaluate.user_name',
                                        'transaction_evaluate.indicator_id as eva_ind_id',
                                        'transaction_evaluate.score',
                                        'transaction_evaluate.comment',
                                        'transaction_evaluate.user_evaluate',
                                        'transaction_evaluate.name_evaluate',
                                        'transaction_evaluate.telephone_evaluate'
                                        )
                                        ->orderBy('indicator_id')
                                        ->get();
       //return   $_ChkScore;
         return view('checkscore.index',['_ChkScore'=>$_ChkScore,'_txt_name'=>$_txt_name , '_office_name_'=>$_office_name ]);
    }
    public function getIndex2() {


        
        $_txt_name = Station::where('id',Auth::user()->station_id2)->get();
        $_office_name = User::where('username',Auth::user()->username)->get();
        $_ChkScore = User::where('username',Auth::user()->username)
                                ->Join('indicator', 'indicator.station_id', '=','user.station_id2')
                                ->leftJoin('transaction_evaluate',function ($join) {
                                    $join->On('transaction_evaluate.indicator_id', '=','indicator.id')
                                         ->On( 'transaction_evaluate.user_name', '=', 'user.username'); 
                                })
                                ->select(
                                        'user.id',
                                        'user.username',
                                        'user.office_name',
                                        'indicator.id AS indicator_id',
                                        'indicator.detail',
                                        'indicator.indicator_type_id',
                                        'indicator.target',
                                        'indicator.percent',
                                        'indicator.station_id',
                                        'user.station_id AS user_station',
                                        'transaction_evaluate.user_name',
                                        'transaction_evaluate.indicator_id as eva_ind_id',
                                        'transaction_evaluate.score',
                                        'transaction_evaluate.comment',
                                        'transaction_evaluate.user_evaluate',
                                        'transaction_evaluate.name_evaluate',
                                        'transaction_evaluate.telephone_evaluate'
                                        )
                                        ->orderBy('indicator_id')
                                        ->get();
       //return   $_ChkScore;
         return view('checkscore.index',['_ChkScore'=>$_ChkScore,'_txt_name'=>$_txt_name , '_office_name_'=>$_office_name ]);
    }
}
