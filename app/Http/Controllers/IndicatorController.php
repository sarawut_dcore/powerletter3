<?php

namespace App\Http\Controllers;

use App\Indicator;
use App\IndicatorChoice;
use App\TransactionChoice;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class IndicatorController extends Controller
{
    public function getIndex(  ) {
        
        if( Auth::user()->group < 2 )
        {
          if(Auth::user()->username <> 'user144' )
          return "<h1>ท่านไม่สามารถรายงานตัวชี้วัดได้เนื่องจากเกินเวลาที่กำหนด</h1>";
        }
        
        

        $indicators = Indicator::where('station_id', Auth::user()->station_id)
         ->orderBy('indicator.id')
         ->get();
        $indicators->each(function ($item) {
            $item->choices;
            $item->subDetails;
            $item->type;
            $item->isPercent = $item->isPercent();
            $item->isCount = $item->isCount();
            $item->isStep = $item->isStep();
        });

        $transactionChoices = TransactionChoice::where('user_id', Auth::user()->id)->get();
        $indicatorChoices = IndicatorChoice::whereIn('indicator_id', $indicators->pluck('id')->toArray())
            ->join('indicator', 'indicator_choice.indicator_id', '=', 'indicator.id')
            ->leftJoin('transaction_choice', function ($join) {
                $join->on('transaction_choice.indicator_choice_id', '=', 'indicator_choice.id')
                    ->where('transaction_choice.user_id', '=', Auth::user()->id);
            })
            ->select('indicator.indicator_type_id as type', 'indicator_choice.id as id', 'transaction_choice.score as answer', 'transaction_choice.path_file as path_file')
            
            ->get();

        $indicatorChoices->each(function ($item) {
            if($item->type == 3) {
                if($item->answer) {
                    $item->answer = 1;
                } else {
                    $item->answer = 0;
                }
            }
        });
      
        return view('indicator.index2', compact('indicators', 'transactionChoices', 'indicatorChoices'));
    }

    public function getIndex2(  ) {
        
        if(Auth::user()->group < 2 )
        {
           // if(Auth::user()->username <> 'user144' )
        // return "<h1>ท่านไม่สามารถรายงานตัวชี้วัดได้เนื่องจากเกินเวลาที่กำหนด</h1>";
        }
        

        $indicators = Indicator::where('station_id', Auth::user()->station_id2)
         ->orderBy('indicator.id')
         ->get();
        $indicators->each(function ($item) {
            $item->choices;
            $item->subDetails;
            $item->type;
            $item->isPercent = $item->isPercent();
            $item->isCount = $item->isCount();
            $item->isStep = $item->isStep();
            $item->isStepEdit = $item->isStepEdit();
        });

        $transactionChoices = TransactionChoice::where('user_id', Auth::user()->id)->get();
        $indicatorChoices = IndicatorChoice::whereIn('indicator_id', $indicators->pluck('id')->toArray())
            ->join('indicator', 'indicator_choice.indicator_id', '=', 'indicator.id')
            ->leftJoin('transaction_choice', function ($join) {
                $join->on('transaction_choice.indicator_choice_id', '=', 'indicator_choice.id')
                    ->where('transaction_choice.user_id', '=', Auth::user()->id);
            })
            ->select('indicator.indicator_type_id as type', 'indicator_choice.id as id', 'transaction_choice.score as answer', 'transaction_choice.path_file as path_file')
            
            ->get();

        $indicatorChoices->each(function ($item) {
            if($item->type == 3) {
                if($item->answer) {
                    $item->answer = 1;
                } else {
                    $item->answer = 0;
                }
            }
        });
      
        return view('indicator2.index2', compact('indicators', 'transactionChoices', 'indicatorChoices'));
    }

    public function postAjaxSaveRecord(Request $request) {

  
     //   $transactionChoices = TransactionChoice::where (['indicator_choice_id', $request->id], )->first();
        $transactionChoices = TransactionChoice::where('indicator_choice_id', $request->id )->where('user_id',Auth::user()->id)->first();

        if(!$transactionChoices) {
            $transactionChoices = new TransactionChoice();
        }


        if($request->hasFile('file')) {
            $filename = 'file_' . Auth::user()->username .'_'. $request->id .'_'. Carbon::now()->toDateString() .'_' . str_random(8) . '.' . $request->file('file')->getClientOriginalExtension();
            $request->file('file')->move(public_path('/file'), $filename);
            $transactionChoices->path_file = $filename;
        }

        $transactionChoices->user_id = Auth::user()->id;
        $transactionChoices->user_name = Auth::user()->username;
        $transactionChoices->department_id = Auth::user()->station_id;
        $transactionChoices->department_id2 = Auth::user()->station_id2;
        $transactionChoices->indicator_choice_id = $request->id;
        $transactionChoices->score = $request->answer;

        $transactionChoices->comment = null;
        $transactionChoices->save();
   
        return response()->json(['message' => 'บันทึกสำเร็จ', 'filename' => $transactionChoices->path_file], 200);

    }
}
