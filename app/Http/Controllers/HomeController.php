<?php

namespace App\Http\Controllers;

use App\User;
use App\Indicator;
use App\IndicatorChoice;
use App\TransactionChoice;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class HomeController extends Controller
{

    public function getIndex() {
      
        
        // ผู้เข้าใช้งาน                _Count_User_Use
        // ไม่ได้เข้าใช้งาน             _Count_User_Non
        // จำนวนผู้ใช้งานในระบบทั้งหมด  _Count_User_All
        // จำนวนผู้ตรวจประเมินผล       _Count_User_Admin
        // จำนวนเอกสารตัวชี้วัด         _Count_TransactionChoice
        // จำนวนแบบฟอร์มตัวชี้วัด       _Count_Indicator_From
        // รวมข้อมูล                 _Count_SUM
        //
        //$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
        //echo json_encode($arr);
        //
        // รายชื่อผู้ดูระบบและผู้ตรวจประเมินผล _UserAdmin
        // รายชื่อผู้ใช้งานแต่ละเรือนจำ _UserNormal
       
        $_Count_User_All = User::All()->count();
        $_Count_User_Use = User::where('remember_token','<>',null)->count();
        $_Count_User_Use = number_format(($_Count_User_Use /  $_Count_User_All)*100,2);
        $_Count_User_Non = User::where('remember_token',null)->count();
        $_Count_User_Non = number_format(($_Count_User_Non /  $_Count_User_All)*100,2);
       
        $_Count_User_Admin = User::where('group','2')->count();
        $_Count_TransactionChoice = TransactionChoice::All()->count(); 
        $_Count_Indicator_From = 18;

        $arr_Count_SUM = array ('Count_User_Use'=>$_Count_User_Use,'Count_User_Non'=>$_Count_User_Non 
                                ,'Count_User_All' =>$_Count_User_All,'Count_User_Admin'=>$_Count_User_Admin
                                ,'Count_TransactionChoice'=>$_Count_TransactionChoice,'Count_Indicator_From'=>$_Count_Indicator_From);
        $_Count_SUM =json_encode($arr_Count_SUM);

        $_UserAdmin = User::where('group','>','1')->get();
        $_UserNormal = User::where('group','1')->get();
       // $_TransactionChoice = TransactionChoice::where('user_name','user144')->where('id','1')->get();;
 
      //  return view('page.index',['name' => 'James']);
      //return view('home.index');
      // return view('page.index',['_UserAdmin' =>  $_UserAdmin , '_UserNormal' =>  $_UserNormal  , '_Count_SUM' => $_Count_SUM]);
         return view('home.index',['_UserAdmin' =>  $_UserAdmin , '_UserNormal' =>  $_UserNormal , '_Count_SUM' => $_Count_SUM]);
      
     
    }
    public function getIndex2() {
        return view('1dashboard');
       //return ('Brown');
    }
}
