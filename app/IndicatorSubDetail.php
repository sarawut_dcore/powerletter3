<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\IndicatorSubDetail
 *
 * @property int $id
 * @property string|null $detail
 * @property int|null $indicator_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorSubDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorSubDetail whereDetail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorSubDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorSubDetail whereIndicatorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\IndicatorSubDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class IndicatorSubDetail extends Model
{
    protected $table = 'indicator_sub_detail';
}
