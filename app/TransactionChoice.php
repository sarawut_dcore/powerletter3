<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\TransactionChoice
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $department_id
 * @property int|null $indicator_choice_id
 * @property int|null $score
 * @property string|null $path_file
 * @property string|null $comment
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice whereIndicatorChoiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice wherePathFile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\TransactionChoice whereUserId($value)
 * @mixin \Eloquent
 */
class TransactionChoice extends Model
{
    protected $table = 'transaction_choice';
}
