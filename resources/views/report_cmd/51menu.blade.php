@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')

<div id="content" class="app-content" role="main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> ตารางสรุปผลแบบรายงานการใช้อำนาจของผู้รับมอบอำนาจในการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
                <div class="panel-heading">
                    
                        ช่วงเวลา
                    </div>
                    <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                                <tr>
                                                    
                                                    <th>เดือน มกราคม</th>
                                                    
                                                    <th>พ.ศ. 2561</th>
                                                    
                                                </tr>
                                            </thead>
                                </table>
                            </div>
                    </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    
                        กองบริการทางการแพทย์
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>เรื่องที่มอบอำนาจ</th>
                                    <th>คำสั่งกรมราชทัณฑ์เลขที่</th>
                                    <th>ผู้รับมอบอำนาจ</th>
                                    <th>ผู้รับมอบอำนาจต่อ</th>
                                    <th>การดำเนินการ</th>
                                    <th>สถานะ</th>
                                    <th>เอกสารแนบ</th>
                                    <th>เรือนจำ/ทัณฑสถาน</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>การอนุญาตให้นำตัวผู้ต้องขังป่วยออกรักษาตัวภายนอกเรือนจำ</td>
                                    <td>1. 312/2550
                                    26 มี.ค. 2550
                                    
                                    2. 313/2550
                                    26 มี.ค. 2550</td>
                                    <td>ผู้ว่าราชการจังหวัด</td>
                                    <td>-</td>
                                    <td>นำตัวผู้ต้องขังป่วยออกไปรักษาภายนอก</td>
                                    <td>□</td>
                                    <td>....</td>
                                    <td>ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี</td>
                                    
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>การจัดซื้อหรือจัดจ้าง ตลอดทั้งการอนุมัติซื้อ หรือสั่งจ้าง และทำสัญญาหรือข้อตกลงรวมถึงการแก้ไขเปลี่ยนแปลงและการเบิกจ่ายเงินบำรุงโรงพยาบาล</td>
                                    <td>1542/2550
                                    26 ธ.ค. 2550</td>
                                    <td>ผู้อำนวยการทัณฑสถานโรงพยาบาลราชทัณฑ์</td>
                                    <td>-</td>
                                    <td>ดำเนินการเกี่ยวกับการพัสดุในการบำรุงโรงพยาบาล</td>
                                    <td>●</td>
                                    <td>....</td>
                                    <td>ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี</td>
                                    
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>การเบิกจ่ายจากโครงการกองทุนโลกด้านวัณโรค SSF ปีที่ 1  </td>
                                    <td>89/55
                                    25 ม.ค. 2555
                                    
                                    353/2555
                                    26 มี.ค. 2555</td>
                                    <td>ผู้อำนวยการทัณฑสถานโรงพยาบาลราชทัณฑ์</td>
                                    <td>-</td>
                                    <td>การดำเนินการเกี่ยวกับการจัดกิจกรรมด้านการบริหารจัดการวัณโรคดื้อยาหลายขนาน</td>
                                    <td>●</td>
                                    <td>....</td>
                                    <td>ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี</td>
                                    
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>การลงนามข้อตกลงให้บริการสาธารณสุข ตาม พรบ. หลักประกันสุขภาพแห่งชาติ พ.ศ. 2545 ในพื้นที่ กทม.</td>
                                    <td>-</td>
                                    <td>ผู้อำนวยการทัณฑสถานโรงพยาบาลราชทัณฑ์</td>
                                    <td>-</td>
                                    <td>ลงนามในหนังสือข้อตกลง</td>
                                    <td>●</td>
                                    <td>....</td>
                                    <td>ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี</td>
                                    
                                </tr>

                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        
    </div>

   
</div>








@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection