@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')

<div id="content" class="app-content" role="main">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header"> ตารางสรุปผลแบบรายงานการใช้อำนาจของผู้รับมอบอำนาจในการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
          <div class="panel panel-default">
                <div class="panel-heading">
                    
                        ช่วงเวลา
                    </div>
                    <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                                <tr>
                                                    
                                                    <th>เดือน มกราคม</th>
                                                    
                                                    <th>พ.ศ. 2561</th>
                                                    
                                                </tr>
                                            </thead>
                                </table>
                            </div>
                    </div>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    
                กองทัณฑวิทยา
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table  table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ลำดับ</th>
                                    <th>เรื่องที่มอบอำนาจ</th>
                                    <th>คำสั่งกรมราชทัณฑ์เลขที่</th>
                                    <th>ผู้รับมอบอำนาจ</th>
                                    <th>ผู้รับมอบอำนาจต่อ</th>
                                    <th>การดำเนินการ</th>
                                    <th>สถานะ</th>
                                    <th>เอกสารแนบ</th>
                                    <th>เรือนจำ/ทัณฑสถาน</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>การอนุญาตให้พกพาอาวุธปืนและเครื่องกระสุนปืนออกไปนอกบริเวณที่ตั้งของหน่วยงานราชการ</td>
                                    <td>276/2544
                                    30 มี.ค. 44
                                    </td>
                                    <td>ผู้ว่าราชการจังหวัด</td>
                                    <td>ผู้บัญชาการเรือนจำ</td>
                                    <td>การดำเนินการจ่ายนักโทษเด็ดขาดออกทำงานสาธารณะ</td>
                                    <td>□</td>
                                    <td>....</td>
                                    <td>ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี</td>
                                    
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>การพิจารณาอนุญาตให้นักศึกษา บุคคล หรือหน่วยงานอื่นที่ประสงค์จะเก็บข้อมูลเกี่ยวกับการทำวิจัยทำวิทยานิพนธ์ ในเรื่องที่เกี่ยวข้องกับการดำเนินงานของกรมราชทัณฑ์ หรือในเรือนจำ ทัณฑสถาน</td>
                                    <td>
                                    <table class="table">
       <tr>  <td>1. 1807/2560 
25 ธ.ค. 60 <p style="margin-bottom:40px"><p></td>   </tr> <tr>  <td> <p style="margin-bottom:40px">2. 1806/2560 
25 ธ.ค. 60 </td>   </tr>
       <tr>  <td>3. 1805/2560 
25 ธ.ค. 60 </td>   </tr>
          </table>
                                    </td>
                                    <td> <table class="table" >
       <tr > <td ><p><p> ผู้ว่าราชการจังหวัด </td>   </tr>
       <tr>  <td><p><p> ผู้บัญชาการเรือนจำ </td>   </tr>
       <tr>  <td> ผู้อำนวยการสำนักทัณฑวิทยา </td>   </tr>
          </table></td>
                                    <td> <table class="table">
       <tr>  <td>ผู้บัญชาการเรือนจำ/ 
ผู้อำนวยการทัณฑสถาน </td></tr>
       <tr>  <td></td>   </tr>
     
          </table></td>
                                    <td> <table  border="0">
       <tr>  <td>  </td>   </tr>
       <tr>  <td> </td>   </tr>
       <tr>  <td></td>   </tr>
          </table></td>
                                    <td> <table class="table">
       <tr>  <td><p><p> ● </td>   </tr>
       <tr>  <td></td>   </tr>
       
          </table></td>
                                    <td> <table border="0">
       <tr>  <td>  </td>   </tr>
       <tr>  <td> </td>   </tr>
       <tr>  <td>  </td>   </tr>
          </table></td>
                                    <td> <table class="table">
       <tr>  <td> ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี </td>   </tr>
       <tr>  <td>  </td>   </tr>
     
          </table></td>
                                    
                                </tr>

                                <td>3</td>
                                    <td>การพิจารณาอนุญาตให้บุคคล หรือนิติบุคคลที่มีความประสงค์จะเข้าไปในเรือนจำเพื่อสัมภาษณ์ผู้ต้องขัง รวมถึงการบันทึกภาพ ภาพยนตร์ วิดีทัศน์ภายในเรือนจำ </td>
                                    <td>
                                    <table class="table">
       <tr>  <td><p style="margin-bottom:40px">1. 1665/2560 
       4 ธ.ค. 60</td>   </tr>
       <tr>  <td><p style="margin-bottom:40px">2. 1664/2560 
       4 ธ.ค. 60</td>   </tr>
          </table>
                                    </td>
                                    <td> <table class="table" >
       <tr > <td ><p><p> ผู้ว่าราชการจังหวัด </td> </tr>
       <tr> <td> ผู้บัญชาการเรือนจำ/ 
ผู้อำนวยการทัณฑสถาน </td>   </tr>
       
          </table></td>
                                    <td> <table class="table">
       <tr>  <td> ผู้บัญชาการเรือนจำ/ 
       ผู้อำนวยการทัณฑสถาน </td></tr>
       <tr>  <td></td>   </tr>
     
          </table></td>
                                    <td> <table  border="0">
       <tr>  <td>  </td>   </tr>
       <tr>  <td> </td>   </tr>
       <tr>  <td></td>   </tr>
          </table></td>
                                    <td> <table class="table">
       <tr>  <td><p><p> ● </td>   </tr>
       <tr>  <td></td>   </tr>
       
          </table></td>
                                    <td> <table border="0">
       <tr>  <td>  </td>   </tr>
       <tr>  <td> </td>   </tr>
       <tr>  <td>  </td>   </tr>
          </table></td>
                                    <td> <table class="table">
       <tr>  <td> ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี </td>   </tr>
       <tr>  <td>  </td>   </tr>
     
          </table></td>
                                    
                                </tr>
                              
                              
                                <tr>
                                    <td>4</td>
                                    <td>ขออนุญาติให้บุคคลภายนอกเข้าชมกิจการหรือติดต่องานกับเรือนจำ</td>
                                    <td>*ตามข้อบังคับกรมราชทัณฑ์
ว่าด้วยการเยี่ยมการติดต่อของบุคคลภายนอกต่อผู้ต้องขังและการเข้าดูกิจการหรือติดต่อการงานกับเรือนจำ พ.ศ. 2555 ข้อ 22-23</td>
                                    <td>ผู้ว่าราชการจังหวัด</td>
                                    <td>ผู้บัญชาการเรือนจำ</td>
                                    <td>-</td>
                                    <td>●</td>
                                    <td>....</td>
                                    <td>ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี</td>
                                    
                                </tr>
                              
                               
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        
    </div>

   
</div>








@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection