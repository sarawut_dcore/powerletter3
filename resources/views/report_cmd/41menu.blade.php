@extends('layouts.master')

@section('title', 'นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์')

@section('content')
  

<div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
            <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header"> ตารางสรุปผลแบบรายงานการใช้อำนาจของผู้รับมอบอำนาจในการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="col-lg-12">
                        <div class="panel panel-default">
                              <div class="panel-heading">
                                  
                                      ช่วงเวลา
                                  </div>
                                  <div class="panel-body">
                                          <div class="table-responsive">
                                              <table class="table table-striped table-bordered table-hover">
                                                      <thead>
                                                              <tr>
                                                                   <tr> เดือน</tr>
                                                                    <div class="dropdown">
                                                                            
                                                                            <div class="col-sm-3">
                                                                                    <select class="form-control">
                                                                                      <option value="+47">Norge (+47)</option>
                                                                                    
                                                                                    </select>
                                                                             </div>

                                                                          
                                                                    </div>
                                                                   
                                                                  <th>พ.ศ. 2561</th>
                                                                  
                                                                  
                                                              </tr>
                                                          </thead>
                                              </table>
                                          </div>
                                  </div>
                          </div>
                      </div>
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนำจกำรปฏิบัติรำชกำรแทนอธิบดีกรมรำชทัณฑ์</h5>              
              </div>
                                 <!-- /.panel-heading -->
                                 <div class="panel-body">
                                    <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                            <th>ลำดับ</th>
                                            <th>เรื่องที่มอบอำนาจ</th>
                                            <th>คำสั่งกรมราชทัณฑ์เลขที่</th>
                                            <th>ผู้รับมอบอำนาจ</th>
                                            <th>ผู้รับมอบอำนาจต่อ</th>
                                            <th>การดำเนินการ</th>
                                            <th>สถานะ</th>
                                            <th>เอกสารแนบ</th>
                                            <th>เรือนจำ/ทัณฑสถาน</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                        
                                                <tr>
                                                    <td>1</td>
                                                    <td>การอนุมัติการเบิกจ่ายเงินสวัสดิการเกี่ยวกับการรักษาพยาบาล การศึกษาบุตร <br> และค่าเช่าบ้านข้าราชการ ของผู้บัญชาการเรือนจำ ผู้อำนวยการทัณฑสถาน สถานกักกัน และสถานที่กักขัง  ในสังกัดราชการบริหารส่วนกลางและราชการบริหารส่วนภูมิภาค</td>
                                                    <td>40/2554<br> 14 ม.ค. 2554</td>
                                                    <td>ผู้ว่าราชการจังหวัด</td>
                                                    <td>-</td>
                                                    <td>-</td>
                                                    <td>□</td>
                                                    <td>....</td>
                                                    <td>ทัณฑสถานบำบัดพิเศษจังหวัดปทุมธานี</td>
                                                    
                                                </tr>
                                 
                                    
                                   
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!--    -->  
        </div>
    </div>
</div>
<!-- / main -->
</div>
</div>
</div>
<!-- /content -->







@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection