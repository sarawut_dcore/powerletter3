@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')
    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนำจกำรปฏิบัติรำชกำรแทนอธิบดีกรมรำชทัณฑ์</h5>              
              </div>
                                 <!-- /.panel-heading -->
                    <div class="panel-body">
                            <div class="table-responsive">
                                    <table cellspacing="0" border="0">
                                            <colgroup width="56"></colgroup>
                                            <colgroup width="362"></colgroup>
                                            <tr>
                                                <td style="border-bottom: 1px solid #000000" colspan=2 height="27" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=4 color="#000000">รายงานมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</font></b></td>
                                                </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=bottom bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">ลำดับ</font></b></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=bottom bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">หน่วยงาน</font></b></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=bottom sdval="1" sdnum="1033;"><font face="TH SarabunIT๙" size=4 color="#000000">1</font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><u><font face="TH SarabunIT๙" size=4 color="#000000">
                                                        <a href = "{{ url('/menu41') }}" > 
                                                    กองบริหารการคลัง</font></u></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=bottom sdval="2" sdnum="1033;"><font face="TH SarabunIT๙" size=4 color="#000000">2</font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><u><font face="TH SarabunIT๙" size=4 color="#000000">
                                                    <a href = "{{ url('/menu51') }}" > 
                                                    กองบริการทางการแพทย์</font></u></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=bottom sdval="3" sdnum="1033;"><font face="TH SarabunIT๙" size=4 color="#000000">3</font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><u><font face="TH SarabunIT๙" size=4 color="#000000">
                                                        <a href = "{{ url('/menu61') }}" > 
                                                    กองบริหารทรัพยากรบุคคล</font></u></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=bottom sdval="4" sdnum="1033;"><font face="TH SarabunIT๙" size=4 color="#000000">4</font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><u><font face="TH SarabunIT๙" size=4 color="#000000">
                                                        <a href = "{{ url('/menu71') }}" > 
                                                    กองพัฒนาพฤตินิสัย</font></u></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=bottom sdval="5" sdnum="1033;"><font face="TH SarabunIT๙" size=4 color="#000000">5</font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><u><font face="TH SarabunIT๙" size=4 color="#000000">
                                                        <a href = "{{ url('/menu81') }}" > 
                                                    กองทัณฑปฏิบัติ</font></u></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=bottom sdval="6" sdnum="1033;"><font face="TH SarabunIT๙" size=4 color="#000000">6</font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><u><font face="TH SarabunIT๙" size=4 color="#000000">
                                                        <a href = "{{ url('/menu91') }}" > 
                                                    กองทัณฑวิทยา</font></u></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=bottom sdval="7" sdnum="1033;"><font face="TH SarabunIT๙" size=4 color="#000000">7</font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=bottom><u><font face="TH SarabunIT๙" size=4 color="#000000">อื่นๆ</font></u></td>
                                            </tr>
                                        </table>
    
    
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
            </div>
          </div>
       
        
        
        
        
        
        
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection

