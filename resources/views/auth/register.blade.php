
<!DOCTYPE html>
<html lang="en" class="">
<head>
    <meta charset="utf-8" />
    <title>สมัครสมาชิก - {{ Config::get('app.name') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/animate.css/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/font-awesome/css/font-awesome.min.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/assets/simple-line-icons/css/simple-line-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/libs/jquery/bootstrap/dist/css/bootstrap.css') }}" type="text/css" />

    <link rel="stylesheet" href="{{ url('assets/css/font.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ url('assets/css/app.css') }}" type="text/css" />
</head>
<body>
<div class="app app-header-fixed ">
    <div class="container">
        <a href class="navbar-brand block m-t">KPI</a>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                @if($errors->all())
                    <div class="alert alert-danger">
                        {{ $errors->first() }}
                    </div>
                @endif
                <div class="panel panel-default">
                    <div class="panel-heading font-bold">Register</div>
                    <div class="panel-body">
                        <form action="{{ url('register') }}" method="post" class="bs-example form-horizontal ng-pristine ng-valid" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label class="col-lg-2 control-label">ชื่อ - สกุล</label>
                                <div class="col-lg-10">
                                    <input type="text" name="fullname" class="form-control" placeholder="ชื่อ - สกุล">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">หน่วยงาน</label>
                                <div class="col-lg-10">
                                    <input type="text" name="department" class="form-control" placeholder="หน่วยงาน">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">เขต</label>
                                <div class="col-lg-10">
                                    <input type="text" name="branch" class="form-control" placeholder="เขต">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">อีเมล</label>
                                <div class="col-lg-10">
                                    <input type="email" name="email" class="form-control" placeholder="อีเมล">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">รหัสผ่าน</label>
                                <div class="col-lg-10">
                                    <input type="password" name="password" class="form-control" placeholder="รหัสผ่าน">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">ยืนยันรหัสผ่าน</label>
                                <div class="col-lg-10">
                                    <input type="password" name="password_confirmation" class="form-control" placeholder="ยืนยันรหัสผ่าน">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">เบอร์โทรศัพท์</label>
                                <div class="col-lg-10">
                                    <input type="number" name="phone_number" class="form-control" placeholder="เบอร์โทรศัพท์">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-2 control-label">รูปโปรไฟล์</label>
                                <div class="col-lg-10">
                                    <input type="file" name="profile">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-sm btn-info">ตกลง</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ url('assets/libs/jquery/jquery/dist/jquery.js') }}"></script>
<script src="{{ url('assets/libs/jquery/bootstrap/dist/js/bootstrap.js') }}"></script>
<script src="{{ url('assets/js/ui-load.js') }}"></script>
<script src="{{ url('assets/js/ui-jp.config.js') }}"></script>
<script src="{{ url('assets/js/ui-jp.js') }}"></script>
<script src="{{ url('assets/js/ui-nav.js') }}"></script>
<script src="{{ url('assets/js/ui-toggle.js') }}"></script>
<script src="{{ url('assets/js/ui-client.js') }}"></script>
</body>
</html>
