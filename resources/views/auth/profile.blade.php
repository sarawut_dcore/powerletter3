@extends('layouts.master')

@section('title', 'ข้อมูลผู้ใช้งาน')

@section('content')
    <!-- content -->
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <h1 class="m-n font-thin h3 text-black">ข้อมูลผู้ใช้งาน</h1>
                            </div>
                        </div>
                    </div>
                    <!-- / main header -->
                    <div class="wrapper-md">

                    </div>
                </div>
                <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection