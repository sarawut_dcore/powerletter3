<!-- header -->

<header id="header" class="app-header navbar" role="menu">
    <!-- navbar header -->
    <div class="navbar-header bg-white-only">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
            <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
            <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="{{ url('/') }}" class="navbar-brand text-lt">
            
            <img src="{{ url('assets/img/logo.jpg') }}">
            <span class="hidden-folded m-l-xs" >กพร.</span>
        </a>
        <!-- / brand -->
    </div>
    <!-- / navbar header -->

    <!-- navbar collapse -->
    <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
        <!-- nabar right -->
            <div align ="right">
                    <table >
                            <tr>
                             
                              <td><h4 align ="center" >ระบบสืบค้นและรายงานมอบอำนาจ</h4></td>
                              <td>        
                                  <ul class="nav navbar-nav navbar-right">
        
                                    <li class="dropdown">
                                         
                                        <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
                                            <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
                                                <img src="{{ url('assets/img/logo.jpg') }}">
                                            </span>
                                            <span class="hidden-sm hidden-md">{{ Auth::user()->username }} / {{Auth::user()->office_name}}</span>
                                            <b class="caret"></b>
                                        </a>
                                        <!-- dropdown -->
                                        <ul class="dropdown-menu animated fadeInRight w">
                                           <!--
                                            <li>
                                                <a href="{{ url('/profile') }}">โปรไฟล์</a>
                                            </li>
                                            -->
                                            <li class="divider"></li>
                                            <li>
                                                <a href="{{ url('/logout') }}">ออกจากระบบ</a>
                                            </li>
                                        </ul>
                                        <!-- / dropdown -->
                                    </li>
                                </ul>
                                <!-- / navbar right -->
                             </td>
                            </tr>
                          </table>
                 
               
                 
                 
                 
                
            </div>
                
            <script>  
            var timeNow = new Date("2018-10-05 23:59:59"); 
           
            
            var strDate =  timeNow.getDate();
            var strMouth =  timeNow.getMonth()+1;
            var strYear = timeNow.getFullYear();
            var strHours = timeNow.getHours();
            var strMinutes= timeNow.getMinutes();
            var strSec  = timeNow.getSeconds();

            
           document.getElementById("TimeNow").innerHTML = strDate + "/" + strMouth + "/" + strYear + " " + strHours + ":" + strMinutes + ":" + strSec;
              
         //document.getElementById("diffTime").innerHTML = diff_timeNow;
                   
            window.onload = function() 
            {
              var hou = 2;
              var sec = 60;
              
              setInterval(function(){
                  
              var _timeNow = new Date();
              
                var _strMouth =   _timeNow.getMonth()+1;
                var _strYear = _timeNow.getFullYear();
                var _strDate = _timeNow.getDate();
                var _strHours = _timeNow.getHours();
                var _strMinutes= _timeNow.getMinutes();
                var _strSec  = _timeNow.getSeconds();
    
         
              // document.getElementById("timer").innerHTML = hou +" : " + sec ;
               // document.getElementById("diffTime").innerHTML =  ((strDate - _strDate)>0)?(1):(0) + " วัน " +  _strHours + "ชั่วโมง" +  _strMinutes + "นาที" +  _strSec +"วินาที"; 
               document.getElementById("diffTime").innerHTML = _strDate + "/" + _strMouth + "/" + _strYear + " " + _strHours + ":" + _strMinutes + ":" + _strSec;

               },1000);
             }
             </script>
             

    </div>
    <!-- / navbar collapse -->
</header>
<!-- / header -->