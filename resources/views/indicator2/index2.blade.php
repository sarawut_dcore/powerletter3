@extends('layouts.master')

@section('title', 'รายงานตัวชี้วัด ตามนโยบายกระทรวงยุติธรรม')

@section('content')
    <div id="app">
        
        <indicator
                :transaction-choices="{{ json_encode($transactionChoices) }}"
                :indicators="{{ json_encode($indicators) }}"
                :url-file="{{ json_encode(url('/file/')) }}"
                :indicator-choices="{{ json_encode($indicatorChoices) }}"
        />
    </div>
@endsection