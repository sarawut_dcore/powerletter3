@extends('layouts.master')

@section('title', 'รายงานตัวชี้วัด ตามนโยบายกระทรวงยุติธรรม')

@section('content')
    <!-- content -->
   
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div  class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div  class="col-sm-6 col-xs-12">
                                <h1   class="m-n font-thin h3 text-black">รายงานตัวชี้วัด ตามนโยบายกระทรวงยุติธรรม</h1>
                            </div>
                        </div>
                    </div>
                    <!-- / main header -->
                    <div class="wrapper-md">
                        <div class="row">
                            <form disabled>
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            รายการ
                                        </div>
                                        <table class="table-responsive table table-bordered">
                                            <thead>
                                            <tr class="footable-header">
                                                <th class="col-md-1">ขั้นตอนที่</th>
                                                <th class="col-md-1">ชื่อตัวชี้วัด</th>
                                                <th class="col-md-1">หน่วยวัด</th>
                                                <th class="col-md-1">เป้าหมาย</th>
                                                <th class="col-md-2">ผลการดำเนินงาน</th>
                                                <th class="col-md-1">แนบไฟล์</th>
                                                <th class="col-md-1">คำสั่ง</th>
                                                
                                            </tr>
                                            </thead>
                                            <tbody>
                                              
                                            @foreach($indicators as $indicator)
                                                <tr class="info">
                                                    <td></td>
                                                    <td class="col-lg-5">
                                                        <span class="font-bold">{{ $indicator->detail }}</span><br/>
                                                        @foreach($indicator->subDetails as $subDetail)
                                                            <span>- {{ $subDetail->detail }}</span><br/>
                                                        @endforeach
                                                    </td>
                                                    <td>{{ $indicator->type ? $indicator->type->name : '-' }}</td>
                                                    <td>{{ $indicator->target }}</td>
                                                    <td>
                                                            @if($indicator->isPercent() || $indicator->isCount())
                                                        <input type="text" name="score" placeholder="คำตอบ" class="form-control" />
                                                            @endif
                                                    </td>
                                                    <td>
                                                        @if($indicator->isPercent() || $indicator->isCount())
                                                            <input type="file" name="file" />
                                                        @endif
                                                    </td>
                                                    <td>
                                                        @if($indicator->isPercent() || $indicator->isCount())
                                                            <span class="btn btn-black" @click="submitRecord(indicator.choices[0].id)">บันทึก</span>
                                                        @endif
                                                    </td>   
                                                </tr>
                                                @php
                                                    $count = 1;
                                                   
                                                @endphp
                                               
                                                @foreach($indicator->choices as $choice)
                                                    @if($indicator->isStep()||$indicator->isStepEdit())
                                                    <tr class="active">
                                                        <td>{{ $count }}</td>
                                                        <td>{{ $choice->detail }}</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>
                                                            @if($indicator->isStep())
                                                            <label class="i-checks">
                                                                <input type="checkbox"><i></i>
                                                            </label>
                                                            @else
                                                            {{ $choice->score }}
                                                            <input type="text" name="score" placeholder="คำตอบ" class="form-control" />
                                                            @endif            
                                                        </td>
                                                        <td>
                                                            <input type="file" name="file" />
                                                        </td>
                                                        <td>
                                                               
                                                            <span class="btn btn-black"  @click="submitRecord(choice.id)">บันทึก</span>
                                                              
                                                        </td>   
                                                    </tr>
                                                    @php
                                                        $count = $count + 1;
                                                    @endphp
                                                    @endif
                                                @endforeach
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
     
                                    <div class="text-center m-b">
                                        <span class="text-danger m-r">**กรณีตรวจสอบความถูกต้องครบทุกตัว**</span>

                                    </div>

                                </div>
                            </form>
                        </div>
                    </div>
                    
                </div>
                <!-- / main -->
            </div>
        </div>
    </div>

    <script>
            export default {
                props: [
                    'indicators',
                    'transactionChoices',
                    'urlFile',
                    'indicatorChoices'
                ],
                created () {
        //            for(let i=0;i<this.transactionChoices.length;i++) {
        //                this.frmAnswer['ans-' + this.transactionChoices[i].indicator_choice_id] = this.transactionChoices[i].score;
        //                this.frmFileDownload['file-' + this.transactionChoices[i].indicator_choice_id] = this.transactionChoices[i].path_file;
        //            }
        
                    for(let i=0;i<this.indicatorChoices.length;i++) {
                        let item = {
                                answer: this.indicatorChoices[i].answer,
                                file: this.indicatorChoices[i].path_file
                        };
        
                        this.data = {...this.data, [this.indicatorChoices[i].id]: item};
                    }
                },
                data () {
                    return {
                        data: {},
                        frmFile: [],
                    }
                },
                methods: {
                    processFile(id, e) {
                        this.frmFile['file-' + id] = e.target.files[0];
                    },
        
                    submitRecord (id) {
                        let data = new FormData();
                        data.append('file', this.frmFile['file-' + id]);
                        data.append('id', id);
                        data.append('answer', this.data[id].answer);
        
                        console.log(this.data[id].answer);
        
                        axios.post('/ajax/indicator/save-record', data).then(res => {
                            alert(res.data.message);
        
                            this.data[id].file = res.data.filename;
                        }).catch(error => {
                            alert('มีบางอย่างผิดพลาด ไม่สามารถบันทึกได้');
                        });
                    },
                    submitForm () {
                        console.log(this.frmAnswer);
                        console.log(this.frmFile);
                    }
                }
            }
        </script>
    <!-- /content -->

@endsection
