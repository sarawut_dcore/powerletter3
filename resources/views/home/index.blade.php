@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')
    <!-- content -->
    @php
      $AllUsers
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="col-md-12">            
      <div class="row row-sm">
        <div class="col-xs-6 text-center">
          <div ui-jq="easyPieChart" ui-options="{
              percent: 75,
              lineWidth: 4,
              trackColor: '#e8eff0',
              barColor: '#7266ba',
              scaleColor: false,
              size: 115,
              rotate: 90,
              lineCap: 'butt'
            }" class="inline m-t easyPieChart" style="width: 115px; height: 115px; line-height: 115px;">
            <div>
              <span class="text-primary h1">{{ json_decode($_Count_SUM)->{'Count_User_Use'} }}%</span>
            </div>
          <canvas width="115" height="115"></canvas></div>
          <div class="text-muted font-bold text-xs m-t m-b">ผู้เข้าใช้งาน</div>
        </div>
        <div class="col-xs-6 text-center">
          <div ui-jq="easyPieChart" ui-options="{
              percent: 50,
              lineWidth: 4,
              trackColor: '#e8eff0',
              barColor: '#23b7e5',
              scaleColor: false,
              size: 115,
              rotate: 180,
              lineCap: 'butt'
            }" class="inline m-t easyPieChart" style="width: 115px; height: 115px; line-height: 115px;">
            <div>
              <span class="text-info h1">{{ json_decode($_Count_SUM)->{'Count_User_Non'} }}%</span>
            </div>
          <canvas width="115" height="115"></canvas></div>
          <div class="text-muted font-bold text-xs m-t m-b">ไม่ได้เข้าใช้งาน</div>
        </div>
      </div>            
    </div>
    
    

    <div class="wrapper-md">
      <!-- users -->
      <div class="row">
        <div class="col-md-6">
          <div class="panel no-border">
            <div class="panel-heading wrapper b-b b-light">

              <h5 class="font-thin m-t-none m-b-none text-muted">รายชื่อผู้ดูระบบและผู้ตรวจประเมินผล</h5>              
            </div>
            <ul class="list-group list-group-lg m-b-none">
                @foreach($_UserAdmin as $_UserAdmin_)
                  <li class="list-group-item">
                    <a href class="thumb-sm m-r">
                      <img src="{{ url('assets/img/admin.jpg') }}" class="r r-2x">
                    </a>
                    <span class="pull-right label bg-primary inline m-t-sm">{{ $_UserAdmin_->username }}</span>
                    <a href>{{ $_UserAdmin_->office_name }}</a>
                  </li>
                @endforeach
            </ul>

          </div>
        </div>
        <div class="col-md-6">
            <div class="panel no-border">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted">รายชื่อผู้ใช้งานแต่ละเรือนจำ</h5>              
              </div>
              <ul class="list-group list-group-lg m-b-none">
                  @foreach($_UserNormal as $_UserNormal_)
                  <li class="list-group-item">
                    <a href class="thumb-sm m-r">
                      <img src="{{ url('assets/img/user.jpg') }}" class="r r-2x">
                    </a>
                    <span class="pull-right label bg-info inline m-t-sm">{{ $_UserNormal_->username }}</span>
                    <a href>{{ $_UserNormal_->office_name }}</a>
                  </li>
                @endforeach

              </ul>
  
            </div>
          </div>
      </div>
      <!-- / users -->
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
