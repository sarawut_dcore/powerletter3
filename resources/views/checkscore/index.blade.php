@extends('layouts.master')

@section('title', 'ตรวจสอบคะแนน')

@section('content')
    <!-- content -->
    <!-- 
    {{  $_ChkScore}}
    -->
    <br>
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div class="col-sm-12 col-xs-12">
                                <b class="m-n font-thin h3 text-black" ><center><font size="3">รายงานสรุป {{  $_txt_name[0]['txt_name']}} ของ {{  $_office_name_[0]['office_name']}} <br>  ประจำปีงบประมาณ พ.ศ.{{  $_txt_name[0]['user_year']}} รอบ {{  $_txt_name[0]['repeat_m']}} เดือน<br></font></center>
                                    <table width="100%" height="31%" border="1" align="center" cellpadding="0" cellspacing="2" id="table">  
                                        <thead>    
                                            <tr><td width="5%" bgcolor="#A4A4A4"><div align="center"><font color="#0101DF" size="2"><b>ตัวชี้วัดที่</b></font></div></td>
                                                <td width="35%" bgcolor="#A4A4A4"><div align="center"><font color="#0101DF" size="2"><b>รายละเอียดตัวชี้วัด</b></font></div></td>	
                                                <td width="5%" bgcolor="#A4A4A4"><div align="center"><font color="#0101DF" size="2"><b>ระดับคะแนน</b></font></div></td>	
                                                <td width="5%" bgcolor="#A4A4A4"><div align="center"><font color="#0101DF" size="2"><b>น้ำหนัก</b></font></div></td>
                                                <td width="5%" bgcolor="#A4A4A4"><div align="center"><font color="#0101DF" size="2"></font></div></td>
                                                <td width="20%" bgcolor="#A4A4A4"><div align="center"><font color="#0101DF" size="2"><b>เหตุผล</b></font></div></td>
                                                <td width="10%" bgcolor="#A4A4A4"><div align="center"><font color="#0101DF" size="2"><b>ผู้ประเมิน</b></font></div></td>
                                                <td width="15%" bgcolor="#A4A4A4"><div align="center"><font color="#0101DF" size="2"><b>เบอร์โทร</b></font></div></td>    
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php 
                                                $count_num=1;
                                                $sum_scort=0;
                                                $percen_scort=0;
                                            @endphp 
                                            @foreach($_ChkScore as $_ChkScore_)
                                            <tr>    
                                                <td width="5%" bgcolor="#DEDEDE"><div align="center"><font size="2">{{  $count_num }}</font></div></td>
                                                <td width="25%" bgcolor="#DEDEDE"><div align="left"><font size="2">{{  $_ChkScore_->detail }}</font></div></td>	
                                                            <td width="5%" bgcolor="#DEDEDE"><div align="center"><font size="2">{{  $_ChkScore_->score }}</font></div></td>
                                                <td width="5%" bgcolor="#DEDEDE"><div align="center"><font size="2">{{  $_ChkScore_->percent }}</font></div></td>
                                                            <td width="5%" bgcolor="#DEDEDE"><div align="center"><font size="2">{{  ($_ChkScore_->score*$_ChkScore_->percent)/100 }}</font></div></td>
                                                <td width="20%" bgcolor="#DEDEDE"><div align="center"><font size="2"> {{  $_ChkScore_->comment }}</font></div></td>
                                                <td width="15%" bgcolor="#DEDEDE"><div align="center"><font size="2">{{  $_ChkScore_->name_evaluate }}</font></div></td>
                                                <td width="10%" bgcolor="#DEDEDE"><div align="center"><font size="2"> {{  $_ChkScore_->telephone_evaluate }}</font></div></td>                                               
                                            </tr>

                                            @php
                                               $sum_scort = $sum_scort+ ($_ChkScore_->score*$_ChkScore_->percent)/100;
                                               $percen_scort = ($sum_scort/5) *100;
                                               $count_num++;
                                            @endphp 
                                            @endforeach
                                            <tr>    
                                                    <td bgcolor="#FFBF00" colspan="4"><div align="center"><font color="red" size="2"><b>สรุปคะแนนถ่วงน้ำหนัก  =</b></font></div></td>
                                                                    
                                                    <td bgcolor="#FFBF00"><div align="center"><font color="red" size="2"><b>{{  $sum_scort}}</b></font></div></td>
                                                    
                                                    <td bgcolor="#FFBF00" colspan="2"><div align="center"><font color="red" size="2"><b> คิดเป็นร้อยละ</b></font></div></td>
                                                    
                                                    
                                                    <td bgcolor="#FFBF00"><div align="center"><font color="red" size="2"><b>{{  $percen_scort }} </b></font></div></td>
                                                    
                                             </tr>                                       
                                        </tbody>
                                     
                                    </table>
                                 
                                    <table width="100%" border="0" align="center">   
                                       <tbody><tr>	
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center"><font color="red"><br>เห็นชอบผลคะแนน</font></div></font></td>
                                        <td width="30%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center"></div></font></td>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center"><font color="red"><br>อนุมัติผลคะแนน</font></div></font></td>	
                                       </tr>
                                       
                                    </tbody></table><br>   
                                    
                                    <table width="100%" border="0" align="center" >   
                                       <tbody><tr>	
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center">ประธานคณะกรรมการตรวจประเมินฯ</div></font></td>
                                        <td width="30%" bgcolor="#FFFFFF"><font color="#000000" ><div align="center"></div></font></td>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center">อธิบดีกรมราชทัณฑ์</div></font></td>	
                                       </tr>
                                       <tr>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000"><div align="center"> <img src="{{ url('assets/img/ast_director.jpg') }}" width="100" height="50"></div></font></td>
                                        <td width="30%" bgcolor="#FFFFFF"><font color="#000000"><div align="center"> </div></font></td>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000"><div align="center"> <img src="{{ url('assets/img/director.jpg') }}" width="100" height="50"></div></font></td>	
                                       </tr>
                                       <tr>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center">(นายอายุตม์  สินธพพันธุ์)</div></font></td>
                                        <td width="30%" bgcolor="#FFFFFF"><font color="#000000"><div align="center"></div></font></td>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center">(พันตำรวจเอก ณรัชต์  เศวตนันทน์)</div></font></td>	
                                       </tr>
                                       <tr>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center">รองอธิบดีกรมราชทัณฑ์ ฝ่ายบริหาร</div></font></td>
                                        <td width="30%" bgcolor="#FFFFFF"><font color="#000000"><div align="center"></div></font></td>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center">อธิบดีกรมราชทัณฑ์</div></font></td>	
                                       </tr>
                                       <tr>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000"><div align="center"></div></font></td>
                                        <td width="30%" bgcolor="#FFFFFF"><font color="#000000" size="3"><div align="center">ออกรายงาน ณ วันที่  09/04/2561</div></font></td>
                                        <td width="35%" bgcolor="#FFFFFF"><font color="#000000"><div align="center"></div></font></td>	
                                       </tr>
                                       
                                       </tbody>
                                    </table>
                                </b>
                                    <script>
                                   function printDiv(divName) {
                                        var printContents = document.getElementById(divName).innerHTML;
                                        var originalContents = document.body.innerHTML;

                                        document.body.innerHTML = printContents;

                                        window.print();

                                        document.body.innerHTML = originalContents;
                                        }
                                    </script>
                                    <center><button onclick="printDiv('content')">Print this page</button></center> 
                                   
                            </div>
                        </div>
                    </div>
                    <!-- / main header -->
                    <div class="wrapper-md">

                    </div>
                </div>
                <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->

@endsection