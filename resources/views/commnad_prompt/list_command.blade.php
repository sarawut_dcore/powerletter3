@extends('layouts.master')

@section('title', 'นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์')

@section('content')
    <!-- content -->
    @php
     

    @endphp

    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <h1 class="m-n font-thin h3 text-black">
                                    นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์
                                </h1>
                               <!-- main table --> 
                            </div>
                        </div>
                    </div>

                    
                    <!-- / main header -->

                    <div class="wrapper-md">
                        <div class="row">
                             <!-- / form  -->
                              
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" >
                                               
                                                        <h3>  แนวทางการดำเนินงานในการปฏิบัติตามนโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์ </h3> 
                                                          
                                     

                                        </div>

                                        <table cellspacing="0" border="0">
                                                <colgroup width="55"></colgroup>
                                                <colgroup width="555"></colgroup>
                                                <colgroup width="208"></colgroup>
                                                <colgroup width="133"></colgroup>
                                                <colgroup width="172"></colgroup>
                                                <colgroup width="344"></colgroup>
                                                <colgroup width="172"></colgroup>
                                                <tr>
                                                    <td style="border-bottom: 1px solid #000000" colspan=7 height="40" align="left" valign=middle bgcolor="#9DC3E6"><b><u><font face="TH SarabunIT๙" size=4 color="#000000">1. การมอบอำนาจของอธิบดีกรมราชทัณฑ์ให้แก่ รองอธิบดีกรมราชทัณฑ์</font></u></b></td>
                                                    </tr>
                                                 <!-- Head Table   -->      
                                                <tr>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 height="80" align="center" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">ลำดับที่</font></b></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">เรื่องที่มอบอำนาจ</font></b></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">คำสั่งกรมฯ </font></b></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">ชื่ออธิบดีกรมราชทัณฑ์</font></b></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">ผู้รับมอบอำนาจ</font></b></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="left" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">หมายเหตุ</font></b></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" rowspan=2 align="center" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">เอกสาร</font></b></td>
                                                </tr>
                                                <tr>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">ลงวันที่</font></b></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle bgcolor="#D9D9D9"><b><font face="TH SarabunIT๙" size=4 color="#000000">ผู้มอบอำนาจ</font></b></td>
                                                </tr>
            
                                                 <!-- body  -->   
                                                 @foreach($_topics_detail_all as $out_topics_detail_all)
                                                <tr>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="50" align="center" valign=middle sdval="1" sdnum="1033;"><font face="TH SarabunIT๙" size=4 color="#000000">{{  $out_topics_detail_all->no }}</font></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="justify" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">{{  $out_topics_detail_all->topics }} </font></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">{{  $out_topics_detail_all->command_no }} 
                                                        <br>
                                                       
                                                     
                                                        <?php
                                                        
                                                         
                                                       
                                                          echo  $out_topics_detail_all->thaiday ."/". $out_topics_detail_all->thaimount."/".$out_topics_detail_all->thaiyear;
     

                                                        ?>
                                                        
                                                        </font>
                                                    </td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">{{  $out_topics_detail_all->name_offer }}</font></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">{{  $out_topics_detail_all->name_receive }}</font></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">{{  $out_topics_detail_all->note }}</font></td>
                                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">
                                                      
                                                        <div class="m-t"><a href="{{ url('/file/').'/'.$out_topics_detail_all->add_file }} ">
                                                            <span class="fa fa-file-pdf-o text-danger m-r-sm" style="font-size: 20px;"></span> {{$out_topics_detail_all->add_file}}
                                                            <br>
                                                            
                                                        </a></div>
                                                    </font></td>
                                                </tr>
                                                @endforeach
            
            
                                                
            
                                            </table>
                                    </div>
                                   
                                </div>
                               
                            <!-- / end form -->
                        </div>
                        
                        
                    </div>
                    

                    
                    
                    
                <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection
<

    
  