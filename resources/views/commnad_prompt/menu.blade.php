
@extends('layouts.master')

@section('title', 'นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์')

@section('content')
    
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted">รายการหัวข้อหนังสือมอบอำนาจ</h5>              
              </div>
              <ul class="list-group list-group-lg m-b-none">
                  
                   
                  
                 
                    @foreach($_data_topics_name_all as $out_data_topics_name_all)
                    
                    <li class="list-group-item">
                            
                                   <a href ="{{ url('menu01/').'/'.$out_data_topics_name_all->no }}"  >
                                    {{  $out_data_topics_name_all->topics  }}       
                                   
                                  
                                   </a>
                                </li>         
                    @endforeach


              </ul>

            </div>
          </div>
       
        
        
        
        
        
        
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection

