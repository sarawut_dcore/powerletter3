@extends('layouts.master')

@section('title', 'ตรวจและประเมินผล')

@section('content')
    <!-- content -->
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
                <!-- main -->
                <div class="col">
                    <!-- main header -->
                    <div class="bg-light lter b-b wrapper-md">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <h1 class="m-n font-thin h3 text-black">ตรวจและประเมินผล</h1>
                               <!-- main table --> 
                            </div>
                        </div>
                    </div>
 
                    @php
                    
                     $tmp_user_name ="";
                     $tmp_choice = 1;
                     $tmp_station_num = 1;
                     $switch = 0;
                     $str_indicator_type_id ="";
                     if($_indicator[0]['indicator_type_id'] == 1 )  $str_indicator_type_id = "ร้อยละ";
                     if($_indicator[0]['indicator_type_id'] == 2 )  $str_indicator_type_id = "จำนวน";
                     if($_indicator[0]['indicator_type_id'] == 3 )  $str_indicator_type_id = "ระดับ";
                     if($_indicator[0]['indicator_type_id'] == 4 )  $str_indicator_type_id = "ระดับจำนวน";
                     
                    @endphp
                    
                  
                    
                    <!-- / main header -->

                    <div class="wrapper-md">
                        <div class="row">
                             <!-- / form  -->
                              
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading" >
                                               
                                                        <h3> รายการตรวจและประเมินผล </h3> 
                                                        <span class="font-bold">{{  $_indicator[0]['detail'] }}, หน่วยการวัด {{  $str_indicator_type_id }} , เป้าหมาย {{  $_indicator[0]['target'] }} </span><br/>  
                                                        @if(($str_indicator_type_id == "ระดับ")||($str_indicator_type_id == "ระดับจำนวน"))
                                                        <span>คะแนน <br>  
                                                        @foreach($_IndicatorChoice as $_IndicatorChoice_)
                                                        <span>
                                                        รายการที่ {{  $tmp_choice }}  
                                                        @php 
                                                         $tmp_choice++;
                                                        @endphp 
                                                         {{ $_IndicatorChoice_->detail }} 
                                                        </span><br/>

                                                        @endforeach
                                                    
                                                    @else
                                                       <span>คะแนน <br>
                                                        @foreach($_IndicatorSubDetail as $_IndicatorSubDetail_)
                                                        
                                                        {{ $_IndicatorSubDetail_->num_grad }} ,
                                                        {{ $_IndicatorSubDetail_->detail }} 
                                                        </span><br/>

                                                        @endforeach  
                                                        @endif

                                        </div>

                                        <table class="table-responsive table table-bordered">
                                            <thead>
                                            <tr class="info">
                                                <th>ชื่อหน่วยงาน</th>
                                                <th>ชื่อตัวชี้วัด</th>
                                                <th>ผลการดำเนินงาน</th>
                                                <th>แนบไฟล์</th>
                                                <th>ผลคะแนน</th>
                                                <th>คำแนะนำ</th>
                                                <th>คำสั่ง</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($_Choice as $_Choice_)   
                                                
                                                    <tr class="info">   
                                                            <td   >
                                                                    <h4>  
                                                                    @php 
                                                                       #$_Choice_->office_name  
                                                                      
                                                                       if(strcmp ($_Choice_->user_name, $tmp_user_name)<>0)
                                                                       {
                                                                           
                                                                       
                                                                        # echo '<>'.
                                                                        echo $tmp_station_num.' '; 
                                                                        echo $_Choice_->office_name;
                                                                        
                                                                        
                                                                        
                                                                        $tmp_choice =1;
                                                                        $tmp_user_name=$_Choice_->user_name;
                                                                        $tmp_station_num++;
                                                                       }
                                                                        
                                                                       else {
                                                                           # code...
                                                                           # echo '=='; 
                                                                           $tmp_choice++;
                                                                           
                                                                       } 
                                                                    
                                                                    @endphp 
                                                                    </h4>
                                                             </td >
                                                          
                                                        <td class="col-lg-4">
                                                            <span >
                                                                @if ($tmp_choice == 1)
                                                             
                                                                <h4  >{{  $_indicator[0]['detail'] }}</h4>
                                                                <br>
                                                               
                                                                @endif
                                                                    @if(($str_indicator_type_id == "ระดับ")||($str_indicator_type_id == "ระดับจำนวน"))
                                                                      รายการที่ {{  $tmp_choice }}  {{ $_Choice_->choice_detail }}
                                                                    @else 
                                                                      ผลการรายงาน
                                                                    @endif 
                                                                          
                                                            </span><br/>
                                                            
                                                           
                                                             
                                                             
                                                        </td>

                                                        <td >
                                                           
                                                              {{ $_Choice_->score }}
                                                        
                                                            
                                                            
                                                            
                                                        </td>
                                                        <td >
                                                                @if($_Choice_->path_file!=null) 
                                                                <div class="m-t"><a href="{{ url('/file/').'/'.$_Choice_->path_file }} ">
                                                                    <span class="fa fa-file-pdf-o text-danger m-r-sm" style="font-size: 20px;"></span> ดาวน์โหลด
                                                                    <br>
                                                                    
                                                                </a></div>
                                                                @endif
                                                        </td>
                                                        <form>
                                                             
                                                        <td >
                                                            @if ($tmp_choice ==1)
                                                               @if($str_indicator_type_id=='ระดับ')
                                                                  <input  type="hidden" name="step" value="1"></input>
                                                               @else
                                                                  <input  type="hidden" name="step" value="0"></input>
                                                               @endif  
                                                        <select name="score"  >
                                                            <option style="display:none;" selected>
                                                             @if($_Choice_->eva_user_name == $_Choice_->user_name)
                                                                {{  $_Choice_->eva_score }}
                                                             @else 
                                                                0    
                                                            @endif 
                                                            </option>
                                                                <option value="1.00">1.00</option>
                                                                <option value="1.25">1.25</option>
                                                                <option value="1.50">1.50</option>
                                                                <option value="1.75">1.75</option>
                                                                <option value="2.00">2.00</option>
                                                                <option value="2.25">2.25</option>
                                                                <option value="2.50">2.50</option>
                                                                <option value="2.75">2.75</option>
                                                                <option value="3.00">3.00</option>
                                                                <option value="3.25">3.25</option>
                                                                <option value="3.50">3.50</option>
                                                                <option value="3.75">3.75</option>
                                                                <option value="4.00">4.00</option>   
                                                                <option value="4.25">4.25</option>
                                                                <option value="4.50">4.50</option>
                                                                <option value="4.75">4.75</option>
                                                                <option value="5.00">5.00</option>                                                                                                                                                                                             
                                                            </select>
                                                            @endif
                                                        </td>
                                                        <td class="col-lg-3">   
                                                            @if ($tmp_choice ==1)
                                                            <textarea name="comment" class="form-control" rows="6" placeholder="Type your message">@if($_Choice_->eva_user_name == $_Choice_->user_name){{ $_Choice_->eva_comment}}@endif</textarea>
                                                            @endif
                                                        </td>
                                                        <td>
                                                                @if ($tmp_choice ==1)
                                                                  <button name="EvaSave" value="{{ $_Choice_->user_name }}" class="btn m-b-xs w-xs btn-success"> บันทึก </button>
                                                                @endif  
                                                        </td>
                                                        </form>
                                                    </tr>
                                               
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="text-center m-b">
                                        <span class="text-danger m-r">**กรณีตรวจสอบความถูกต้องครบทุกตัว**</span>
                                        
                                    </div>
                                   
 
                                </div>
                               
                            <!-- / end form -->
                        </div>
                        
                        
                    </div>
                    

                    
                    
                    
                <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection