@extends('layouts.master')

@section('title', 'นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์')

@section('content')
  

<div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนำจกำรปฏิบัติรำชกำรแทนอธิบดีกรมรำชทัณฑ์</h5>              
              </div>
                                 <!-- /.panel-heading -->
                                 <div class="panel-body">
                                    <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>รายละเอียดการดำเนินงาน</th>
                                        <th>ผลดำเนินงาน</th>
                                        <th>เอกสารแนบ</th>
                                        <th>สั่ง</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>

                                 
                                    @foreach($_datapolicy as $_datapolicy_)
                                    <form>   
                                    <tr>
                                            <td>
                                            @if( $_datapolicy_->num_3 == '0') 
                                                {{  $_datapolicy_->num_1 <> '0' ? $_datapolicy_->num_1 : ' ' }}
                                                {{  $_datapolicy_->num_2 <> '0' ? '.'.$_datapolicy_->num_2 : ' ' }}
                                            @endif    

                                            </td>
                                            <td> {{$_datapolicy_->detail }} </td>
                                            <td>
                                                <label class="i-checks">
                                               
                                               
                                                   <input type="checkbox"  name="Check" 
                                                   
                                                   
                                                   {{  $_datapolicy_->check_value <> '1' ? '' : 'checked' }}

                                                   ><i></i>
                                                </label>    
                                            </td>
                                            <td>
                                                @if( $_datapolicy_->num_2 == '0') 
                                                Form::open(array('url' => '/uploadfile','files'=>'true'));
                                                @endif    
                                                
                                            </td>
                                            <td>
                                                <button name="policySave" value="{{$_datapolicy_->id }}"> บันทึก </button>
                                            </td>

                                    </tr>
                                     </form>           
                                    @endforeach
                                   
                                    
                                   
                                </tbody>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
            </div>
            <!--    -->  
        </div>
    </div>
</div>
<!-- / main -->
</div>
</div>
</div>
<!-- /content -->







@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection