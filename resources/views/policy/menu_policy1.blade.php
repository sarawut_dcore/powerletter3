@extends('layouts.master')

@section('title', 'นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์')

@section('content')
  

   <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-header"> ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h3>
                <br>
                        <!--  -->
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        ข้อมูลคำสั่งมอบอำนำจกำรปฏิบัติรำชกำรแทนอธิบดีกรมรำชทัณฑ์
                        
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table cellspacing="0" border="0">
                                <colgroup width="67"></colgroup>
                                <colgroup width="648"></colgroup>
                                <colgroup width="94"></colgroup>
                                <tr>
                                    <td style="border-bottom: 1px solid #000000" colspan=3 height="35" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=5 color="#000000">นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</font></b></td>
                                    </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">ลำดับ</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">รายละเอียดการดำเนินงาน</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">เอกสาร</font></b></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=middle sdval="1" sdnum="1033;"><b><font face="TH SarabunIT๙" size=4 color="#000000">1</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="TH SarabunIT๙" size=4 color="#000000">เอกสารประกอบ </font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="center" valign=middle><b><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">&quot;นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์&quot; ประจำปี 2561&quot;</font></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br><img src="69ea4435214141c7b239ed222fd92fa6_htm_83bf706817772acf.png" width=22 height=21 hspace=36 vspace=16>
                                </font></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=middle sdval="2" sdnum="1033;"><b><font face="TH SarabunIT๙" size=4 color="#000000">2</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="TH SarabunIT๙" size=4 color="#000000">แบบฟอร์ม ที่ 1</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="center" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">ใบลงนามรับทราบของผู้ปฏิบัติงาน<br>***ให้ เรือนจำ/ทัณฑสถาน แนบเอกสารประกอบ***</font></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br><img src="69ea4435214141c7b239ed222fd92fa6_htm_83bf706817772acf.png" width=22 height=21 hspace=36 vspace=16>
                                </font></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=middle sdval="3" sdnum="1033;"><b><font face="TH SarabunIT๙" size=4 color="#000000">3</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="TH SarabunIT๙" size=4 color="#000000">แบบฟอร์ม ที่ 2</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="center" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">บัญชีรายชื่อข้าราชการที่รับทราบและลงนามรับทราบ<br>***ให้ เรือนจำ/ทัณฑสถาน แนบเอกสารประกอบ***</font></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br><img src="69ea4435214141c7b239ed222fd92fa6_htm_83bf706817772acf.png" width=22 height=21 hspace=36 vspace=16>
                                </font></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=middle sdval="4" sdnum="1033;"><b><font face="TH SarabunIT๙" size=4 color="#000000">4</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="TH SarabunIT๙" size=4 color="#000000">แบบฟอร์ม ที่ 3</font></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="32" align="center" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">แบบสรุปรายงานผลการดำเนินการตามมาตรการ โครงการตามนโยบายกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</font></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br><img src="69ea4435214141c7b239ed222fd92fa6_htm_83bf706817772acf.png" width=22 height=21 hspace=36 vspace=5>
                                </font></td>
                                </tr>
                                <tr>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="31" align="center" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><u><font face="TH SarabunIT๙" size=5 color="#000000">
                                         <a href = "{{ url('/list_policy_01') }}" > 
                                        ส่งรายงานผลการดำเนินการ</font></u></b></td>
                                    <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><u><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></u></td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-6 -->
            
        </div>

       
   </div>



@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection