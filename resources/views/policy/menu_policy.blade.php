@extends('layouts.master')

@section('title', 'หน้าหลัก')

@section('content')
    <!-- content -->
    @php
      // $_menu_9001
    @endphp
    <div id="content" class="app-content" role="main">
        <div class="app-content-body ">
            <div class="hbox hbox-auto-xs hbox-auto-sm">
  <!-- main -->
  <div class="col">
    <div class="wrapper-md">
      
        <div class="bg-light lter b-b wrapper-md">
            <div class="row">
                <div class="col-sm-6 col-xs-12">
                    <h1 class="m-n font-thin h3 text-black">ข้อมูลคำสั่งมอบอำนาจการปฏิบัติราชการแทนอธิบดีกรมราชทัณฑ์</h1>
                   <!-- main table --> 
                </div>
            </div>
        </div>
      <!-- users -->
      <div class="row">
        
        <!--    -->  

       
          <!--  1  -->  
          <div class="col-md-12">
            <div class="panel panel-success">
              <div class="panel-heading wrapper b-b b-light">

                <h5 class="font-thin m-t-none m-b-none text-muted"> ข้อมูลคำสั่งมอบอำนำจกำรปฏิบัติรำชกำรแทนอธิบดีกรมรำชทัณฑ์</h5>              
              </div>
                                 <!-- /.panel-heading -->
                                 <div class="panel-body">
                                    <div class="table-responsive">
                                        <table cellspacing="0" border="0">
                                            <colgroup width="67"></colgroup>
                                            <colgroup width="648"></colgroup>
                                            <colgroup width="94"></colgroup>
                                            <tr>
                                                <td style="border-bottom: 1px solid #000000" colspan=3 height="35" align="center" valign=middle bgcolor="#BDD7EE"><b><font face="TH SarabunIT๙" size=5 color="#000000">นโยบายการกำกับดูแลองค์การที่ดีของกรมราชทัณฑ์</font></b></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">ลำดับ</font></b></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">รายละเอียดการดำเนินงาน</font></b></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle bgcolor="#ADB9CA"><b><font face="TH SarabunIT๙" size=4 color="#000000">เอกสาร</font></b></td>
                                            </tr>


                                            @foreach($_datapolicy_menu as $out_datapolicy_menu)
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="27" align="center" valign=middle sdval="1" sdnum="1033;"><b><font face="TH SarabunIT๙" size=4 color="#000000">{{ $out_datapolicy_menu->no }}</font></b></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><b><font face="TH SarabunIT๙" size=4 color="#000000">{{ $out_datapolicy_menu->menu_detail }}</font></b></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                            </tr>
                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="54" align="center" valign=middle><b><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></b></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000">{{ $out_datapolicy_menu->sub_detail }}</font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="left" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br>
                                                  <a href="{{ $out_datapolicy_menu->add_file }}">
                                                    <br><span class="fa fa-file-pdf-o text-danger m-r-sm" style="font-size: 20px;"></span>เอกสาร<br>
                                                </a>
                                                </font></td>
                                            </tr>
                                            @endforeach


                                            <tr>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" height="31" align="center" valign=middle><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><b><u><font face="TH SarabunIT๙" size=5 color="#000000">
                                                     <a href = "{{ url('/list_policy_01') }}" > 
                                                    ส่งรายงานผลการดำเนินการ</td>
                                                <td style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000" align="center" valign=middle><u><font face="TH SarabunIT๙" size=4 color="#000000"><br></font></u></td>
                                            </tr>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->

            </div>
          </div>
       
        
        
        
        
        
        
        <!--    -->  
        
      </div>
    </div>
  </div>
  <!-- / main -->
            </div>
        </div>
    </div>
    <!-- /content -->
@endsection

@section('script')
    <script>
        $(document).ready(function() {

        });
    </script>
@endsection
