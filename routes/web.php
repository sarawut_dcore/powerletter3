<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Home Controller
Route::get('/', 'HomeController@getIndex')->middleware('auth');


Route::get('/menu01','topics_name_controller@getIndex')->middleware('auth');

Route::get('/menu01/{no_topics}','topics_name_controller@getTopics_detail')->middleware('auth');

//------------------------------------------------------------------------------
//   Test 



Route::get('/menu2', 'policy_menu_controller@getIndex');

Route::get('/list_policy_01', 'policycontroller@getIndex');
Route::get('/list_policy_01/{Indicator_id}', 'policycontroller@savePolicy');



Route::get('/menu3', function () {
    return view('report_auth/menu_report');
})->middleware('auth');


Route::get('/menu41', function () {
    return view('report_cmd/41menu');
});


Route::get('/menu51', function () {
    return view('report_cmd/51menu');
});

Route::get('/menu61', function () {
    return view('report_cmd/61menu');
});
Route::get('/menu71', function () {
    return view('report_cmd/71menu');
});
Route::get('/menu81', function () {
    return view('report_cmd/81menu');
});
Route::get('/menu91', function () {
    return view('report_cmd/91menu');
});
Route::get('/menu101', function () {
    return view('report_cmd/101menu');
});


/*
// Evaluation Controller
Route::get('/evaluation/{Indicator_id}', 'EvaluationController@getKpi')->middleware('auth');
Route::get('/evaluation', 'EvaluationController@getIndex')->middleware('auth');

Route::get('/evaluation2/{Indicator_id}', 'EvaluationController@getKpi2')->middleware('auth');
Route::get('/evaluation2', 'EvaluationController@getIndex2')->middleware('auth');

Route::post('/save_eva','EvaluationController@saveEva')->middleware('auth');
Route::post('/save_eva2','EvaluationController@saveEva2')->middleware('auth');

// Report  Controller
Route::get('/report/sum06', 'ReportController@getIndex6');
Route::get('/report/sum12', 'ReportController@getIndex12');



// Indicator Controller
Route::get('/indicator', 'IndicatorController@getIndex')->middleware('auth');
Route::get('/indicator2', 'IndicatorController@getIndex2')->middleware('auth');
Route::post('/ajax/indicator/save-record', 'IndicatorController@postAjaxSaveRecord')->middleware('auth');

// CheckScore Controller
Route::get('/check-score', 'CheckScoreController@getIndex')->middleware('auth');
Route::get('/check-score2', 'CheckScoreController@getIndex2')->middleware('auth');
*/

// Auth
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', 'Auth\LoginController@login');
Route::get('/register', 'Auth\RegisterController@showRegistrationForm');
Route::post('/register', 'Auth\RegisterController@register');
Route::get('/logout', function() {
    Auth::logout();
    return redirect('/login');
});

// Gen password
Route::get('generate-password/{password}', function($password) {
    return bcrypt($password);
});